<?php


class MenuMain{
  
    // database connection and table name
    private $conn;
    private $table_name = "menu_main";

    // object properties
    public $main_id;
	public $title;
	public $owner;
	public $lang_bm;
	 
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }
	// create object
	function createGrp(){
	 
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "  
				SET
				owner=:owner,title=:title,lang_bm=:lang_bm";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		 // sanitize
		
		$this->title=htmlspecialchars(strip_tags($this->title));
		$this->owner=htmlspecialchars(strip_tags($this->owner));
		$this->lang_bm=htmlspecialchars(strip_tags($this->lang_bm));
		
		// bind values
		//$stmt->bindParam(":main_id", $this->main_id);
		$stmt->bindParam(":title", $this->title);
		$stmt->bindParam(":owner", $this->owner);
		$stmt->bindParam(":lang_bm", $this->lang_bm);
				
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					owner,main_id";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}


	// used when filling up the update record form
	function readOne(){

		$this->main_id=htmlspecialchars(strip_tags($this->main_id));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					main_id = :main_id
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":main_id", $this->main_id);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		// $this->main_id = $row['main_id'];
		// $this->title = $row['title'];
		// $this->owner = $row['owner'];
	}

	// used when filtering by owner
	function readByOwner(){

		$this->owner=htmlspecialchars(strip_tags($this->owner));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					owner = :owner
					";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	
		// bind code of data to be updated
		$stmt->bindParam(":owner", $this->owner);

		// execute query
		$stmt->execute();
	
		return $stmt;
	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				main_id=:main_id,  title=:title, owner=:owner";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->main_id=htmlspecialchars(strip_tags($this->main_id));
		$this->title=htmlspecialchars(strip_tags($this->title));
		$this->owner=htmlspecialchars(strip_tags($this->owner));
		
		// bind values
		$stmt->bindParam(":main_id", $this->main_id);
		$stmt->bindParam(":title", $this->title);
		$stmt->bindParam(":owner", $this->owner);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	// update the Grp
	function updateGrp()
	{
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						title = :title,
						owner = :owner,
						lang_bm = :lang_bm
					WHERE
						main_id = :main_id";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->main_id=htmlspecialchars(strip_tags($this->main_id));
			$this->title=htmlspecialchars(strip_tags($this->title));
			$this->owner=htmlspecialchars(strip_tags($this->owner));

			$this->lang_bm=htmlspecialchars(strip_tags($this->lang_bm));
			
			// bind values
			$stmt->bindParam(":main_id", $this->main_id);
			$stmt->bindParam(":title", $this->title);
			$stmt->bindParam(":owner", $this->owner);
			$stmt->bindParam(":lang_bm", $this->lang_bm);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}
	
	// update the record
	function update(){
	
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						title = :title,
						owner = :owner,
						
					WHERE
						main_id = :main_id";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->main_id=htmlspecialchars(strip_tags($this->main_id));
			$this->title=htmlspecialchars(strip_tags($this->title));
			$this->owner=htmlspecialchars(strip_tags($this->owner));
			
			// bind values
			$stmt->bindParam(":main_id", $this->main_id);
			$stmt->bindParam(":title", $this->title);
			$stmt->bindParam(":owner", $this->owner);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE main_id = ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->main_id=htmlspecialchars(strip_tags($this->main_id));

		// bind id of record to delete
		$stmt->bindParam(1, $this->main_id);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}

	function readAllUniqueGroup()
	{
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				Group BY
					owner";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}
	
}

?>