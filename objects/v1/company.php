<?php
 ini_set('display_errors', 1); 
 ini_set('display_startup_errors', 1);
 error_reporting(E_ALL);
 mysqli_report(MYSQLI_REPORT_ALL);

 include_once 'company-document.php';

class Company{
  
    // database connection and table name
    private $conn;
    private $table_name = "company";

    // object properties
	public $co_id;
    public $co_reg_no;
	public $name;
	public $address;
	public $town;
	public $district;
	public $postcode;
	public $state;
	public $geocode;
	public $pic_url;
	public $contact_no;
	public $email;
	 
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					name";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}


	// used when filling up the update record form
	function readOne(){

		$this->co_reg_no=htmlspecialchars(strip_tags($this->co_reg_no));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					co_reg_no = :co_reg_no
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":co_reg_no", $this->co_reg_no);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->co_id = $row['co_id'];
		$this->co_reg_no = $row['co_reg_no'];
		$this->name = $row['name'];
		$this->address = $row['address'];
		$this->town = $row['town'];
		$this->district = $row['district'];
		$this->postcode = $row['postcode'];
		$this->state = $row['state'];
		$this->geocode = $row['geocode'];
		$this->pic_url = $row['pic_url'];
		$this->contact_no = $row['contact_no'];
		$this->email = $row['email'];

	}

	// added by majina 26-08-2021
 	// used when read record by Compang Reg No
	 function readByCoRegNo($co_reg_no){

		$companyDocument = new CompanyDocument($this->conn);

		$this->co_reg_no=htmlspecialchars(strip_tags($co_reg_no));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					co_reg_no = :co_reg_no
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":co_reg_no", $this->co_reg_no);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to json properties
		$record_item = null;

		if (isset($row['co_reg_no'])){
			$record_item=array(
				"co_id" => $row['co_id'],
				"co_reg_no" => $row['co_reg_no'],
				"name" => $row['name'],
				"address" => $row['address'],
				"town" => $row['town'],
				"district" => $row['district'],
				"postcode" => $row['postcode'],
				"state" => $row['state'],
				"geocode" => $row['geocode'],
				"pic_url" => $row['pic_url'],
				"contact_no" => $row['contact_no'],
				"email" => $row['email'],
				"documents" => $companyDocument->readByCoRegNo($row['co_reg_no'])
			);
		}

		return $record_item;

	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				co_reg_no=:co_reg_no,  name=:name, 
				address=:address, town=:town,
				district=:district, postcode=:postcode,
				state=:state, geocode=:geocode,
				pic_url=:pic_url, contact_no=:contact_no,
				email=:email";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->co_reg_no=htmlspecialchars(strip_tags($this->co_reg_no));
		$this->name=htmlspecialchars(strip_tags($this->name));
		$this->address=htmlspecialchars(strip_tags($this->address));
		$this->town=htmlspecialchars(strip_tags($this->town));
		$this->district=htmlspecialchars(strip_tags($this->district));
		$this->postcode=htmlspecialchars(strip_tags($this->postcode));
		$this->state=htmlspecialchars(strip_tags($this->state));
		$this->geocode=htmlspecialchars(strip_tags($this->geocode));
		$this->pic_url=htmlspecialchars(strip_tags($this->pic_url));
		$this->contact_no=htmlspecialchars(strip_tags($this->contact_no));
		$this->email=htmlspecialchars(strip_tags($this->email));
		
		// bind values
		$stmt->bindParam(":co_reg_no", $this->co_reg_no);
		$stmt->bindParam(":name", $this->name);
		$stmt->bindParam(":address", $this->address);
		$stmt->bindParam(":town", $this->town);
		$stmt->bindParam(":district", $this->district);
		$stmt->bindParam(":postcode", $this->postcode);
		$stmt->bindParam(":state", $this->state);
		$stmt->bindParam(":geocode", $this->geocode);
		$stmt->bindParam(":pic_url", $this->pic_url);
		$stmt->bindParam(":contact_no", $this->contact_no);
		$stmt->bindParam(":email", $this->email);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	
	// update the record
	function update(){
	
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						name=:name, 
						address=:address, town=:town,
						district=:district, postcode=:postcode,
						state=:state, geocode=:geocode,
						pic_url=:pic_url, contact_no=:contact_no,
						email=:email
					WHERE
						co_reg_no=:co_reg_no";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->co_reg_no=htmlspecialchars(strip_tags($this->co_reg_no));
			$this->name=htmlspecialchars(strip_tags($this->name));
			$this->address=htmlspecialchars(strip_tags($this->address));
			$this->town=htmlspecialchars(strip_tags($this->town));
			$this->district=htmlspecialchars(strip_tags($this->district));
			$this->postcode=htmlspecialchars(strip_tags($this->postcode));
			$this->state=htmlspecialchars(strip_tags($this->state));
			$this->geocode=htmlspecialchars(strip_tags($this->geocode));
			$this->pic_url=htmlspecialchars(strip_tags($this->pic_url));
			$this->contact_no=htmlspecialchars(strip_tags($this->contact_no));
			$this->email=htmlspecialchars(strip_tags($this->email));
			
			// bind values
			$stmt->bindParam(":co_reg_no", $this->co_reg_no);
			$stmt->bindParam(":name", $this->name);
			$stmt->bindParam(":address", $this->address);
			$stmt->bindParam(":town", $this->town);
			$stmt->bindParam(":district", $this->district);
			$stmt->bindParam(":postcode", $this->postcode);
			$stmt->bindParam(":state", $this->state);
			$stmt->bindParam(":geocode", $this->geocode);
			$stmt->bindParam(":pic_url", $this->pic_url);
			$stmt->bindParam(":contact_no", $this->contact_no);
			$stmt->bindParam(":email", $this->email);
	
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE co_reg_no = ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->co_reg_no=htmlspecialchars(strip_tags($this->co_reg_no));

		// bind id of record to delete
		$stmt->bindParam(1, $this->co_reg_no);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}
	

	// search records
	function search($keywords){
		
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					co_reg_no LIKE ? OR name LIKE ?  
				ORDER BY
				co_reg_no";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$keywords=htmlspecialchars(strip_tags($keywords));
		$keywords = "%{$keywords}%";
		
		// bind
		$stmt->bindParam(1, $keywords);
		$stmt->bindParam(2, $keywords);
	
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}
}

?>