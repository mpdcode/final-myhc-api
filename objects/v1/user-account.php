<?php

ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ALL);


include_once 'trans-log.php';
include_once 'registration-person.php';

class UserAccount{
  
    // database connection and table name
    private $conn;
    private $table_name = "user_account";

    // object properties
	public $email;
    public $username;
	public $password;
	public $ic_no;
	public $acc_type_code;
	public $acc_status_code;
	// public $menu_owner;
	public $reg_no;
	public $date_created;
	public $date_updated;
	public $last_login;
	public $date_of_birth;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					date_created desc";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}

	// read all records
	function readAllMauPerson(){
	
		// select all query
		$query = "SELECT
					u.*, p.name, p.email
				FROM
					" . $this->table_name . " u , person p
				where acc_type_code = 'MAU'
				and u.ic_no = p.ic_no
				and (acc_status_code = 'PRE-ACCOUNT' or acc_status_code = 'APPROVED')
				ORDER BY
					p.name";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}

	// read all records
	function readAllAdminPerson(){

		// select all query
		$query = "SELECT
					u.*, p.name, p.email
				FROM
					" . $this->table_name . " u , person p
				where acc_type_code = 'ADMIN'
				and u.ic_no = p.ic_no
				ORDER BY
					p.name";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}


	// used when to read user account by username
	function readOne(){

		$this->username=htmlspecialchars(strip_tags($this->username));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
				username = :username
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":username", $this->username);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->email = $row['email'] ?? null;
		// $this->password = $row['password'];
		$this->ic_no = $row['ic_no']?? null;
		$this->acc_type_code = $row['acc_type_code']?? null;
		// $this->reg_no = $row['reg_no'];
		$this->acc_status_code = $row['acc_status_code']?? null;
		$this->menu_owner = $row['menu_owner']?? null;
		$this->date_created = $row['date_created']?? null;
		$this->date_updated = $row['date_updated']?? null;
		$this->last_login = $row['last_login']?? null;

		$this->date_of_birth = $row['date_of_birth']?? null;
	}

	// used when to read user account by username
	function readOneByIcNo(){

		$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					ic_no = :ic_no
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":ic_no", $this->ic_no);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		
		// set values to object properties
		$this->username = $row['username'] ?? null;
		$this->ic_no = $row['ic_no'] ?? null;
		$this->acc_type_code = $row['acc_type_code'] ?? null;
		// $this->reg_no = $row['reg_no'];
		$this->acc_status_code = $row['acc_status_code'] ?? null;
		// $this->menu_owner = $row['menu_owner'];
		$this->date_created = $row['date_created'] ?? null;
		$this->date_updated = $row['date_updated'] ?? null;
		$this->last_login = $row['last_login']?? null;
		$this->date_of_birth = $row['date_of_birth']?? null;
	}

	// used when to read user account by username
	function readOneByIcNo_array(){

		$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					ic_no = :ic_no
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":ic_no", $this->ic_no);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$userAccount_item=null;

		if (isset($row['username'])){
			$userAccount_item=array(
				"username" => $row['username'],
				"ic_no" => $row['ic_no'],
				"acc_type_code" => $row['acc_type_code'],
				"acc_status_code" =>  $row['acc_status_code'],
				// "reg_no" => $row['reg_no'],
				// "menu_owner" => $row['menu_owner'],
				"date_created" => $row['date_created'],
				"date_updated" => $row['date_updated'],
				"last_login" => $row['last_login']
			);
		}

		return $userAccount_item;
	}

	// used when to validate and verify current password that entered by user
	function isPasswordVerified($epass,$username){

		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					username = :username  
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":username", $username);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);


		$hCpass = $this->encryptPassword($epass); 
		if (hash_equals($row['password'],$hCpass))
			return true;
		else
			return false;
		
 
	}

	function readByIcNo($ic_no){
		// select all query
		$query = "SELECT * FROM " . $this->table_name . "  where ic_no=:ic_no";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind code of data to be updated
		$stmt->bindParam(":ic_no", $ic_no);
	  
		// execute query
		$stmt->execute();

	    $row = $stmt->fetch(PDO::FETCH_ASSOC) ;

		$userAccount_item=null;

		if (isset($row['username'])){
			$userAccount_item=array(
				"username" => $row['username'],
				"ic_no" => $row['ic_no'],
				"acc_type_code" => $row['acc_type_code'],
				"acc_status_code" =>  $row['acc_status_code'],
				"date_created" => $row['date_created'],
				"date_updated" => $row['date_updated']
			);
		}

		return $userAccount_item;

	}

	function readByUsername($username){
		// select all query
		$query = "SELECT * FROM " . $this->table_name . "  where username = :username";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind code of data to be updated
		$stmt->bindParam(":username", $username);
	  
		// execute query
		$stmt->execute();

	    $row = $stmt->fetch(PDO::FETCH_ASSOC) ;

		$userAccount_item=null;

		if (isset($row['username'])){
			$userAccount_item=array(
				"username" => $row['username'],
				"ic_no" => $row['ic_no'],
				"acc_type_code" => $row['acc_type_code'],
				"acc_status_code" =>  $row['acc_status_code'],
				// "reg_no" => $row['reg_no'],
				"menu_owner" => $row['menu_owner'],
				"date_created" => $row['date_created'],
				"date_updated" => $row['date_updated'],
				"last_login" => $row['last_login']
			);
		}

		return $userAccount_item;

	}

  	// create object
	function create(){

		$transLog = new TransLog($this->conn);
                   
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET name=:name,
				email=:email, username=:username, ic_no=:ic_no, password=:password,menu_owner=:menu_owner,
				acc_type_code=:acc_type_code,  acc_status_code=:acc_status_code,
				date_created=:date_created , last_login=:last_login,
				date_of_birth=:dob
				
				";
			  
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->name=htmlspecialchars(strip_tags($this->name));
		$this->username=htmlspecialchars(strip_tags($this->username));
		$this->email=htmlspecialchars(strip_tags($this->email));
		$this->password=htmlspecialchars(strip_tags($this->password));
		$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
		$this->acc_type_code=htmlspecialchars(strip_tags($this->acc_type_code));
		// $this->reg_no=htmlspecialchars(strip_tags($this->reg_no));
		$this->acc_status_code=htmlspecialchars(strip_tags($this->acc_status_code));
		$this->menu_owner=htmlspecialchars(strip_tags($this->menu_owner));
		$this->date_created=htmlspecialchars(strip_tags($this->date_created));

		$this->last_login=htmlspecialchars(strip_tags($this->last_login));
		
		$this->dob=htmlspecialchars(strip_tags($this->dob));

		
		// bind values
		$stmt->bindParam(":name", $this->name);
		$stmt->bindParam(":username", $this->username);
		$stmt->bindParam(":email", $this->email);
		$stmt->bindParam(":password", $this->password);
		$stmt->bindParam(":ic_no", $this->ic_no);
		$stmt->bindParam(":acc_type_code", $this->acc_type_code);
		// $stmt->bindParam(":reg_no", $this->reg_no);
		$stmt->bindParam(":acc_status_code", $this->acc_status_code);
		$stmt->bindParam(":menu_owner", $this->menu_owner);
		$stmt->bindParam(":date_created", $this->date_created);
		$stmt->bindParam(":last_login", $this->last_login);
		$stmt->bindParam(":dob", $this->dob);


		
		
		// execute query
		if($stmt->execute()){
			
			$registrationPerson = new RegistrationPerson($this->conn);
			$registrationPerson->ic_no = $this->ic_no;
			$registrationPerson->reg_no = $this->reg_no;
			$registrationPerson->username = $this->username;
			$registrationPerson->updateUsername();

			$transLog->activity="Staff account created ".$this->username;
			$transLog->username="admin";
			$transLog->status="success";
			$transLog->create();
			return true;
		}else{
			print_r($stmt->errorInfo());
			return false;
		}
		
	}	
	
	// update the record
	function update(){
	
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						acc_type_code = :acc_type_code,
						acc_status_code = :acc_status_code,
						menu_owner = :menu_owner 
					WHERE
						username = :username";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->acc_type_code=htmlspecialchars(strip_tags($this->acc_type_code));
			$this->acc_status_code=htmlspecialchars(strip_tags($this->acc_status_code));
			$this->username=htmlspecialchars(strip_tags($this->username));
			$this->menu_owner=htmlspecialchars(strip_tags($this->menu_owner));
			
			// bind values
			$stmt->bindParam(":acc_type_code", $this->acc_type_code);
			$stmt->bindParam(":acc_status_code", $this->acc_status_code);
			$stmt->bindParam(":username", $this->username);
			$stmt->bindParam(":menu_owner", $this->menu_owner);

			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	// update the record
	function updateLastLogin(){

		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						last_login = :last_login
					WHERE
						username = :username";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->last_login=htmlspecialchars(strip_tags($this->last_login));
			$this->username=htmlspecialchars(strip_tags($this->username));
			
			// bind values
			$stmt->bindParam(":last_login", $this->last_login);
			$stmt->bindParam(":username", $this->username);

			// execute the query
			if($stmt->execute()){
				echo $this->last_login ." - ".$this->username;
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	// change password
	function changePassword(){

		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						password = :password
					WHERE
						username = :username";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->password=htmlspecialchars(strip_tags($this->password));
			$this->username=htmlspecialchars(strip_tags($this->username));
			
			// bind values
			$stmt->bindParam(":password", $this->password);
			$stmt->bindParam(":username", $this->username);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	// change acccount status
	function changeStatus(){

		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						acc_status_code = :acc_status_code
					WHERE
						username = :username";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->acc_status_code=htmlspecialchars(strip_tags($this->acc_status_code));
			$this->username=htmlspecialchars(strip_tags($this->username));
			
			// bind values
			$stmt->bindParam(":acc_status_code", $this->acc_status_code);
			$stmt->bindParam(":username", $this->username);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE username = ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->username=htmlspecialchars(strip_tags($this->username));

		// bind id of record to delete
		$stmt->bindParam(1, $this->username);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}


	// function deleteByRegNo(){
	// 	// delete query
	// 	$query = "DELETE FROM " . $this->table_name . " WHERE reg_no = ?";
	// 	// prepare query
	// 	$stmt = $this->conn->prepare($query);
	// 	// sanitize
	// 	$this->reg_no=htmlspecialchars(strip_tags($this->reg_no));
	// 	// bind id of record to delete
	// 	$stmt->bindParam(1, $this->reg_no);
	// 	// execute query
	// 	if($stmt->execute()){
	// 		return true;
	// 	}
	// 	return false;
	// }
	
	function randomPassword($length)
	{
	   $string = "";
	   $chars = "abcdefghijkmanopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ0123456789";
	   $size = strlen($chars);
	   for ($i = 0; $i < $length; $i++) {
		   $string .= $chars[rand(0, $size - 1)];
	   }
	   return $string; 
	}

	function encryptPassword($source){
		return hash('sha256',$source);
	}


	// search records
	function search($keywords){

		// select all query
		$query = "SELECT
			u.*, p.name, p.email
		FROM
			user_account u , person p
			where  u.ic_no = p.ic_no
			and (LOWER(u.username) like ? or LOWER(p.name) like ?)
		ORDER BY
			p.name";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$keywords= strtolower(htmlspecialchars(strip_tags($keywords)));
		$keywords = "%{$keywords}%";
		
		// bind
		$stmt->bindParam(1, $keywords);
		$stmt->bindParam(2, $keywords);
	
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}

	function getNextId(){
		$query = "update account_id set id=(id+1)" ;
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$query = "select id from account_id" ;
		$stmt = $this->conn->prepare($query);

		if ($stmt->execute()){
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			return $row['id'];
		}else{
			return -1;
		}	
	}

	// Enchanment 

	function readByEmail($email){
		// select all query
		$query = "SELECT * FROM " . $this->table_name . "  where email = :email";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	
		// bind code of data to be updated
		$stmt->bindParam(":email", $email);
	  
		// execute query
		$stmt->execute();
	
		$row = $stmt->fetch(PDO::FETCH_ASSOC) ;
	
		$userAccount_item=null;
	
		if (isset($row['email'])){
			$userAccount_item=array(
				"username" => $row['username'],
				"email" => $row['email'],
				"ic_no" => $row['ic_no'],
				"acc_type_code" => $row['acc_type_code'],
				"acc_status_code" =>  $row['acc_status_code'],
				// "reg_no" => $row['reg_no'],
				"menu_owner" => $row['menu_owner'],
				"date_created" => $row['date_created'],
				"date_updated" => $row['date_updated'],
				"last_login" => $row['last_login']
			);
		}
	
		return $userAccount_item;
	
	}



	// change password
	function changePasswordByEmail(){

		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						password = :password
					WHERE
						email = :email";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->password=htmlspecialchars(strip_tags($this->password));
			$this->email=htmlspecialchars(strip_tags($this->email));
			
			// bind values
			$stmt->bindParam(":password", $this->password);
			$stmt->bindParam(":email", $this->email);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}
	
	

}

?>