<?php


include_once 'package-test-groups.php';
include_once 'package-patient.php';
include_once 'package-add-ons.php';

class ScreeningPlan{
  
    // database connection and table name
    private $conn;
    private $table_name = "screening_packages";

    // object properties
    
	public $package_code;
	public $description;
	public $single_package;
	public $category_code;
	public $picture_path;
	public $price;
	public $license_validity_year;
	public $test_included;
	public $note;
	public $commercial;


  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					package_code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}

	// read all records
	function readAllByCommercialCategory(){
	
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "
				WHERE commercial=:commercial  
				and category_code=:category_code
				ORDER BY
					package_code";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		$stmt->bindParam(":commercial", $this->commercial);
		$stmt->bindParam(":category_code", $this->category_code);
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}

	// read all records
	function readAllByCommercial(){

		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "
				WHERE commercial=:commercial  
				ORDER BY
					package_code";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		$stmt->bindParam(":commercial", $this->commercial);
		// $stmt->bindParam(":category_code", $this->category_code);
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				 package_code=:package_code, single_package=:single_package, 
				 category_code=:category_code,description=:description, picture_path=:picture_path, price=:price, license_validity_year=:license_validity_year,
				 test_included=:test_included, note=:note, commercial=:commercial
				 ";
		

		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		
		$this->package_code=htmlspecialchars(strip_tags($this->package_code));
		$this->single_package=htmlspecialchars(strip_tags($this->single_package));
		$this->category_code=htmlspecialchars(strip_tags($this->category_code));
		$this->description=htmlspecialchars(strip_tags($this->description));
		$this->picture_path=htmlspecialchars(strip_tags($this->picture_path));
		$this->price=htmlspecialchars(strip_tags($this->price));
		$this->license_validity_year=htmlspecialchars(strip_tags($this->license_validity_year));
		$this->test_included=htmlspecialchars(strip_tags($this->test_included));
		$this->commercial=htmlspecialchars(strip_tags($this->commercial));

		
		
		// bind values
	
		$stmt->bindParam(":package_code", $this->package_code);
		$stmt->bindParam(":single_package", $this->single_package);
		$stmt->bindParam(":category_code", $this->category_code);
		$stmt->bindParam(":description", $this->description);
		$stmt->bindParam(":picture_path", $this->picture_path);
		$stmt->bindParam(":price", $this->price);
		$stmt->bindParam(":license_validity_year", $this->license_validity_year);
		$stmt->bindParam(":test_included", $this->test_included);
		$stmt->bindParam(":note", $this->note);
		$stmt->bindParam(":commercial", $this->commercial);

		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}


	// used when filling up the update record form
	function readOne(){

		$this->package_code=htmlspecialchars(strip_tags($this->package_code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					package_code = :package_code
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated by primary key
		$stmt->bindParam(":package_code", $this->package_code);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->package_code = $row['package_code'];
		$this->single_package = $row['single_package']; 
		$this->category_code= $row['category_code'];
		$this->description= $row['description'];
		$this->picture_path= $row['picture_path'];
		$this->price= $row['price'];
		$this->license_validity_year= $row['license_validity_year'];
		$this->test_included= $row['test_included'];
		$this->note= $row['note'];
		$this->commercial= $row['commercial'];
	}

	//added by majina 18/08/2021
	function readByPackageCode($package_code){

		$packagePatient =  new PackagePatient($this->conn);
		$packageAddOns =  new PackageAddOns($this->conn);
		$packageTestGroups =  new PackageTestGroups($this->conn);
		//$packageTestPanels =  new PackageTestPanels($this->conn);

		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					package_code = :package_code
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated by primary key
		$stmt->bindParam(":package_code", $package_code);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		$package_item=array(
			"package_code" => $row['package_code'],
			"description" =>  $row['description'],
			"single_package" =>  $row['single_package'],
			"category_code" =>  $row['category_code'],
			"picture_path" =>  $row['picture_path'],
			"price" =>  $row['price'],
			"license_validity_year" =>  $row['license_validity_year'],
			"test_included" =>  $row['test_included'],
			"note" =>  $row['note'],
			"commercial" =>  $row['commercial'],
			"package_patients" => $packagePatient->readByPackageCode($package_code),
			"package_test_groups"=> $packageTestGroups->readByPackageCode($package_code),
			"package_add_ons"=> $packageAddOns->readByPackageCode($package_code)
			//"package_test_panels"=> $packageTestPanels->readByPackageCode($package_code)
			
		);
 
		return $package_item;

	}
	
	//added by majina 23/08/2021
	function readByCategoryCode($category_code){

		$packagePatient =  new PackagePatient($this->conn);
		$packageAddOns =  new PackageAddOns($this->conn);
		$packageTestGroups =  new PackageTestGroups($this->conn);

		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					category_code = :category_code
				order by package_code";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated by primary key
		$stmt->bindParam(":category_code", $category_code);
		
		// execute query
		$stmt->execute();
		
		$arr=array();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record_item=array(
				"package_code" => $package_code,
				"single_package" => $single_package,
				"category_code" => $category_code,
				"description" =>  $description,
				"picture_path" => $picture_path,
				"price" => $price,  
				"license_validity_year" => $license_validity_year,
				"test_included" => $test_included,
				"note" => $note,
				"commercial" =>  $row['commercial']
			);
			array_push($arr, $record_item);
		}
	  
		return $arr;
 
	}

 
	// update the record except primary key Code
	function update(){
		
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
					    single_package=:single_package, 
						category_code=:category_code, 
						description=:description, 
						price=:price, 
						license_validity_year=:license_validity_year,
						test_included=:test_included, note=:note,
						commercial=:commercial				
					WHERE
						package_code = :package_code";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
				
			$this->package_code=htmlspecialchars(strip_tags($this->package_code));
			$this->single_package=htmlspecialchars(strip_tags($this->single_package));
			$this->category_code=htmlspecialchars(strip_tags($this->category_code));
			$this->description=htmlspecialchars(strip_tags($this->description));
			$this->price=htmlspecialchars(strip_tags($this->price));
			$this->license_validity_year=htmlspecialchars(strip_tags($this->license_validity_year));
			$this->test_included=htmlspecialchars(strip_tags($this->test_included));
			$this->commercial=htmlspecialchars(strip_tags($this->commercial));


			// bind values
			
			$stmt->bindParam(":package_code", $this->package_code);
			$stmt->bindParam(":single_package", $this->single_package);
			$stmt->bindParam(":category_code", $this->category_code);
			$stmt->bindParam(":description", $this->description);
			$stmt->bindParam(":price", $this->price);
			$stmt->bindParam(":license_validity_year", $this->license_validity_year);
			$stmt->bindParam(":test_included", $this->test_included);
			$stmt->bindParam(":note", $this->note);
			$stmt->bindParam(":commercial", $this->commercial);

			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	
	// update the record
	function updatePhoto(){

		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						picture_path=:picture_path 
					WHERE
						package_code = :package_code";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->package_code=htmlspecialchars(strip_tags($this->package_code));
			$this->picture_path=htmlspecialchars(strip_tags($this->picture_path));
			
			// bind values
			$stmt->bindParam(":package_code", $this->package_code);
			$stmt->bindParam(":picture_path", $this->picture_path);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	// delete the record based on the primary key, add PK if more than 1
	function delete(){
		
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE package_code= ?";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->package_code=htmlspecialchars(strip_tags($this->package_code));
		
		// bind id of record to delete
		$stmt->bindParam(1, $this->package_code);
		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
				

	
	// search records
	function search($keywords){
		
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					package_code LIKE ? OR single_package LIKE ?  
				ORDER BY
				package_code";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$keywords=htmlspecialchars(strip_tags($keywords));
		$keywords = "%{$keywords}%";
		
		// bind
		$stmt->bindParam(1, $keywords);
		$stmt->bindParam(2, $keywords);
	
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}


}

?>