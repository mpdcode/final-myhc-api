<?php

$message = '';


function fetch_data()
{
 $connect = new PDO("mysql:host=localhost;dbname=myhclive_appsmyhc", "myhclive_admin", "Janu*2022");
 $query = "SELECT * FROM booking";
 $statement = $connect->prepare($query);
 $statement->execute();
 $result = $statement->fetchAll();
 $output = '<style>'.file_get_contents($_SERVER['DOCUMENT_ROOT'].'myhc-api/bootstrap/bootstrap.min.css').'</style>';
 $output .= '
  
 <div class="table-responsive">
  <table class="table table-striped table-bordered">
   <tr>
    <th>Name</th>
    <th>Address</th>
    <th>City</th>
    <th>Postal Code</th>
    <th>Country</th>
   </tr>
 ';
 foreach($result as $row)
 {
  $output .= '
   <tr>
    <td>'.$row["ic_no"].'</td>
    <td>'.$row["booking_no"].'</td>
    <td>'.$row["screening_center_id"].'</td>
    <td>'.$row["screening_date"].'</td>
    <td>'.$row["screening_time"].'</td>
   </tr>
  ';
 }
 $output .= '
  </table>
 </div>
 ';
 return $output;
}

require_once('../../tcpdf_min/tcpdf.php');  
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);  
$obj_pdf->SetCreator(PDF_CREATOR);  
$obj_pdf->SetTitle("Receipt");  
$obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);  
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));  
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));  
$obj_pdf->SetDefaultMonospacedFont('helvetica');  
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);  
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);  
$obj_pdf->setPrintHeader(false);  
$obj_pdf->setPrintFooter(false);  
$obj_pdf->SetAutoPageBreak(TRUE, 10);  
$obj_pdf->SetFont('helvetica', '', 12);  
$obj_pdf->AddPage();  
$content = '';  
$content .= fetch_data();  
$obj_pdf->writeHTML($content);  
$obj_pdf->Output('receipt.pdf', 'I'); 

?>