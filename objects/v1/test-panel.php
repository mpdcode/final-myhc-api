<?php

class TestPanel{
  
    // database connection and table name
    private $conn;
    private $table_name = "test_panel";

    // object properties
    public $panel_id;
    public $test_panel_code;
    public $name;
	public $description;
	public $input_type;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					panel_id";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
					panel_id=:panel_id, test_panel_code=:test_panel_code, 
					name=:name, description=:description, input_type=:input_type";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->panel_id=htmlspecialchars(strip_tags($this->panel_id));
		$this->test_panel_code=htmlspecialchars(strip_tags($this->test_panel_code));
		$this->name=htmlspecialchars(strip_tags($this->name));
		$this->description=htmlspecialchars(strip_tags($this->description));
		$this->input_type=htmlspecialchars(strip_tags($this->input_type));
		
		// bind values
		$stmt->bindParam(":panel_id", $this->panel_id);
		$stmt->bindParam(":test_panel_code", $this->test_panel_code);
		$stmt->bindParam(":name", $this->name);
		$stmt->bindParam(":description", $this->description);
		$stmt->bindParam(":input_type", $this->input_type);

		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}
		
		
	// used when filling up the update record form
	function readOne(){

		$this->test_panel_code=htmlspecialchars(strip_tags($this->test_panel_code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					test_panel_code = :test_panel_code
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":test_panel_code", $this->test_panel_code);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->panel_id = $row['panel_id'];
		$this->test_panel_code = $row['test_panel_code'];
		$this->name = $row['name']; 
		$this->description = $row['description'];
		$this->input_type = $row['input_type'];
	}

	// used when filter by test_panel_code
	function readByTestPanelCode($test_panel_code){

		$this->test_panel_code=htmlspecialchars(strip_tags($test_panel_code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					test_panel_code = :test_panel_code
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":test_panel_code", $this->test_panel_code);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
 		$record=array(
			"panel_id" => $row['panel_id'],
			"test_panel_code" =>  $row['test_panel_code'],
			"name" => $row['name'],
			"description" => $row['description'],
			"input_type" => $row['input_type']
		);

		return $record;
	}

	// used when filter by test_group -> test_group_panel -> test_group
	function readByTestGroupCode($test_group_code){

		$test_group_code=htmlspecialchars(strip_tags($test_group_code));
		
		// query to read single record
		$query = "SELECT
					p.*
				FROM
					" . $this->table_name . " p , test_group_panel g
				WHERE
					g.test_group_code = :test_group_code
					and p.test_panel_code = g.test_panel_code";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":test_group_code", $test_group_code);
		
		// execute query
		$stmt->execute();
		
		$arr=array();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record_item=array(
				"panel_id" => $panel_id,
				"test_panel_code" => $test_panel_code,
				"name" => $name,
				"description" => $description,
				"input_type" => $input_type 
			);
			array_push($arr, $record_item);
		}
	  
		return $arr;
 
	}	
	

	// update the record
	function update(){
		
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						name = :name,
						description = :description ,
						input_type = :input_type 
					WHERE
					test_panel_code = :test_panel_code";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->test_panel_code=htmlspecialchars(strip_tags($this->test_panel_code));
			$this->name=htmlspecialchars(strip_tags($this->name));
			$this->description=htmlspecialchars(strip_tags($this->description));
			$this->input_type=htmlspecialchars(strip_tags($this->input_type));
			
			// bind values
			$stmt->bindParam(":test_panel_code", $this->test_panel_code);
			$stmt->bindParam(":name", $this->name);
			$stmt->bindParam(":description", $this->description);
			$stmt->bindParam(":input_type", $this->input_type);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	// delete the record
	function delete(){
		
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE test_panel_code = ?";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->test_panel_code=htmlspecialchars(strip_tags($this->test_panel_code));
		
		// bind id of record to delete
		$stmt->bindParam(1, $this->test_panel_code);
		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	// search records
	function search($keywords){
		
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					test_panel_code LIKE ? OR name LIKE ?  
				ORDER BY
					test_panel_code";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$keywords=htmlspecialchars(strip_tags($keywords));
		$keywords = "%{$keywords}%";
		
		// bind
		$stmt->bindParam(1, $keywords);
		$stmt->bindParam(2, $keywords);
	
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}


}

?>