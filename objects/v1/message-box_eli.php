<?php
class MessageBox{

    //database connection and table-name
    private $conn;
    private $tablename = "message_inbox";

    //object properties
    private $message_inbox_code; //PK
    private $sender;
    private $receiver;
    private $subject;
    private $message;
    private $headers;
    private $date_sent;
    private $message_type_code; //PK :  SUPPORT, ENQUIRY, INBOX
    private $ic_no; 
    private $status; 
    private $attachment;

    //for sending messages properties
	public $paramValues;
	public $receivers;

    //constructor with $db
    public function __construct($db){
        $this->conn = $db;
    }

    // read all records for receiver
    function read-by-receiver(){
        // select all query
		$query = "SELECT
                    *
                FROM
                    " . $this->table_name . "  
                ORDER BY
                receiver";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }


        // read all records for message-type
        function read-by-message-type(){
            // select all query
            $query = "SELECT
                        *
                    FROM
                        " . $this->table_name . "  
                    ORDER BY
                    message_type_code";
    
            // prepare query statement
            $stmt = $this->conn->prepare($query);
    
            // execute query
            $stmt->execute();
    
            return $stmt;
        }

        function sendMail(){
            $content = $this->message;
    
            foreach ($this->paramValues as $paramValue) {
                $content = str_replace($paramValue['param'],$paramValue['value'],$message);
            }
    
            if (mail($this->receiver,$this->title,$message,"From: ". $this->sender)){
                return true;
            }else{
                return false;
            }
        }


        
    function create(){
            $query = "INSERT INTO 
                        " . $this->table_name . "
                        
                        SET
                        message_inbox_code=:message_inbox_code,
                        sender=:sender, receiver=:receiver, subject=:subject,
                        message=:message, headers=:headers, date_sent=:date_sent,
                        message_type_code=:message_type_code, ic_no=:ic_no,
                        status=:status, attachment
                        
                        ";

            //prepare query
            $stmt = $this->conn->prepare($query);
        
            // sanitize
            
            $this->message_inbox_code=htmlspecialchars(strip_tags($this->message_inbox_code));
            $this->sender=htmlspecialchars(strip_tags($this->sender));
            $this->receiver=htmlspecialchars(strip_tags($this->receiver));
            $this->subject=htmlspecialchars(strip_tags($this->subject));
            $this->message=htmlspecialchars(strip_tags($this->message));
            $this->headers=htmlspecialchars(strip_tags($this->headers));
            $this->date_sent=htmlspecialchars(strip_tags($this->date_sent));
            $this->message_type_code=htmlspecialchars(strip_tags($this->message_type_code));
            $this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
            $this->status=htmlspecialchars(strip_tags($this->status));
            $this->attachment=htmlspecialchars(strip_tags($this->attachment));
    

            // bind values
	
            $stmt->bindParam(":message_inbox_code", $this->message_inbox_code);
            $stmt->bindParam(":sender", $this->sender);
            $stmt->bindParam(":receiver", $this->receiver);
            $stmt->bindParam(":subject", $this->subject);
            $stmt->bindParam(":message", $this->message);
            $stmt->bindParam(":headers", $this->headers);	
            $stmt->bindParam(":date_sent", $this->date_sent);
            $stmt->bindParam(":message_type_code", $this->message_type_code);
            $stmt->bindParam(":ic_no", $this->ic_no);			
            $stmt->bindParam(":status", $this->status);
            $stmt->bindParam(":attachment", $this->attachment);			
			
				

		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
    }


    // read all records
    function readAll(){
        // select all query
		$query = "SELECT
                    *
                FROM
                    " . $this->table_name . "  
                ORDER BY
                message_inbox_code";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }
    
    

    function readOne() {
        // query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
                    message_inbox_code =:message_inbox_code and
					message_type_code =:message_type_code 
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated by primary key, 2 PK
		$stmt->bindParam(":message_inbox_code", $this->message_inbox_code);
		$stmt->bindParam(":message_type_code", $this->message_type_code);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		


		// set values to object properties
		$this->message_inbox_code = $row['message_inbox_code'];
		$this->sender = $row['sender']; 
		$this->receiver= $row['receiver'];
        $this->subject = $row['subject'];
		$this->message = $row['message']; 
		$this->headers= $row['headers'];
        $this->date_sent = $row['date_sent'];
		$this->message_type_code = $row['message_type_code']; 
		$this->ic_no= $row['ic_no'];
        $this->status = $row['status']; 
		$this->attachment= $row['attachment'];

    }



    message_inbox_code
    sender
    receiver
    subject
    message
    headers
    date_sent
    message_type_code
    ic_no
    status
    attachment

    // delete the record based on the primary key, add PK if more than 1
	function delete(){
		
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE message_inbox_code = ? and message_type_code = ?";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->message_inbox_code=htmlspecialchars(strip_tags($this->message_inbox_code));
		$this->message_type_code=htmlspecialchars(strip_tags($this->message_type_code));

		// bind id of record to delete
		$stmt->bindParam(1, $this->message_inbox_code);
		$stmt->bindParam(2, $this->message_type_code);

		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}

   // search records
	function search($keywords){
		
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
                    message_inbox_code LIKE ? OR message_type_code LIKE ?  
				ORDER BY
				message_inbox_code";

		
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$keywords=htmlspecialchars(strip_tags($keywords));
		$keywords = "%{$keywords}%";
		
		// bind
		$stmt->bindParam(1, $keywords);
		$stmt->bindParam(2, $keywords);
	

		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}

    function update(){

    }

    function delete(){

    }



}
?>