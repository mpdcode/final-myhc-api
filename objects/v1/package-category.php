<?php
 

class PackageCategory{
  
    // database connection and table name
    private $conn;
    private $table_name = "package_category";

    // object properties
    public $category_code;
	public $description;
	public $picture_path;
	public $prefix;
	 
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					category_code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}

	// read all records
	function readShow(){
	
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . " 
			 	where show_display =1
				ORDER BY
					category_code";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}


	// used when filling up the update record form
	function readOne(){

		$this->category_code=htmlspecialchars(strip_tags($this->category_code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					category_code = :category_code
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":category_code", $this->category_code);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->category_code = $row['category_code'];
		$this->description = $row['description'];
		$this->picture_path = $row['picture_path'];
		$this->prefix = $row['prefix'];
	}

	// used when filling up the update record form
	function readByCategoryCode($code){

		$this->category_code=htmlspecialchars(strip_tags($code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					category_code = :category_code
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":category_code", $this->category_code);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$record=array(
			"category_code" => $row['category_code'],
			"description" => $row['description'],
			"picture_path" => $row['picture_path'],
			"prefix" => $row['prefix']
		);

		return $record;
	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				category_code=:category_code,  description=:description, 
				picture_path=:picture_path, prefix=:prefix";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->category_code=htmlspecialchars(strip_tags($this->category_code));
		$this->description=htmlspecialchars(strip_tags($this->description));
		$this->picture_path=htmlspecialchars(strip_tags($this->picture_path));
		
		// bind values
		$stmt->bindParam(":category_code", $this->category_code);
		$stmt->bindParam(":description", $this->description);
		$stmt->bindParam(":picture_path", $this->picture_path);
		$stmt->bindParam(":prefix", $this->prefix);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	
	// update the record
	function update(){
	
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						description = :description,
						picture_path = :picture_path,
						prefix=:prefix
					WHERE
						category_code = :category_code";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->category_code=htmlspecialchars(strip_tags($this->category_code));
			$this->description=htmlspecialchars(strip_tags($this->description));
			$this->picture_path=htmlspecialchars(strip_tags($this->picture_path));
			$this->prefix=htmlspecialchars(strip_tags($this->prefix));
			
			// bind values
			$stmt->bindParam(":category_code", $this->category_code);
			$stmt->bindParam(":description", $this->description);
			$stmt->bindParam(":picture_path", $this->picture_path);
			$stmt->bindParam(":prefix", $this->prefix);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE category_code = ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->category_code=htmlspecialchars(strip_tags($this->category_code));

		// bind id of record to delete
		$stmt->bindParam(1, $this->category_code);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}
	
}

?>