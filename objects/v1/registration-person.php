<?php

include_once 'person.php';
include_once 'user-account.php';
include_once 'pre-registration.php';
include_once 'trans-log.php';

class RegistrationPerson{
  
    // database connection and table name
    private $conn;
    private $table_name = "registration_person";

    // object properties
    public $reg_no;
	public $ic_no;
	public $person_type_code;
	public $admin_type;
	public $patient_type_code;
	public $username;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					reg_no";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}

	// used when filling up the update record form
	function readOne(){

		$this->reg_no=htmlspecialchars(strip_tags($this->reg_no));
		$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					reg_no = :reg_no and
					ic_no = :ic_no
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":reg_no", $this->reg_no);
		$stmt->bindParam(":ic_no", $this->ic_no);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->reg_no = $row['reg_no'];
		$this->ic_no = $row['ic_no'];
		$this->person_type_code = $row['person_type_code'];
		$this->admin_type = $row['admin_type'];
		$this->patient_type_code = $row['patient_type_code'];
	}

	// used when filling up the update record form
	function readByPatientStatus(){

		$this->reg_no=htmlspecialchars(strip_tags($this->reg_no));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					person_type_code = 'PATIENT' and
					ic_no = :ic_no
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":ic_no", $this->ic_no);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->reg_no = $row['reg_no'];
		$this->ic_no = $row['ic_no'];
		$this->person_type_code = $row['person_type_code'];
		$this->admin_type = $row['admin_type'];
		$this->patient_type_code = $row['patient_type_code'];
	}

	// read all records
	function readAllPatients(){
	
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . " 
				where person_type_code = 'PATIENT' 
				ORDER BY
					reg_no";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}

	// used to check whether the person has been registered to a package plan as a patient
	function hasRegisteredToPackagePlan(){

		$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					ic_no = :ic_no and person_type_code = 'PATIENT'
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":ic_no", $this->ic_no);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
 
		$this->reg_no = $row['reg_no'] ?? null;
		$this->ic_no = $row['ic_no'] ?? null;
		$this->person_type_code = $row['person_type_code'] ?? null;
		$this->admin_type = $row['admin_type'] ?? null;
		$this->patient_type_code = $row['patient_type_code'] ?? null;
 

	}
	

	function readByIcNo($ic_no){

		$preRegistration =  new PreRegistration($this->conn);
		$userAccount =  new UserAccount($this->conn);

		// select all query
		$query = "SELECT * FROM " . $this->table_name . "  where ic_no=:ic_no order by reg_no, patient_type_code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind code of data to be updated
		$stmt->bindParam(":ic_no", $ic_no);
	  
		// execute query
		$stmt->execute();

		$arr=array();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$userAccount->ic_no = $ic_no;
			$regPerson_item=array(
				"ic_no" => $ic_no,
				"reg_no" => $reg_no,
				"person_type_code" => $person_type_code,
				"admin_type" => $admin_type,
				"patient_type_code" => $patient_type_code,
				"registration" => $preRegistration->readOne_array($reg_no),
				"user_account" => $userAccount->readOneByIcNo_array()
			);
			array_push($arr, $regPerson_item);
		}
 

		return $arr;

	}

	
	function readByIcNoWithPatientStatus($ic_no){

		// select all query
		$query = "SELECT * FROM " . $this->table_name . "  where ic_no=:ic_no and person_type_code ='PATIENT'  ";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind code of data to be updated
		$stmt->bindParam(":ic_no", $ic_no);
	  
		// execute query
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		// set values to object properties
		$this->reg_no = $row['reg_no'] ?? null;
		$this->ic_no = $row['ic_no'] ?? null;
		$this->person_type_code = $row['person_type_code'] ?? null;
		$this->admin_type = $row['admin_type'] ?? null;
		$this->patient_type_code = $row['patient_type_code'] ?? null;

	}

	function readByRegNo($reg_no){

		$person =  new Person($this->conn);
		$userAccount =  new UserAccount($this->conn);

		// select all query
		$query = "SELECT * FROM " . $this->table_name . "  where reg_no=:reg_no order by reg_no, patient_type_code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind code of data to be updated
		$stmt->bindParam(":reg_no", $reg_no);
	  
		// execute query
		$stmt->execute();

		$arr=array();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$userAccount->ic_no = $ic_no;
			$regPerson_item=array(
				"ic_no" => $ic_no,
				"reg_no" => $reg_no,
				"person_type_code" => $person_type_code,
				"admin_type" => $admin_type,
				"patient_type_code" => $patient_type_code,
				"person" => $person->readByIcNo($ic_no),
				"user_account" => $userAccount->readOneByIcNo_array($ic_no)
			);
			array_push($arr, $regPerson_item);
		}
 

		return $arr;

	}

	function readByRegNoAndPerson_Stmt($reg_no){

		$person =  new Person($this->conn);
		$userAccount =  new UserAccount($this->conn);

		// select all query
		$query = "SELECT pr.*, p.mobile_no, p.email, p.name 
		FROM registration_person pr, person p  where p.ic_no = pr.ic_no and pr.reg_no=:reg_no order by pr.reg_no, pr.admin_type";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind code of data to be updated
		$stmt->bindParam(":reg_no", $reg_no);
	  
		// execute query
		$stmt->execute();

     	return $stmt;

	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
					reg_no=:reg_no,  ic_no=:ic_no, 
					person_type_code=:person_type_code, admin_type= :admin_type ,patient_type_code= :patient_type_code  ";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->reg_no=htmlspecialchars(strip_tags($this->reg_no));
		$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
		$this->person_type_code=htmlspecialchars(strip_tags($this->person_type_code));
		$this->admin_type=htmlspecialchars(strip_tags($this->admin_type));
		$this->patient_type_code=htmlspecialchars(strip_tags($this->patient_type_code));
		
		// bind values
		$stmt->bindParam(":reg_no", $this->reg_no);
		$stmt->bindParam(":ic_no", $this->ic_no);
		$stmt->bindParam(":person_type_code", $this->person_type_code);
		$stmt->bindParam(":admin_type", $this->admin_type);
		$stmt->bindParam(":patient_type_code", $this->patient_type_code);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	
	// update the record
	function update(){
	
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						person_type_code = :person_type_code , patient_type_code=:patient_type_code
					WHERE
						reg_no = :reg_no and ic_no = :ic_no";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->reg_no=htmlspecialchars(strip_tags($this->reg_no));
			$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
			$this->person_type_code=htmlspecialchars(strip_tags($this->person_type_code));
			$this->admin_type=htmlspecialchars(strip_tags($this->admin_type));
			$this->patient_type_code=htmlspecialchars(strip_tags($this->patient_type_code));
			
			// bind values
			$stmt->bindParam(":reg_no", $this->reg_no);
			$stmt->bindParam(":ic_no", $this->ic_no);
			$stmt->bindParam(":person_type_code", $this->person_type_code);
			$stmt->bindParam(":admin_type", $this->admin_type);
			$stmt->bindParam(":patient_type_code", $this->patient_type_code);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	// update the record
	function updateUsername(){
		$transLog = new TransLog($this->conn);
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						username = :username  
					WHERE
						reg_no = :reg_no and ic_no = :ic_no";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->reg_no=htmlspecialchars(strip_tags($this->reg_no));
			$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
			$this->username=htmlspecialchars(strip_tags($this->username));
			
			// bind values
			$stmt->bindParam(":reg_no", $this->reg_no);
			$stmt->bindParam(":ic_no", $this->ic_no);
			$stmt->bindParam(":username", $this->username);
			
			// execute the query
			if($stmt->execute()){
				$transLog->activity="Update username " . $this->username . " in registration person -  ".$this->reg_no;
				$transLog->username="admin";
				$transLog->status="success";
				$transLog->create();
				return true;
			}else{
				$transLog->activity="Update username " . $this->username . " in registration person -  ".$this->reg_no;
				$transLog->username="admin";
				$transLog->status="failed";
				$transLog->create();
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	// update the record
	function updatePatientTypeCodeByAge($age){

		try{
			$this->patient_type_code = $this->getPatientTypeCode($age);

			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						patient_type_code = :patient_type_code 
					WHERE
						reg_no = :reg_no and ic_no = :ic_no";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->reg_no=htmlspecialchars(strip_tags($this->reg_no));
			$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
			$this->patient_type_code=htmlspecialchars(strip_tags($this->patient_type_code));
			
			// bind values
			$stmt->bindParam(":reg_no", $this->reg_no);
			$stmt->bindParam(":ic_no", $this->ic_no);
			$stmt->bindParam(":patient_type_code", $this->patient_type_code);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE reg_no = ? and ic_no =?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->reg_no=htmlspecialchars(strip_tags($this->reg_no));
		$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));

		// bind id of record to delete
		$stmt->bindParam(1, $this->reg_no);
		$stmt->bindParam(2, $this->ic_no);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}

	
	function deleteByRegNo(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE reg_no = ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->reg_no=htmlspecialchars(strip_tags($this->reg_no));

		// bind id of record to delete
		$stmt->bindParam(1, $this->reg_no);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}

	function getPatientTypeCode($age){
		if ($age >= 18){
			return 'ADULT';
		}else if ($age < 60) {
			return 'CHILD';
		}else return 'ELDERLY';
	}
	
}

?>