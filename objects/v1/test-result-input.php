<?php

include_once 'test-marker.php';

class TestResultInput{
  
    // database connection and table name
    private $conn;
    private $table_name = "test_result";

    // object properties
    public $patient_ic_no;
	public $booking_no;
	public $reg_no;
    public $test_marker_code;
	public $test_panel_code;
    public $test_date;
	public $test_value;
	public $source;
	public $date_updated;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
				patient_ic_no,test_date,test_marker_code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}

	// read all records
	function readAllResult(){
	
		// select all query
		$query = "SELECT
					tr.*, p.name as patient_name, tm.name as marker_name, tp.name as panel_name, tm.unit
				FROM
					test_result tr, person p, test_marker tm, test_panel tp
				WHERE
					tr.patient_ic_no = p.ic_no
					and tm.code = tr.test_marker_code
					and tr.test_panel_code = tp.test_panel_code
				ORDER BY
				patient_ic_no,test_date,tp.panel_id,tr.test_marker_code";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// execute query
		$stmt->execute();
		
		$data=array();
		$subdata = array();
		
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$grp_ic_no = $patient_ic_no;
			if ( !isset( $data[$grp_ic_no] ) ) {
				$data[$grp_ic_no]= array( "name" => $patient_name, "ic_no" => $patient_ic_no);
			}
			if ( !isset( $data[$grp_ic_no]['test_date#'.$test_date] ) ) {
				$data[$grp_ic_no]['test_date#'.$test_date] = array( "test_date" => $test_date, "source" => $source);
			}
			if ( !isset( $data[$grp_ic_no]['test_date#'.$test_date]['test_panel_code#'.$test_panel_code] ) ) {
				$data[$grp_ic_no]['test_date#'.$test_date]['test_panel_code#'.$test_panel_code] = array(  "test_panel_code" => $test_panel_code);
			}
			$data[$grp_ic_no]['test_date#'.$test_date]['test_panel_code#'.$test_panel_code]['test_markers'][] = array("patient_ic_no" => $patient_ic_no,"test_date" => $test_date,"test_panel_code" => $test_panel_code,"test_panel_name" => $panel_name, "test_marker_code"=>$test_marker_code,"test_marker_name"=>$marker_name,"test_value"=>$test_value,"test_unit"=>$unit,"source"=>$source);
		}
 

		return $data;
	}

	// read all records
	function search($keywords){

		// select all query
		$query = "SELECT
					tr.*, p.name as patient_name, tm.name as marker_name, tp.name as panel_name, tm.unit, tp.panel_id
				FROM
					test_result tr, person p, test_marker tm, test_panel tp
				WHERE
					tr.patient_ic_no = p.ic_no
					and tm.code = tr.test_marker_code
					and tm.test_panel_code = tr.test_panel_code
					and tr.test_panel_code = tp.test_panel_code
					and (p.ic_no like ? or tr.test_date like ?)
				ORDER BY
				patient_ic_no,test_date,tp.panel_id,tr.test_marker_code";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		$keywords= strtolower(htmlspecialchars(strip_tags($keywords)));
		$keywords = "%{$keywords}%";

		// bind
		$stmt->bindParam(1, $keywords);
		$stmt->bindParam(2, $keywords);
		
		// execute query
		$stmt->execute();
		
		$data=array();
		$subdata = array();
		
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$grp_ic_no = $patient_ic_no;
			if ( !isset( $data[$grp_ic_no] ) ) {
				$data[$grp_ic_no]= array( "name" => $patient_name, "ic_no" => $patient_ic_no);
			}
			if ( !isset( $data[$grp_ic_no]['test_date#'.$test_date] ) ) {
				$data[$grp_ic_no]['test_date#'.$test_date] = array( "test_date" => $test_date, "source" => $source);
			}
			if ( !isset( $data[$grp_ic_no]['test_date#'.$test_date]['test_panel_id#'.$panel_id] ) ) {
				$data[$grp_ic_no]['test_date#'.$test_date]['test_panel_id#'.$panel_id] = array(  "test_panel_code" => $test_panel_code,"test_panel_name" => $panel_name);
			}
			$data[$grp_ic_no]['test_date#'.$test_date]['test_panel_id#'.$panel_id]['test_markers'][] = array("patient_ic_no" => $patient_ic_no,"test_date" => $test_date,"test_panel_code" => $test_panel_code,"test_panel_name" => $panel_name, "test_marker_code"=>$test_marker_code, "test_marker_name"=>$marker_name,"test_value"=>$test_value,"test_unit"=>$unit,"source"=>$source);
		}
		return $data;
	}

	// read all records with reference range
	function resultWithRefByIcNo($icno){

		// select all query
		$query = "SELECT
					tr.*, p.name as patient_name, tm.name as marker_name, tp.name as panel_name, tm.unit, tp.panel_id
				FROM
					test_result tr, person p, test_marker tm, test_panel tp
				WHERE
					tr.patient_ic_no = p.ic_no
					and tm.code = tr.test_marker_code
					and tm.test_panel_code = tr.test_panel_code
					and tr.test_panel_code = tp.test_panel_code
					and p.ic_no = ?
				ORDER BY
				patient_ic_no,test_date,tp.panel_id,tr.test_marker_code";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind
		$stmt->bindParam(1, $icno);
		
		// execute query
		$stmt->execute();
		
		$data=array();
		$subdata = array();

		$testMarker = new TestMarker($this->conn);
		
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);

			$test_marker_arr = $testMarker->readByPanelCode($test_panel_code);

			$ref_value = null;
			foreach ($test_marker_arr as $test_marker_item) {
				$tmcode = $test_marker_item['code'];
				if ($test_marker_code==$tmcode){
					$ref_arr = $test_marker_item['test_reference_range'];
					foreach ($ref_arr as $ref_item) {
						$minval=0;
						$maxval=999;
						if ($ref_item['max']==$maxval){
							if ($test_value>$ref_item['min']) $ref_value = $ref_item['code'];
						}else{
							if ($ref_item['min']==$minval) {
								if ($test_value<$ref_item['max']) $ref_value = $ref_item['code'];
							}else{
								if ($test_value>=$ref_item['min'] && $test_value<=$ref_item['max']){
									$ref_value = $ref_item['code'];
								}
							}
						}

					}
				}
			}
			
			$grp_ic_no = $patient_ic_no;
			if ( !isset( $data[$grp_ic_no] ) ) {
				$data[$grp_ic_no]= array( "name" => $patient_name, "ic_no" => $patient_ic_no);
			}
			if ( !isset( $data[$grp_ic_no]['test_date#'.$test_date] ) ) {
				$data[$grp_ic_no]['test_date#'.$test_date] = array( "test_date" => $test_date, "source" => $source);
			}
			if ( !isset( $data[$grp_ic_no]['test_date#'.$test_date]['test_panel_id#'.$panel_id] ) ) {
				$data[$grp_ic_no]['test_date#'.$test_date]['test_panel_id#'.$panel_id] = array(  "test_panel_code" => $test_panel_code,"test_panel_name" => $panel_name);
			}
			$data[$grp_ic_no]['test_date#'.$test_date]['test_panel_id#'.$panel_id]['test_markers'][] = array("patient_ic_no" => $patient_ic_no,"test_date" => $test_date,"test_panel_code" => $test_panel_code,"test_panel_name" => $panel_name, "test_marker_code"=>$test_marker_code, "test_marker_name"=>$marker_name,"test_value"=>$test_value,"test_unit"=>$unit,"source"=>$source,"ref_value"=>$ref_value);
		}
		return $data;
	}


	// read all records with reference range
	function resultDashboardRefByIcNo($icno){

		// select all query
		$query = "SELECT
					tr.*, p.name as patient_name, tm.name as marker_name, tp.name as panel_name, tm.unit, tp.panel_id,tp.test_panel_code,  tm.data_format as decimal_point
				FROM
					test_result tr, person p, test_marker tm, test_panel tp
				WHERE
					tr.patient_ic_no = p.ic_no
					and tm.code = tr.test_marker_code
					and tm.test_panel_code = tr.test_panel_code
					and tr.test_panel_code = tp.test_panel_code
					and p.ic_no = ?
				ORDER BY
				patient_ic_no,tp.panel_id,tr.test_marker_code,test_date";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind
		$stmt->bindParam(1, $icno);
		
		// execute query
		$stmt->execute();
		
		$data=array();
		$subdata = array();

		$testMarker = new TestMarker($this->conn);
		
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);

			$test_marker_arr = $testMarker->readByPanelCode($test_panel_code);

			$ref_value = null;
			foreach ($test_marker_arr as $test_marker_item) {
				$tmcode = $test_marker_item['code'];
				if ($test_marker_code==$tmcode){
					$ref_arr = $test_marker_item['test_reference_range'];
					foreach ($ref_arr as $ref_item) {
						$minval=0;
						$maxval=999;
						if ($ref_item['max']==$maxval){
							if ($test_value>$ref_item['min']) $ref_value = $ref_item['code'];
						}else{
							if ($ref_item['min']==$minval) {
								if ($test_value<$ref_item['max']) $ref_value = $ref_item['code'];
							}else{
								if ($test_value>=$ref_item['min'] && $test_value<=$ref_item['max']){
									$ref_value = $ref_item['code'];
								}
							}
						}

					}
				}
			}
			
			$grp_ic_no = $patient_ic_no;

			if ( !isset( $data[$grp_ic_no] ) ) {
				$data[$grp_ic_no]= array( "name" => $patient_name, "ic_no" => $patient_ic_no);
			}
			if ( !isset( $data[$grp_ic_no]['test_panel_id#'.$panel_id] ) ) {
				$data[$grp_ic_no]['test_panel_id#'.$panel_id] = array( "test_panel_id" => $panel_id,"test_panel_code" => $test_panel_code, "test_panel_name" => $panel_name, "test_marker_range" => $test_marker_arr, "source" => $source);
			}
			if ( !isset( $data[$grp_ic_no]['test_panel_id#'.$panel_id]['test_marker_code#'.$test_marker_code] ) ) {
				$data[$grp_ic_no]['test_panel_id#'.$panel_id]['test_marker_code#'.$test_marker_code] = array(  "test_marker_code" => $test_marker_code,"test_marker_name" => $marker_name,"decimal_point"=>$decimal_point);
			}
			$data[$grp_ic_no]['test_panel_id#'.$panel_id]['test_marker_code#'.$test_marker_code]['test_dates'][] = array("patient_ic_no" => $patient_ic_no,"test_date" => $test_date,"test_panel_code" => $test_panel_code,"test_panel_name" => $panel_name, "test_marker_code"=>$test_marker_code, "test_marker_name"=>$marker_name,"test_value"=>$test_value,"test_unit"=>$unit, "source"=>$source,"ref_value"=>$ref_value);
		}
		return $data;
	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				patient_ic_no= :patient_ic_no,  booking_no= :booking_no, reg_no=:reg_no, test_marker_code= :test_marker_code, 
				test_date=:test_date, test_value= :test_value, source= :source, test_panel_code = :test_panel_code";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->test_marker_code=htmlspecialchars(strip_tags($this->test_marker_code));
		$this->patient_ic_no=htmlspecialchars(strip_tags($this->patient_ic_no));
		$this->booking_no=htmlspecialchars(strip_tags($this->booking_no));
		$this->reg_no=htmlspecialchars(strip_tags($this->reg_no));
		$this->test_date=htmlspecialchars(strip_tags($this->test_date));
		$this->test_value=htmlspecialchars(strip_tags($this->test_value));
		$this->source=htmlspecialchars(strip_tags($this->source));
		$this->test_panel_code=htmlspecialchars(strip_tags($this->test_panel_code));
		
		// bind values
		$stmt->bindParam(":test_marker_code", $this->test_marker_code);
		$stmt->bindParam(":patient_ic_no", $this->patient_ic_no);
		$stmt->bindParam(":booking_no", $this->booking_no);
		$stmt->bindParam(":reg_no", $this->reg_no);
		$stmt->bindParam(":test_date", $this->test_date);
		$stmt->bindParam(":test_value", $this->test_value);
		$stmt->bindParam(":source", $this->source);
		$stmt->bindParam(":test_panel_code", $this->test_panel_code);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}
		
		
	// used when filling up the update record form
	function readOne(){

		$this->code=htmlspecialchars(strip_tags($this->code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					patient_ic_no = :patient_ic_no and test_marker_code=:test_marker_code 
					and booking_no = :booking_no and reg_no = :reg_no 
					and source = :source  
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":patient_ic_no", $this->patient_ic_no);
		$stmt->bindParam(":test_marker_code", $this->test_marker_code);
		$stmt->bindParam(":booking_no", $this->booking_no);
		$stmt->bindParam(":reg_no", $this->reg_no);
		$stmt->bindParam(":source", $this->source);
		// $stmt->bindParam(":date_updated", $this->date_updated);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->test_marker_code = $row['test_marker_code'];
		$this->patient_ic_no = $row['patient_ic_no'];
		$this->booking_no = $row['booking_no'];
		$this->reg_no = $row['reg_no'];
		$this->test_date = $row['test_date']; 
		$this->test_value = $row['test_value'];
		$this->source = $row['source'];
		// $this->date_updated = $row['date_updated'];
	}


	// update the record
	function update(){
		
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						test_value = :test_value,
						source = :source 
					WHERE
						patient_ic_no = :patient_ic_no and test_marker_code=:test_marker_code and test_date = :test_date";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->patient_ic_no=htmlspecialchars(strip_tags($this->patient_ic_no));
			$this->test_marker_code=htmlspecialchars(strip_tags($this->test_marker_code));
			$this->test_date=htmlspecialchars(strip_tags($this->test_date));
			// $this->booking_no=htmlspecialchars(strip_tags($this->booking_no));
			$this->test_value=htmlspecialchars(strip_tags($this->test_value));
			$this->source=htmlspecialchars(strip_tags($this->source));
			
			// bind values
			$stmt->bindParam(":test_marker_code", $this->test_marker_code);
			$stmt->bindParam(":patient_ic_no", $this->patient_ic_no);
			// $stmt->bindParam(":booking_no", $this->booking_no);
			$stmt->bindParam(":test_date", $this->test_date);
			$stmt->bindParam(":test_value", $this->test_value);
			$stmt->bindParam(":source", $this->source);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

  	// delete the record
	function delete(){
		
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE test_panel_code = ? and patient_ic_no=? and test_date=?";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->test_panel_code=htmlspecialchars(strip_tags($this->test_panel_code));
		$this->patient_ic_no=htmlspecialchars(strip_tags($this->patient_ic_no));
		$this->test_date=htmlspecialchars(strip_tags($this->test_date));
		
		// bind id of record to delete
		$stmt->bindParam(1, $this->test_panel_code);
		$stmt->bindParam(2, $this->patient_ic_no);
		$stmt->bindParam(3, $this->test_date);
		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}


}

?>