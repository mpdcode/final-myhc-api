<?php

class RefMessage{
  
    // database connection and table name
    private $conn;
    private $table_name = "ref_message";

    // object properties
    
	public $message_type_code;
	public $message_type_desc;



  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
				message_type_code";
					
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}
	



	function readOne(){
		
		$this->message_type_code=htmlspecialchars(strip_tags($this->message_type_code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					message_type_code = :message_type_code
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated by primary key
		$stmt->bindParam(":message_type_code", $this->message_type_code);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->message_type_code = $row['message_type_code'];
		$this->message_type_desc = $row['message_type_desc']; 

	}
}

?>