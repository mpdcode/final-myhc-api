<?php


error_reporting(E_ALL);

class TestGroupPanel{
  
    // database connection and table name
    private $conn;
    private $table_name = "test_group_panel";

    // object properties
	public $test_group_code;
    public $test_panel_code;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					test_group_code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}

	// used when filling up the update record form
	function readOne(){

		$this->test_group_code=htmlspecialchars(strip_tags($this->test_group_code));
		$this->test_panel_code=htmlspecialchars(strip_tags($this->test_panel_code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					test_group_code = :test_group_code and test_panel_code = :test_panel_code
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated by primary key
		$stmt->bindParam(":test_group_code", $this->test_group_code);
		$stmt->bindParam(":test_panel_code", $this->test_panel_code);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->test_group_code = $row['test_group_code'];
		$this->test_panel_code = $row['test_panel_code']; 
 

	}
	

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
					test_group_code=:test_group_code, test_panel_code=:test_panel_code  ";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->test_group_code=htmlspecialchars(strip_tags($this->test_group_code));
		$this->test_panel_code=htmlspecialchars(strip_tags($this->test_panel_code));
		
		// bind values
		$stmt->bindParam(":test_group_code", $this->test_group_code);
		$stmt->bindParam(":test_panel_code", $this->test_panel_code);

		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}
		
	// delete the record
	function delete(){
		
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE test_group_code = ? and test_panel_code = ? ";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->test_group_code=htmlspecialchars(strip_tags($this->test_group_code));
		$this->test_panel_code=htmlspecialchars(strip_tags($this->test_panel_code));
		
		// bind id of record to delete
		$stmt->bindParam(1, $this->test_group_code);
		$stmt->bindParam(2, $this->test_panel_code);
		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	 


}

?>