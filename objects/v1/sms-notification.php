<?php

include_once 'sms-log.php';
include_once 'constant.php';

class SmsNotification{
  
    // database connection and table name
    private $conn;
    private $table_name = "sms_notification";

    // object properties
    public $code;
	public $message;
	public $params;

	//for sending messages properties
	private $urlSmsGateway = CONST_SMS_GATEWAY;
	public $paramValues;
	public $mobile_no;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}

	// used when filling up the update record form
	function readOne(){

		$this->code=htmlspecialchars(strip_tags($this->code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					code = :code
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":code", $this->code);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->code = $row['code'];
		$this->message = $row['message'];
		$this->params = $row['params'];
	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				 code=:code,  message=:message";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->code=htmlspecialchars(strip_tags($this->code));
		$this->message=htmlspecialchars(strip_tags($this->message));
		$this->params=htmlspecialchars(strip_tags($this->params));
		
		// bind values
		$stmt->bindParam(":code", $this->code);
		$stmt->bindParam(":message", $this->message);
		$stmt->bindParam(":params", $this->params);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	
	// update the record
	function update(){
	
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						message = :message
					WHERE
						code = :code";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->code=htmlspecialchars(strip_tags($this->code));
			$this->message=htmlspecialchars(strip_tags($this->message));
			$this->params=htmlspecialchars(strip_tags($this->params));
			
			// bind values
			$stmt->bindParam(":code", $this->code);
			$stmt->bindParam(":message", $this->message);
			$stmt->bindParam(":params", $this->params);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE code = ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->code=htmlspecialchars(strip_tags($this->code));

		// bind id of record to delete
		$stmt->bindParam(1, $this->code);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}

	function sendSms(){

		$msg = $this->message;

		foreach ($this->paramValues as $paramValue) {
			$msg = str_replace($paramValue['param'],$paramValue['value'],$msg);
		}

		$fields = [
			'username' => CONST_SMS_USERNAME,
			'password' => CONST_SMS_PASSWORD,
			'message' => $msg,
			'mobile' => $this->mobile_no,
			'sender' => 'test',
			'type' => '1'
		];
		

		$response = $this->httpPost($this->urlSmsGateway, $fields);
		
		//insert to sms_log
		$smsLog = new SmsLog($this->conn);
		$smsLog->mobile_no = $this->mobile_no;
		$smsLog->message = $msg;

		if(strpos($response,'1701')!==false){
			$smsLog->status = "SUCCESS";
			$smsLog->create();
			return true;
		}else{
			$smsLog->status = "FAILED - " .$response;
			$smsLog->create();
			return false;
		}

	}

	
	function httpPost($url, $data)
	{
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($curl);
		curl_close($curl);
		return $response;
	}
	
}

?>