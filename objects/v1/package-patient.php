<?php

class PackagePatient{
  
    // database connection and table name
    private $conn;
    private $table_name = "package_patient";

    // object properties
    
	public $package_code;
	public $patient_type_code;  
	public $total_patient;
	public $doc_required;
	
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					patient_type_code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}

	//call from screening_plan. read-all.php
	function readByPackageCode($package_code){
  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . " 
					
				WHERE
					package_code = :package_code

				ORDER BY
					patient_type_code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	   
		// bind code of data to be updated
		$stmt->bindParam(":package_code", $package_code);

		// execute query
		$stmt->execute();

		$arr=array();
		//read line by line
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row); //read every field in the row
			$packagePatient_item=array(
				"package_code" => $package_code,
				"patient_type_code" => $patient_type_code,
				"total_patient" => $total_patient,
				"doc_required" => $doc_required

			);
			array_push($arr, $packagePatient_item);
		}
	  
		return $arr;

	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				package_code=:package_code, patient_type_code=:patient_type_code, 
				total_patient=:total_patient 
				 ";
					
				 
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		
		$this->package_code=htmlspecialchars(strip_tags($this->package_code));
		$this->patient_type_code=htmlspecialchars(strip_tags($this->patient_type_code));
		$this->total_patient=htmlspecialchars(strip_tags($this->total_patient));
		// $this->doc_required=htmlspecialchars(strip_tags($this->doc_required));
		
		// bind values
	
		$stmt->bindParam(":package_code", $this->package_code);
		$stmt->bindParam(":patient_type_code", $this->patient_type_code);
		$stmt->bindParam(":total_patient", $this->total_patient);				
		// $stmt->bindParam(":doc_required", $this->doc_required);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}

		
	// used when filling up the update record form
	function readOne(){

		$this->package_code=htmlspecialchars(strip_tags($this->package_code));
		$this->patient_type_code=htmlspecialchars(strip_tags($this->patient_type_code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					package_code =:package_code and
					patient_type_code =:patient_type_code 
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated by primary key, 2 PK
		$stmt->bindParam(":package_code", $this->package_code);
		$stmt->bindParam(":patient_type_code", $this->patient_type_code);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		


		// set values to object properties
		$this->package_code = $row['package_code'];
		$this->patient_type_code = $row['patient_type_code']; 
		$this->total_patient= $row['total_patient'];
		$this->doc_required= $row['doc_required'];

	}


	// update the record except 2 primary key Code
	function update(){
		
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
					
					total_patient=:total_patient,
					doc_required=:doc_required

					WHERE
					package_code=:package_code and patient_type_code=:patient_type_code";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
	

			$this->package_code=htmlspecialchars(strip_tags($this->package_code));
			$this->patient_type_code=htmlspecialchars(strip_tags($this->patient_type_code));
			$this->total_patient=htmlspecialchars(strip_tags($this->total_patient));
			// $this->doc_required=htmlspecialchars(strip_tags($this->doc_required));
			
			// bind values


			$stmt->bindParam(":package_code", $this->package_code);
			$stmt->bindParam(":patient_type_code", $this->patient_type_code);
			$stmt->bindParam(":total_patient", $this->total_patient);
			$stmt->bindParam(":doc_required", $this->doc_required);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	// delete the record based on the primary key, add PK if more than 1
	function delete(){
		
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE package_code = ? and patient_type_code = ?";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->package_code=htmlspecialchars(strip_tags($this->package_code));
		$this->patient_type_code=htmlspecialchars(strip_tags($this->patient_type_code));
		
		// bind id of record to delete
		$stmt->bindParam(1, $this->package_code);
		$stmt->bindParam(2, $this->patient_type_code);
		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}


	
	// search records
	function search($keywords){
		
		// select all query
		// patient_type_code LIKE ?   
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					patient_type_code LIKE ?  OR package_code LIKE ?  
				ORDER BY
					patient_type_code";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$keywords=htmlspecialchars(strip_tags($keywords));
		$keywords = "%{$keywords}%";
		
		// bind
		$stmt->bindParam(1, $keywords);
		$stmt->bindParam(2, $keywords);
	
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}

}

?>