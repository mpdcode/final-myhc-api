<?php


class MenuMainItem{
  
    // database connection and table name
    private $conn;
    private $table_name = "menu_main_item";

    // object properties
    public $item_id;
	public $main_id;
	public $sort_id;

	public $create;
	public $read;
	public $update;
	public $delete;
	 
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }

  

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
					item_id=:item_id,  main_id=:main_id, sort_id=:sort_id";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->item_id=htmlspecialchars(strip_tags($this->item_id));
		$this->main_id=htmlspecialchars(strip_tags($this->main_id));
		$this->sort_id=htmlspecialchars(strip_tags($this->sort_id));
		
		// bind values
		$stmt->bindParam(":item_id", $this->item_id);
		$stmt->bindParam(":main_id", $this->main_id);
		$stmt->bindParam(":sort_id", $this->sort_id);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
 
	// used when filling up the update record form
	function readOne(){

		$this->main_id=htmlspecialchars(strip_tags($this->main_id));
		$this->item_id=htmlspecialchars(strip_tags($this->item_id));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					main_id = :main_id and item_id = :item_id
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":main_id", $this->main_id);
		$stmt->bindParam(":item_id", $this->item_id);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->main_id = $row['main_id'];
		$this->item_id = $row['item_id'];
		$this->sort_id = $row['sort_id'];
	}


	
	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE item_id = ? and  main_id= ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->item_id=htmlspecialchars(strip_tags($this->item_id));
		$this->main_id=htmlspecialchars(strip_tags($this->main_id));

		// bind id of record to delete
		$stmt->bindParam(1, $this->item_id);
		$stmt->bindParam(2, $this->main_id);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}

	function readCRUD()
	{
		//query to read single record
		$query = "SELECT * FROM " . $this->table_name . " WHERE item_id = ? and  main_id= ?";
		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->item_id=htmlspecialchars(strip_tags($this->item_id));
		$this->main_id=htmlspecialchars(strip_tags($this->main_id));

		// bind id of record to delete
		$stmt->bindParam(1, $this->item_id);
		$stmt->bindParam(2, $this->main_id);

		// execute query
		$stmt->execute();

		$arr=array();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record=array(
				
				"permission" => array("create"=>$int_create,"read"=>$int_read,"update"=>$int_update,'delete'=>$int_delete)
			);
			array_push($arr, $record);
		}
 

		return $arr;
	}

	function updateCRUD()
	{
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
					int_create=:create,
					int_read=:read,
					int_delete=:delete,
					int_update=:update						
					WHERE
						main_id = :main_id
					AND
						item_id = :item_id";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->main_id=htmlspecialchars(strip_tags($this->main_id));
			$this->item_id=htmlspecialchars(strip_tags($this->item_id));
			$this->create=htmlspecialchars(strip_tags($this->create));
			$this->read=htmlspecialchars(strip_tags($this->read));
			$this->delete=htmlspecialchars(strip_tags($this->delete));
			$this->update=htmlspecialchars(strip_tags($this->update));
			
			// bind values
			$stmt->bindParam(":main_id", $this->main_id);
			$stmt->bindParam(":item_id", $this->item_id);
			$stmt->bindParam(":create", $this->create);
			$stmt->bindParam(":read", $this->read);
			$stmt->bindParam(":delete", $this->delete);
			$stmt->bindParam(":update", $this->update);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}
}

?>