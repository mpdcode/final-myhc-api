<?php

class SixDigitCode{
  
    // database connection and table name
    private $conn;
    private $table_name = "six_digit_code";

    // object properties
    public $code;
	public $mobie;
	public $expired;
	public $date_sent;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }
 
	// used when filling up the update record form
	function readOne(){

		$this->mobile=htmlspecialchars(strip_tags($this->mobile));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					mobile = :mobile
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":mobile", $this->mobile);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->code = $row['code'];
		// $this->mobile = $row['mobile'];
		$this->expired = $row['expired'];
		$this->date_sent = $row['date_sent'];
	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				 code=:code,  mobile=:mobile, expired=:expired";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->code=htmlspecialchars(strip_tags($this->code));
		$this->mobile=htmlspecialchars(strip_tags($this->mobile));
		$this->expired=htmlspecialchars(strip_tags($this->expired));
		
		// bind values
		$stmt->bindParam(":code", $this->code);
		$stmt->bindParam(":mobile", $this->mobile);
		$stmt->bindParam(":expired", $this->expired);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	
	// update the record
	function update(){
	
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						code = :code, expired = :expired
					WHERE
						mobile = :mobile";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->code=htmlspecialchars(strip_tags($this->code));
			$this->expired=htmlspecialchars(strip_tags($this->expired));
			$this->mobile=htmlspecialchars(strip_tags($this->mobile));
			
			// bind values
			$stmt->bindParam(":code", $this->code);
			$stmt->bindParam(":expired", $this->expired);
			$stmt->bindParam(":mobile", $this->mobile);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

 
	
}

?>