<?php

class ScreeningCenterTimeslot{
  
    // database connection and table name
    private $conn;
    private $table_name = "screening_center_timeslot";

    // object properties
    public $ts_code;
	public $sc_id;
 
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }

	 

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				 ts_code=:ts_code,  sc_id=:sc_id ";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->ts_code=htmlspecialchars(strip_tags($this->ts_code));
		$this->sc_id=htmlspecialchars(strip_tags($this->sc_id)); 
		
		// bind values
		$stmt->bindParam(":ts_code", $this->ts_code);
		$stmt->bindParam(":sc_id", $this->sc_id); 
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	
	// update the record
	function update(){
	
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						sc_id=:sc_id,  ts_code=:ts_code 
					WHERE
						sc_id = :sc_id and ts_code = :ts_code";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->ts_code=htmlspecialchars(strip_tags($this->ts_code));
			$this->sc_id=htmlspecialchars(strip_tags($this->sc_id)); 
			
			// bind values
			$stmt->bindParam(":ts_code", $this->ts_code);
			$stmt->bindParam(":sc_id", $this->sc_id); 
 
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE sc_id = ? and ts_code=?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->id=htmlspecialchars(strip_tags($this->sc_id));

		// bind id of record to delete
		$stmt->bindParam(1, $this->sc_id);
		$stmt->bindParam(2, $this->ts_code);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}

	function readByScreeningCenterId($sc_id){

		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					sc_id = :sc_id
				order by ts_code";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated by primary key
		$stmt->bindParam(":sc_id", $sc_id);
		
		// execute query
		$stmt->execute();
		
		$arr=array();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record_item=array(
				"sc_id" => $sc_id,
				"ts_code" => $ts_code 
			);
			array_push($arr, $record_item);
		}
	  
		return $arr;
 
	}
	
}

?>