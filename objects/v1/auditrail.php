<?php
include_once 'constant.php';

class Auditrail{
  
    // database connection and table name
    private $conn;
    private $table_name = "auditrail_staff";

    // object properties
    
	public $intAuditrailId;
	public $intProject;
	public $strModuleNamePathTblName;
	public $intRecordId;
	public $intAction;
	public $strAddedByUsername;
	public $strAddedByEmail;
	public $intStatus;
	public $CreateDate;
	public $UpdateDate;
	
	
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
                intAuditrailId";
					
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}

	// create object
	function create(){

	 
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
                intAuditrailId=:intAuditrailId,
                intProject=:intProject, strModuleNamePathTblName=:strModuleNamePathTblName, 
                intRecordId=:intRecordId, intAction=:intAction, strAddedByUsername=:strAddedByUsername,
                strAddedByEmail=:strAddedByEmail, intStatus=:intStatus, CreateDate=:CreateDate, UpdateDate=:UpdateDate
				 ";
					
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		$arrSantizeAndBind = array('1' => 'intAuditrailId',
		                           '2'=>'intProject',
								   '3'=>'strModuleNamePathTblName',
								   '4'=>'intRecordId',
								   '5'=>'intAction',
								   '6'=>'strAddedByUsername',
								   '7'=>'strAddedByEmail',
								   '8'=>'intStatus',
								   '9'=>'CreateDate',
								   '10'=>'UpdateDate',
								);

			$i=0;	
			$vConvert= "";				
		foreach($arrSantizeAndBind as $key =>$value)
		{
			//echo $value;
			// $iv = htmlspecialchars(strip_tags($this->$value));
			// if($value=="intAuditrailId")
			// {
			// 	$this->intAuditrailId =  $this->getNextId();
			// 	$stmt->bindParam(":intAuditrailId",$this->intAuditrailId);
			// }
			
			// else{
			 	$stmt->bindParam(":".$value,$this->$value);
						
			//}
			
			
		}

       

		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}
		
		
	

	

	function getNextId(){
		$query = "SELECT AUTO_INCREMENT
		FROM information_schema.TABLES
		WHERE TABLE_SCHEMA = '". CONST_DB_NAME ."'
		AND TABLE_NAME = '". $this->table_name ."'" ;

		// prepare query
		$stmt = $this->conn->prepare($query);

		if ($stmt->execute()){
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			return $row['AUTO_INCREMENT'];
		}else{
			return -1;
		}	
		
	}
	
}

?>