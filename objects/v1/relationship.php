<?php

class Relationship{
  
    // database connection and table name
    private $conn;
    private $table_name = "ref_relationship";

    // object properties
    
	public $code;
	public $description;

	
	
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					code";
					
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}
	
}

?>