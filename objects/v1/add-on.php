<?php
include_once 'constant.php';

class AddOn{
  
    // database connection and table name
    private $conn;
    private $table_name = "add_on_services";

    // object properties
    
	public $uid;
	public $add_on_code;
	public $name;
	public $price;
	public $unit;
	public $unit_decimal;
	public $remark;
	public $status;
	public $patient_type_code;
	public $no_of_patient;
	
	
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					add_on_code";
					
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				 uid=:uid,
				 add_on_code=:add_on_code, name=:name, 
				 price=:price, unit=:unit, unit_decimal=:unit_decimal,
				 remark=:remark, status=:status, patient_type_code=:patient_type_code, no_of_patient=:no_of_patient
				 ";
					
				
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->uid =  $this->getIdAuto();
		$this->add_on_code=htmlspecialchars(strip_tags(CONST_ADDON_SERVICE_CODE . $this->uid));
		$this->name=htmlspecialchars(strip_tags($this->name));
		$this->price=htmlspecialchars(strip_tags($this->price));
		$this->unit=htmlspecialchars(strip_tags($this->unit));
		$this->unit_decimal=htmlspecialchars(strip_tags($this->unit_decimal));
		$this->remark=htmlspecialchars(strip_tags($this->remark));
		$this->status=htmlspecialchars(strip_tags($this->status));
		$this->patient_type_code=htmlspecialchars(strip_tags($this->patient_type_code));
		$this->no_of_patient=htmlspecialchars(strip_tags($this->no_of_patient));
		
		
		
		// bind values
		$stmt->bindParam(":uid",$this->uid);
		$stmt->bindParam(":add_on_code", $this->add_on_code);
		$stmt->bindParam(":name", $this->name);
		$stmt->bindParam(":price", $this->price);
		$stmt->bindParam(":unit", $this->unit);
		$stmt->bindParam(":unit_decimal", $this->unit_decimal);
		$stmt->bindParam(":remark", $this->remark);
		$stmt->bindParam(":status", $this->status);
		$stmt->bindParam(":patient_type_code", $this->patient_type_code);
		$stmt->bindParam(":no_of_patient", $this->no_of_patient);
	
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			print_r($stmt->errorInfo());
				return false;
		}
		
	}
		
		
	// used when filling up the update record form
	function readOne(){
		
		$this->add_on_code=htmlspecialchars(strip_tags($this->add_on_code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					add_on_code = :add_on_code
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated by primary key
		$stmt->bindParam(":add_on_code", $this->add_on_code);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->uid = $row['uid'];
		$this->add_on_code = $row['add_on_code'];
		$this->name = $row['name']; 
		$this->price= $row['price'];
		$this->unit= $row['unit'];
		$this->unit_decimal= $row['unit_decimal'];
		$this->remark= $row['remark'];
		$this->status= $row['status'];
		$this->patient_type_code= $row['patient_type_code'];
		$this->no_of_patient= $row['no_of_patient'];

	}

	//added by smaina 19-08-2021
	// used when filtering by add_on_code
	function readByAddOnCode($add_on_code){
	
		$this->add_on_code=htmlspecialchars(strip_tags($add_on_code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					add_on_code = :add_on_code
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated by primary key
		$stmt->bindParam(":add_on_code", $this->add_on_code);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$record=array(
			"uid" => $row['uid'],
			"add_on_code" => $row['add_on_code'],
			"name" =>  $row['name'],
			"price" => $row['price'],
			"unit" => $row['unit'],
			"unit_decimal" => $row['unit_decimal'],
			"remark" => $row['remark'],
			"status" => $row['status'],
			"patient_type_code" => $row['patient_type_code'],
			"no_of_patient" => $row['no_of_patient']
		);

		return $record;

	}
	

	// update the record except primary key Code
	function update(){
		
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						name=:name, 
						price=:price, unit=:unit, unit_decimal=:unit_decimal,
						remark=:remark, status=:status, patient_type_code=:patient_type_code, no_of_patient=:no_of_patient
				   
					WHERE
						add_on_code = :add_on_code";
						
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
	 
			$this->add_on_code=htmlspecialchars(strip_tags($this->add_on_code));
			$this->name=htmlspecialchars(strip_tags($this->name));
			$this->price=htmlspecialchars(strip_tags($this->price));
			$this->unit=htmlspecialchars(strip_tags($this->unit));
			$this->unit_decimal=htmlspecialchars(strip_tags($this->unit_decimal));
			$this->remark=htmlspecialchars(strip_tags($this->remark));
			$this->status=htmlspecialchars(strip_tags($this->status));
			$this->patient_type_code=htmlspecialchars(strip_tags($this->patient_type_code));
			$this->no_of_patient=htmlspecialchars(strip_tags($this->no_of_patient));
			

			
			// bind values
	 
			$stmt->bindParam(":add_on_code", $this->add_on_code);
			$stmt->bindParam(":name", $this->name);
			$stmt->bindParam(":price", $this->price);
			$stmt->bindParam(":unit", $this->unit);
			$stmt->bindParam(":unit_decimal", $this->unit_decimal);
			$stmt->bindParam(":remark", $this->remark);
			$stmt->bindParam(":status", $this->status);
			$stmt->bindParam(":patient_type_code", $this->patient_type_code);
			$stmt->bindParam(":no_of_patient", $this->no_of_patient);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	// delete the record based on the primary key, add PK if more than 1
	function delete(){
		
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE add_on_code = ?";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->add_on_code=htmlspecialchars(strip_tags($this->add_on_code));
		
		// bind id of record to delete
		$stmt->bindParam(1, $this->add_on_code);
		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	// search records
	function search($keywords){
		
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					add_on_code LIKE ? OR name LIKE ?  
				ORDER BY
					add_on_code";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$keywords=htmlspecialchars(strip_tags($keywords));
		$keywords = "%{$keywords}%";
		
		// bind
		$stmt->bindParam(1, $keywords);
		$stmt->bindParam(2, $keywords);
	
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}
	
	function getIdAuto()
	{
		$query = "SELECT max(uid)+1 as autoID
		FROM ". $this->table_name  ;
	
		// prepare query
		$stmt = $this->conn->prepare($query);

		if ($stmt->execute()){
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			return  $row['autoID'];
		}else{
			return -1;
		}	


	}



	function getNextId(){
		$query = "SELECT AUTO_INCREMENT
		FROM information_schema.TABLES
		WHERE TABLE_SCHEMA = '". CONST_DB_NAME ."'
		AND TABLE_NAME = '". $this->table_name ."'" ;

		// prepare query
		$stmt = $this->conn->prepare($query);

		if ($stmt->execute()){
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			return  str_pad($row['AUTO_INCREMENT'],2,"0",STR_PAD_LEFT);
		}else{
			return -1;
		}	
		
	}
	
}

?>