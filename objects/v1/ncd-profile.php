<?php

class NcdProfile{
  
    // database connection and table name
    private $conn;
    private $table_name = "ncd_profile";

    // object properties
    public $ic_no;
	public $tabacco_use;
	public $alcohol_consumption;
	public $diet_eat_fruit;
	public $diet_eat_vege;
	public $physical_activities;
	public $history_hypertension;
	public $history_diabetes;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }

   

	// used when filling up the update record form
	function readOne(){

		$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					ic_no = :ic_no
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":ic_no", $this->ic_no);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->ic_no = $row['ic_no'];
		$this->tabacco_use = $row['tabacco_use'];
		$this->alcohol_consumption = $row['alcohol_consumption'];
		$this->diet_eat_fruit = $row['diet_eat_fruit'];
		$this->diet_eat_vege = $row['diet_eat_vege'];
		$this->physical_activities = $row['physical_activities'];
		$this->history_hypertension = $row['history_hypertension'];
		$this->history_diabetes = $row['history_diabetes'];
	}

	// create object
	function create(){
	 
		// query to insert record
		$query = "INSERT  
						INTO
					" . $this->table_name . "
				SET
				ic_no=:ic_no,
				tabacco_use=:tabacco_use,  alcohol_consumption=:alcohol_consumption,  diet_eat_fruit=:diet_eat_fruit,  
				diet_eat_vege=:diet_eat_vege,  physical_activities=:physical_activities, history_hypertension=:history_hypertension,
				history_diabetes=:history_diabetes";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
		$this->tabacco_use=htmlspecialchars(strip_tags($this->tabacco_use));
		$this->alcohol_consumption=htmlspecialchars(strip_tags($this->alcohol_consumption));
		$this->diet_eat_fruit=htmlspecialchars(strip_tags($this->diet_eat_fruit));
		$this->diet_eat_vege=htmlspecialchars(strip_tags($this->diet_eat_vege));
		$this->physical_activities=htmlspecialchars(strip_tags($this->physical_activities));
		$this->history_hypertension=htmlspecialchars(strip_tags($this->history_hypertension));
		$this->history_diabetes=htmlspecialchars(strip_tags($this->history_diabetes));
		
		// bind values
		$stmt->bindParam(":ic_no", $this->ic_no);
		$stmt->bindParam(":tabacco_use", $this->tabacco_use);
		$stmt->bindParam(":alcohol_consumption", $this->alcohol_consumption);
		$stmt->bindParam(":diet_eat_fruit", $this->diet_eat_fruit);
		$stmt->bindParam(":diet_eat_vege", $this->diet_eat_vege);
		$stmt->bindParam(":physical_activities", $this->physical_activities);
		$stmt->bindParam(":history_hypertension", $this->history_hypertension);
		$stmt->bindParam(":history_diabetes", $this->history_diabetes);
		

		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			print_r($stmt->errorInfo());
			return false;
		}
		
	}	
	
	// update the record
	function update(){
	
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						tabacco_use=:tabacco_use,  alcohol_consumption=:alcohol_consumption,  diet_eat_fruit=:diet_eat_fruit,  
						diet_eat_vege=:diet_eat_vege,  physical_activities=:physical_activities, history_hypertension=:history_hypertension,
						history_diabetes=:history_diabetes
					WHERE
						ic_no=:ic_no";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
			$this->tabacco_use=htmlspecialchars(strip_tags($this->tabacco_use));
			$this->alcohol_consumption=htmlspecialchars(strip_tags($this->alcohol_consumption));
			$this->diet_eat_fruit=htmlspecialchars(strip_tags($this->diet_eat_fruit));
			$this->diet_eat_vege=htmlspecialchars(strip_tags($this->diet_eat_vege));
			$this->physical_activities=htmlspecialchars(strip_tags($this->physical_activities));
			$this->history_hypertension=htmlspecialchars(strip_tags($this->history_hypertension));
			$this->history_diabetes=htmlspecialchars(strip_tags($this->history_diabetes));
			
			// bind values
			$stmt->bindParam(":ic_no", $this->ic_no);
			$stmt->bindParam(":tabacco_use", $this->tabacco_use);
			$stmt->bindParam(":alcohol_consumption", $this->alcohol_consumption);
			$stmt->bindParam(":diet_eat_fruit", $this->diet_eat_fruit);
			$stmt->bindParam(":diet_eat_vege", $this->diet_eat_vege);
			$stmt->bindParam(":physical_activities", $this->physical_activities);
			$stmt->bindParam(":history_hypertension", $this->history_hypertension);
			$stmt->bindParam(":history_diabetes", $this->history_diabetes);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE ic_no = ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));

		// bind id of record to delete
		$stmt->bindParam(1, $this->ic_no);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}

 
}

?>