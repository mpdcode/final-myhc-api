<?php


error_reporting(E_ALL);
ini_set("display_errors",1);

include_once 'constant.php';
include_once 'test-panel.php';

class TestGroup{
  
    // database connection and table name
    private $conn;
    private $table_name = "test_group";

    // object properties
	public $uid;
	public $test_group_code;
    public $package_category;
    public $group_name;
	public $patient_type;
	public $price;
	public $enabled;
	
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					test_group_code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
					uid=:uid,
					test_group_code=:test_group_code, package_category=:package_category, 
					group_name=:group_name, patient_type=:patient_type, 
					price=:price, enabled=:enabled";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->uid =  $this->getNextId();
		$this->test_group_code=htmlspecialchars(strip_tags(CONST_ADDON_TEST_CODE . $this->uid));
		$this->package_category=htmlspecialchars(strip_tags($this->package_category));
		$this->group_name=htmlspecialchars(strip_tags($this->group_name));
		$this->patient_type=htmlspecialchars(strip_tags($this->patient_type));
		$this->price=htmlspecialchars(strip_tags($this->price));
		$this->enabled=htmlspecialchars(strip_tags($this->enabled));
		
		// bind values
		$stmt->bindParam(":uid",$this->uid);
		$stmt->bindParam(":test_group_code", $this->test_group_code);
		$stmt->bindParam(":package_category", $this->package_category);
		$stmt->bindParam(":group_name", $this->group_name);
		$stmt->bindParam(":patient_type", $this->patient_type);
		$stmt->bindParam(":price", $this->price);
		$stmt->bindParam(":enabled", $this->enabled);

		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}
		
		
	// used when filling up the update record form
	function readOne(){

		$this->test_group_code=htmlspecialchars(strip_tags($this->test_group_code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					test_group_code = :test_group_code
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":test_group_code", $this->test_group_code);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->uid = $row['uid'];
		$this->test_group_code = $row['test_group_code'];
		$this->package_category = $row['package_category'];
		$this->group_name = $row['group_name']; 
		$this->patient_type = $row['patient_type'];
		$this->price = $row['price'];
		$this->enabled = $row['enabled'];
	}

	

	// update the record
	function update(){
		
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						group_name = :group_name,
						package_category = :package_category ,
						patient_type = :patient_type,
						price = :price,
						enabled = :enabled 
					WHERE
						test_group_code = :test_group_code";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->test_group_code=htmlspecialchars(strip_tags($this->test_group_code));
			$this->package_category=htmlspecialchars(strip_tags($this->package_category));
			$this->group_name=htmlspecialchars(strip_tags($this->group_name));
			$this->patient_type=htmlspecialchars(strip_tags($this->patient_type));
			$this->price=htmlspecialchars(strip_tags($this->price));
			$this->enabled=htmlspecialchars(strip_tags($this->enabled));
			
			// bind values
			$stmt->bindParam(":test_group_code", $this->test_group_code);
			$stmt->bindParam(":package_category", $this->package_category);
			$stmt->bindParam(":group_name", $this->group_name);
			$stmt->bindParam(":patient_type", $this->patient_type);
			$stmt->bindParam(":price", $this->price);
			$stmt->bindParam(":enabled", $this->enabled);
			
			// execute the query
			if ($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	// delete the record
	function delete(){
		
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE test_group_code = ?";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->test_panel_code=htmlspecialchars(strip_tags($this->test_group_code));
		
		// bind id of record to delete
		$stmt->bindParam(1, $this->test_group_code);
		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	// search records
	function search($keywords){
		
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					test_group_code LIKE ? OR name LIKE ?  
				ORDER BY
					test_group_code";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$keywords=htmlspecialchars(strip_tags($keywords));
		$keywords = "%{$keywords}%";
		
		// bind
		$stmt->bindParam(1, $keywords);
		$stmt->bindParam(2, $keywords);
	
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}

	// used when filling up the update record form
	function readByTestGroupCode($test_group_code){

		$testPanel = new TestPanel($this->conn);
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					test_group_code = :test_group_code
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":test_group_code", $test_group_code);
		
		// execute query
		$stmt->execute();
		
		$record_arr=null;
		
		if ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record_arr = array(
				"uid" =>  $uid,
				"test_group_code" =>  $test_group_code,
				"package_category" => $package_category,
				"group_name" => $group_name ,
				"patient_type" => $patient_type,
				"price" => $price,
				"enabled" => $enabled,
				"test_panels" => $testPanel->readByTestGroupCode($test_group_code)
			);
		}

		return $record_arr;
	}


	function getNextId(){
		$query = "SELECT AUTO_INCREMENT
		FROM information_schema.TABLES
		WHERE TABLE_SCHEMA = '". CONST_DB_NAME ."'
		AND TABLE_NAME = '". $this->table_name ."'" ;

		// prepare query
		$stmt = $this->conn->prepare($query);

		if ($stmt->execute()){
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			return str_pad($row['AUTO_INCREMENT'],2,"0",STR_PAD_LEFT);
		}else{
			return -1;
		}	
		
	}

}

?>