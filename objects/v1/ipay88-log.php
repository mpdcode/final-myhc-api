<?php

class Ipay88Log{
  
    // database connection and table name
    private $conn;
    private $table_name = "ipay88_log";

    // object properties
    public $log_id;
    public $trans_id;
	public $trans_date;
    public $trans_status;
    public $payment_id;
	public $ref_no;
	public $amount;
	public $params;
	 
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
                    log_id = :log_id, trans_id = :trans_id, payment_id = :payment_id, trans_date = :trans_date, 
                    trans_status =:trans_status, ref_no = :ref_no,  amount = :amount, params = :params";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
        $this->log_id=htmlspecialchars(strip_tags($this->log_id));
		$this->trans_id=htmlspecialchars(strip_tags($this->trans_id));
		$this->trans_date=htmlspecialchars(strip_tags($this->trans_date));
        $this->trans_status=htmlspecialchars(strip_tags($this->trans_status));
		$this->payment_id=htmlspecialchars(strip_tags($this->payment_id));
		$this->ref_no=htmlspecialchars(strip_tags($this->ref_no));
		$this->amount=htmlspecialchars(strip_tags($this->amount));
		$this->params=htmlspecialchars(strip_tags($this->params));
 
		
		// bind values
        $stmt->bindParam(":log_id", $this->log_id);
		$stmt->bindParam(":trans_id", $this->trans_id);
		$stmt->bindParam(":trans_date", $this->trans_date);
		$stmt->bindParam(":trans_status", $this->trans_status);
		$stmt->bindParam(":payment_id", $this->payment_id);
        $stmt->bindParam(":ref_no", $this->ref_no);
		$stmt->bindParam(":amount", $this->amount);
		$stmt->bindParam(":params", $this->params);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}

}




?>