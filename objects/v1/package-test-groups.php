<?php

include_once 'test-group.php';

class PackageTestGroups{
  
    // database connection and table name
    private $conn;
    private $table_name = "package_test_groups";
 

    // object properties
    
	public $package_code;
	public $test_group_code;
	public $test_location;
	public $total_test_conducted;
	public $remark;
	
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
				test_group_code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}

	//call from screening_plan. read-all.php
	function readByPackageCode($package_code){

		$testGroup = new TestGroup($this->conn);
  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . " 
					
				WHERE
					package_code = :package_code 


				ORDER BY
					test_group_code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	   
		// bind code of data to be updated
		$stmt->bindParam(":package_code", $package_code);

		// execute query
		$stmt->execute();

		$arr=array();
		//read line by line
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row); //read every field in the row
			$packageTestGroups_item=array(
				"package_code" => $package_code,
				"test_group_code" => $test_group_code,
				"test_location" => $test_location,
				"total_test_conducted" => $total_test_conducted,
				"remark" => $remark,
				"test_group"=>$testGroup->readByTestGroupCode($test_group_code)
			);
			array_push($arr, $packageTestGroups_item);
		}
	  
		return $arr;

	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				
				 package_code=:package_code,
				 test_group_code=:test_group_code,
				 test_location=:test_location,
				 total_test_conducted=:total_test_conducted,
				 remark=:remark";

		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		
		
		$this->package_code=htmlspecialchars(strip_tags($this->package_code));
		$this->test_group_code =htmlspecialchars(strip_tags($this->test_group_code));
		$this->test_location=htmlspecialchars(strip_tags($this->test_location));
		$this->total_test_conducted=htmlspecialchars(strip_tags($this->total_test_conducted));
		$this->remark=htmlspecialchars(strip_tags($this->remark));
		


		
		// bind values
	
		$stmt->bindParam(":package_code", $this->package_code);
		$stmt->bindParam(":test_group_code", $this->test_group_code);
		$stmt->bindParam(":test_location", $this->test_location);
		$stmt->bindParam(":total_test_conducted", $this->total_test_conducted);
		$stmt->bindParam(":remark", $this->remark);

		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}
		
		
	// used when filling up the update record form
	function readOne(){

		$this->package_code=htmlspecialchars(strip_tags($this->package_code));
		$this->test_group_code=htmlspecialchars(strip_tags($this->test_group_code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					package_code =:package_code and
					test_group_code =:test_group_code
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated by primary key, 2 PK
		$stmt->bindParam(":package_code", $this->package_code);
		$stmt->bindParam(":test_group_code", $this->test_group_code);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->package_code = $row['package_code'];
		$this->test_group_code = $row['test_group_code']; 
		$this->test_location= $row['test_location'];
		$this->total_test_conducted= $row['total_test_conducted'];
		$this->remark= $row['remark'];

	}


	// update the record except 2 primary key Code
	function update(){
		
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
									
						test_location=:test_location,
						total_test_conducted=:total_test_conducted,
						remark=:remark
						
					WHERE
					package_code=:package_code and test_group_code=:test_group_code";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
				
			$this->package_code=htmlspecialchars(strip_tags($this->package_code));
			$this->test_group_code =htmlspecialchars(strip_tags($this->test_group_code));
			$this->test_location=htmlspecialchars(strip_tags($this->test_location));
			$this->total_test_conducted=htmlspecialchars(strip_tags($this->total_test_conducted));
			$this->remark=htmlspecialchars(strip_tags($this->remark));

			
			// bind values

			$stmt->bindParam(":package_code", $this->package_code);
			$stmt->bindParam(":test_group_code", $this->test_group_code);
			$stmt->bindParam(":test_location", $this->test_location);
			$stmt->bindParam(":total_test_conducted", $this->total_test_conducted);
			$stmt->bindParam(":remark", $this->remark);


			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	// delete the record based on the primary key, add PK if more than 1
	function delete(){
		
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE package_code = ? and test_group_code = ?";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->package_code=htmlspecialchars(strip_tags($this->package_code));
		$this->test_group_code=htmlspecialchars(strip_tags($this->test_group_code));
		
		// bind id of record to delete
		$stmt->bindParam(1, $this->package_code);
		$stmt->bindParam(2, $this->test_group_code);
		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	// search records
	function search($keywords){
		
		// select all query
		// pakai test_group_code ke atau test_location nama tak sama dgn tbl test_group
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					package_code LIKE ? OR test_group_code LIKE ?  
				ORDER BY
					test_group_code";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$keywords=htmlspecialchars(strip_tags($keywords));
		$keywords = "%{$keywords}%";
		
		// bind
		$stmt->bindParam(1, $keywords);
		$stmt->bindParam(2, $keywords);
	
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}


}

?>