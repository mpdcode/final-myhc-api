<?php

function fetch_data($result,$path)
{
  
    $output = '
        <link rel="stylesheet" href="'.$path.'bootstrap/bootstrap.min.css" />
        <script src="bootstrap/bootstrap.min.js"></script>
     
            <div class="row">
                <div class="col-sm-12">
                    <img width="100%"   src="'.$path.'images/header-receiptlogo.jpg">
                </div>
           
                <div class="col-sm-12">
                    <table style="width: 100%; border: 1px solid rgb(4, 165, 219)">
                    <tbody>
                        <tr style="background-color: lightblue;" class="font-weight-bold">
                            <td style="width: 50%;border: 1px solid rgb(4, 165, 219);padding:5px"> BIL TO </td>
                            <td style="border: 1px solid rgb(4, 165, 219);padding:5px"> PAYMENT DETAILS </td>
                        </tr>
                        <tr>
                        <td style="border: 1px solid rgb(4, 165, 219);padding:5px">
                        '.$result['person']['name'].'<br>
                        '.$result['person']['address'].'<br>
                        '.$result['person']['town'].', '.$result['person']['district'].'<br>
                        '.$result['person']['postcode'].', '.$result['person']['state'].'<br>
                        +60'.$result['person']['mobile_no'].'<br>
                        '.$result['person']['email'].' 
                        </td>
                        <td style="border: 1px solid rgb(4, 165, 219);vertical-align: text-top;padding:5px">';
    
    $receiptTotal=0;
    $sizeOfPayments = sizeof($result['payments']);               
    for($i = 0; $i < $sizeOfPayments; $i++) {
        $receiptTotal += $result['payments'][$i]['amount'];
        $output .= '
                        <div >
                            <b>RECEIPT NO:</b> '.$result['payments'][$i]['trans_no'].' <br>
                            <b>PAYMENT METHOD:</b> '.$result['payments'][$i]['ipay88']['payment_method'].'<br>
                            <b>DATE:</b> '.$result['payments'][$i]['payment_date'].'<br>
                            <b>TOTAL PAID:</b> MYR'.$result['payments'][$i]['amount'].'
                        </div>';
    }

    $output .= '                       
                        </td>
                      </tr>
                    </tbody>
                    </table>
                </div>

                <div class="col-sm-12">
                    <br>
                </div>

                <div class="col-sm-12">
                    <table style="width: 100%; border: 1px solid rgb(4, 165, 219)">
                        <tbody>
                            <tr style="background-color: rgb(41, 40, 40); color:white" class="font-weight-bold">
                                <td style="width: 40%;border: 1px solid rgb(4, 165, 219);padding:5px"> DESCRIPTION </td>
                                <td style="border: 1px solid rgb(4, 165, 219);padding:5px" class="text-center" width="20%"> AMOUNT (MYR) </td>
                            </tr>';

    $sizeOfProducts = sizeof($result['products']);
    
    for($pd = 0; $pd < $sizeOfProducts; $pd++) {
        $output .= '
                            <tr>
                            <td style="border: 1px solid rgb(4, 165, 219);padding:5px" >
                               '.$result['products'][$pd]['quantity'].' x '.$result['products'][$pd]['name'].'
                            </td>
                            <td style="border: 1px solid rgb(4, 165, 219);padding:5px" class="text-right">'.$result['products'][$pd]['total_price'].'</td>
                          </tr>';
    }


    $output .= '            <tr>
                                <td style="border: 1px solid rgb(4, 165, 219);padding:5px">
                                <b>Users to be screen:</b>';
     
    $sizeOfPatients = sizeof($result['patients']);
    for($pt = 0; $pt < $sizeOfPatients; $pt++) {
        $output .= '                             
                                <div">
                                    '. ($pt+1) .'. ' .$result['patients'][$pt]['person']['name'] .'
                                </div>';
    }

    $venue = $result['screening_location'];
    if (isset($result['screening_location'])) $venue = $result['screening_center']['sc_name'];

    $output .= '                <br>
                                <div>Venue: '.$venue.'</div>
                                <div>Date: '.$result['screening_date'].'</div>
                                <div>Time: '.$result['screening_time'].'</div>
                                </td>
                                <td style="border: 1px solid rgb(4, 165, 219);padding:5px"></td>
                            </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                          <td class="text-right font-weight-bold text-danger">RECEIPT TOTAL</td>
                          <td style="background-color: rgb(85, 214, 223);font-size:20px;font-weight: bold;padding:5px" class="text-right font-weight-bold ">'.number_format($receiptTotal, 2).'</td>
                        </tr>
                      </tfoot>
                    </table>
                </div>
                <div class="col-sm-12">
                    <br>
                </div>
                <div class="col-sm-12">
                        <table style="width: 100%; ">
                            <tbody>
                                <tr>
                                <td
                                    style="width: 50%; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; "
                                    ><span  >If you have any
                                    questions
                                    about this receipt,&nbsp;please contact :<br>Tel:
                                    +603 89595182<br>Fax: +603 89585182</span><br></td>
                                <td
                                    style="width: 50%; text-align: right; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; "
                                    ><span
                                    style="text-align: right; ">No
                                    Signature affixed<br>This is an auto-generated
                                    receipt</span><br></td>
                                </tr>
                            </tbody>
                        </table>
                </div>
                <div class="col-sm-12">
                    <br>
                </div>
            </div>
    ';
    return $output;
}


?>