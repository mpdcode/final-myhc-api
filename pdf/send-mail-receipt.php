<?php

    include_once '../config/db.php';
    include_once '../objects/v1/booking.php';
    include_once '../objects/v1/constant.php';
    include_once '../objects/v1/email-mgmt.php';
    include_once '../objects/v1/sms-notification.php';

    // get database connection
    $database = new Database();
    $db = $database->getConnection();

    // prepare object
    $emailMgmt = new EmailMgmt($db);
    $emailMgmt->code = "ipay88-thank-you-payment";
    $emailMgmt->readOne();

    $ref_no = "B202202-11#1";
    $booking = new Booking($db);
    $booking_arr = $booking->readByRefNo($ref_no);

    echo $ref_no.'<br><pre>'.json_encode($booking).'</pre>';

    require_once 'receipt.php'; 
    include('pdf.php');
    $file_name = md5(rand()) . '.pdf';
    $html_code = '<link rel="stylesheet" href="bootstrap/bootstrap.min.css">';
    $html_code .= fetch_data($booking_arr);
    $pdf = new Pdf();
    $pdf->load_html($html_code);
    $pdf->render();
    $file = $pdf->output();
    file_put_contents($file_name, $file);
    
   
    require 'class/class.phpmailer.php'; // filepath to the PHPMailer class
    require 'class/class.smtp.php';
   
    $mail = new PHPMailer();
   
   //  $mail->SMTPDebug = 2;
    $mail->Mailer = "smtp";
    $mail->Host = "localhost";
    $mail->Port = 25;
    $mail->SMTPAuth = true; // turn on SMTP authentication
    $mail->Username = CONST_EMAIL_ID; // SMTP username
    $mail->Password = CONST_EMAIL_PASSWORD; // SMTP password 
    $mail->Priority = 1;
   

    $mail->AddAddress($booking_arr['person']['email'],"Name");

    if (isset($emailMgmt->sender)){
        $emailMgmt->paramValues =  array( 
            array("param" => "?name", "value" => $booking_arr['person']['name']),
            array("param" => "?total_payment", "value" => "120.00"),
            array("param" => "?date_of_payment", "value" => "2022-03-10")
        );
        $mail->SetFrom($emailMgmt->sender, "Info");
        $mail->AddReplyTo($emailMgmt->sender, "Info");
        $mail->Subject  = $emailMgmt->title;
        $mail->Body     = $emailMgmt->readMailContent();
    }


    $mail->WordWrap = 50;  
    $mail->IsHTML(true);       //Sets message type to HTML    
    $mail->AddAttachment($file_name);         //Adds an attachment from a path on the filesystem
   
    if(!$mail->Send()) {
        echo 'Message was not sent.';
        echo 'Mailer error: ' . $mail->ErrorInfo;
    } else {
        echo 'Message has been sent.';
    }
   
    unlink($file_name);

    $smsNotification = new SmsNotification($db);
    $smsNotification->code = "ipay88-thank-you-payment";
    $smsNotification->readOne();
    $smsNotification->mobile_no = "60". $booking_arr['person']['mobile_no'];
    $smsNotification->paramValues =  array( 
         array("param" => "?total_payment", "value" => "120.00")
    );
    $smsNotification->sendSms();
 
?>