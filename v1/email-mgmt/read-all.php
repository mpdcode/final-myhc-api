<?php
/**
 * Author: Majina
 * EmailMgmt.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/email-mgmt/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/email-mgmt.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$emailMgmt = new EmailMgmt($db);

// query data
$stmt = $emailMgmt->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $emailMgmt_arr=array();
    $emailMgmt_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $emailMgmt_item=array(
            "code" => $code,
			"title" => $title,
            "content" => $content,
            "sender" => $sender,
        );
  
        array_push($emailMgmt_arr["data"], $emailMgmt_item);
        $total_records++;
    }

    $emailMgmt_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($emailMgmt_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No Email Content has found.","error" => "404 Not found")
    );
}
?>