<?php
/**
 * Author: Majina
 * Company.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/company/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/company.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$company = new Company($db);

// query data
$stmt = $company->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $company_arr=array();
    $company_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $company_item=array(
            "co_reg_no" => $co_reg_no,
			"name" => $name,
            "address" => $address,
            "town" => $town,
			"district" => $district,
            "postcode" => $postcode,
            "state" => $state,
			"geocode" => $geocode,
            "pic_url" => $pic_url,
            "contact_no" => $contact_no,
			"email" => $email

        );
  
        array_push($company_arr["data"], $company_item);
        $total_records++;
    }


    $company_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($company_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No Company has found.","error" => "404 Not found")
    );
}
?>