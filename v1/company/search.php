<?php
/**
 * Author: Elizha
 * Company.search()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/company/search.php?s=<keyword>
 
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// include database and object files
include_once '../../config/core.php';
include_once '../../config/db.php';
include_once '../../objects/v1/company.php';
  
// instantiate database and company object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$company = new Company($db);
  
// get keywords
$keywords=isset($_GET["s"]) ? $_GET["s"] : "";
  
// query data
$stmt = $company->search($keywords);
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $company_arr=array();
    $company_arr["data"]=array();
  
    $total_records = 0;
    
    // retrieve our table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $company_rec=array(   
          
            "co_reg_no" => $co_reg_no,
			"name" => $name,
            "address" => $address,
            "town" => $town,
			"district" => $district,
            "postcode" => $postcode,
            "state" => $state,
			"geocode" => $geocode,
            "pic_url" => $pic_url,
            "contact_no" => $contact_no,
			"email" => $email

        );
  
        array_push($company_arr["data"], $company_rec);
        $total_records++;
    }
  
    $company_arr["total_records"]=$total_records;

    // set response code - 200 OK
    http_response_code(200);
  
    // show data
    echo json_encode($company_arr);
}
  
else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not found
    echo json_encode(
        array("message" => "No Company found.","errorFound"=>true,"error"=>"404 Not found")
    );
}
?>