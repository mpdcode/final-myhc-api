<?php
/**
 * Author: Majina
 * Company.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/company/read-one.php?c=<co_reg_no>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/company.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$company = new Company($db);
  
// set ID property of record to read
$company->co_reg_no = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$company->readOne();
  
if (isset*$company->co_reg_no)){
 
    // create array
    $company_arr = array(
        "co_reg_no" => $company->co_reg_no,
        "name" => $company->name,
        "address" => $company->address,
        "town" => $company->town,
        "district" => $company->district,
        "postcode" => $company->postcode,
        "state" => $company->state,
        "geocode" => $company->geocode,
        "pic_url" => $company->pic_url,
        "contact_no" => $company->contact_no,
        "email" => $company->email
    );
  
    
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($company_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Company info does not exist for " . $company->co_reg_no,"error" => "404 Not found"));
}
?>