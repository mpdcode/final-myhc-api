<?php
/**
 * Author: Elizha
 * AddOn.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/add-on/create.php
 * JSON input: { "add_on_code":"<add_on_code>", "name": "<name>", "price":"<price>", "remark":"<remark>", "status":"<status>", 
 * "unit":"<unit>", "unit_decimal":"<unit_decimal>","no_of_patient":"<no_of_patient>", "patient_type_code":"<patient_type_code>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  


if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {    
    return 0;    
}

// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/add-on.php';
  
$database = new Database();
$db = $database->getConnection();

// prepare object
$addOn = new AddOn($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->price) &&
    !empty($data->name) 
){
    
    // set data property values
    $addOn->add_on_code = $data->add_on_code;
    $addOn->name = $data->name;
	$addOn->price = $data->price;
    $addOn->unit = $data->unit;
    $addOn->unit_decimal = $data->unit_decimal;
    $addOn->remark = $data->remark;
    $addOn->status = $data->status;
    $addOn->patient_type_code = $data->patient_type_code;
	$addOn->no_of_patient = $data->no_of_patient;

  
    // create the record
    if($addOn->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Add On Services info was created.","errorFound"=>false,"error" => "",));
    }
  
    // if unable to create record, tell the user
    else{
		// $addOn->code = $data->code;
  		// read the details of record to be edited
		// $addOn->readOne();
		if($addOn->name!=null){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Add On Services info already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Add On Services info.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Add On Services info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>