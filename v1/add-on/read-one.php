<?php
/**
 * Author: Elizha
 * AddOn.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/add-on/read-one.php?c=<code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/add-on.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$addOn = new AddOn($db);

// set ID property of record to read
$addOn->add_on_code = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$addOn->readOne();
  
if($addOn->add_on_code!=null){
    // create array
    $addOn_arr = array(
        "uid"  => $addOn->uid,
        "add_on_code"  => $addOn->add_on_code,
        "name"  => $addOn->name ,
        "price" => $addOn->price,
        "unit"  => $addOn->unit,
        "unit_decimal" => $addOn->unit_decimal,
        "remark" =>$addOn->remark,
        "status" =>$addOn->status,
        "no_of_patient"=>$addOn->no_of_patient,
        "patient_type_code"=>$addOn->patient_type_code
        
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($addOn_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Add On Services info does not exist for " . $addOn->add_on_code,"error" => "404 Not found"));
}
?>