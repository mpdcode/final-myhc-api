<?php
/**
 * Author: Elizha
 * MessageBox.delete()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/message-box/delete.php
 * JSON input: { "message_inbox_code":"<message_inbox_code>", "sender": "<sender>", "receiver":"<receiver>", "subject":"<subject>", "message":"<message>", 
 * "headers":"<headers>", "date_sent":"<date_sent>","message_type_code":"<message_type_code>", "ic_no":"<ic_no>",
 * "status":"<status>", "attachment":"<attachment>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/message-box.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$messageBox = new MessageBox($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted
$messageBox->message_inbox_code = $data->message_inbox_code;

// delete the record
if($messageBox->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Message Inbox info was deleted.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete Message Inbox info.","error"=>"503 service unavailable","errorFound"=>true));
}
?>