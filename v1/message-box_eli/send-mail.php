<?php
/**
 * Author: Elizha
 * EmailMgmt.testSendMail()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/message-box/send-mail.php
 * JSON input: { "message_inbox_code":"<message_inbox_code>", "sender": "<sender>", "receiver":"<receiver>", "subject":"<subject>", "message":"<message>", 
 * "headers":"<headers>", "date_sent":"<date_sent>","message_type_code":"<message_type_code>", "ic_no":"<ic_no>",
 * "status":"<status>", "attachment":"<attachment>"}
 ** Method: GET   
 */

 
ini_set('display_errors',1);
error_reporting(E_ALL);

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/message-box.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$messageBox = new MessageBox($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));

$messageBox->message_inbox_code = $data->message_inbox_code;
$messageBox->receivers = $data->receivers;
  
// read the details of data to be edited
$messageBox->readOne();
  
if($messageBox->code!=null){
 
    $messageBox->paramValues = $data->paramValues;

    if ( $messageBox->sendMail()){
        // set response code - 200 OK
        http_response_code(200);
    
        // make it json format
        echo json_encode(array("message" => "An email has been sent to your mailbox","error" => "200 Success"));
    }else{
       // set response code - 200 OK
       http_response_code(200);
    
       // make it json format
       echo json_encode(array("message" => "Failed to send email","error" => "200 Failed"));

    }

}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Email Content info does not exist for " . $data->message_inbox_code,"error" => "404 Not found"));
}
?>