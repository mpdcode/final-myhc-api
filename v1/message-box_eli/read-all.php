<?php
/**
 * Author: Elizha
 * MessageBox.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/message-box/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/message-box.php';

  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$messageBox = new MessageBox($db);
//$testMarker = new TestMarker($db);

// query data
$stmt = $messageBox->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $messageBox_arr=array();
    $messageBox_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
         
        $messageBox_item=array(
            "message_inbox_code"  => $message_inbox_code,
            "sender"  => $sender ,
            "receiver" => $receiver,
            "subject"  => $subject,
            "message" => $message;
            "headers" => $headers,
            "date_sent" =>$date_sent,
            "message_type_code" =>$message_type_code,
            "ic_no"=>$ic_no,
            "status"=>$status,
            "attachment"=>$attachment
              //"test_markers" => $testMarker->readByPanelCode($code)

        );
  
        array_push($messageBox_arr["data"], $messageBox_item);
        $total_records++;
    }

    $messageBox_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($messageBox_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No Message found.","error" => "404 Not found")
    );
}
?>