<?php
/**
 * Author: Majina
 * TestPanel.search()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-panel/search.php?s=<keyword>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// include database and object files
include_once '../../config/core.php';
include_once '../../config/db.php';
include_once '../../objects/v1/test-panel.php';
  
// instantiate database and patient object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$testPanel = new TestPanel($db);
  
// get keywords
$keywords=isset($_GET["s"]) ? $_GET["s"] : "";
  
// query data
$stmt = $testPanel->search($keywords);
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $testPanel_arr=array();
    $testPanel_arr["data"]=array();
  
    $total_records = 0;
    
    // retrieve our table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $testPanel_rec=array(
            "panel_id" => $panel_id,
            "test_panel_code" => $test_panel_code,
            "name" => $name,
			"description" => $description,
            "input_type" => $input_type
        );
  
        array_push($testPanel_arr["data"], $testPanel_rec);
        $total_records++;
    }
  
    $testPanel_arr["total_records"]=$total_records;

    // set response code - 200 OK
    http_response_code(200);
  
    // show data
    echo json_encode($testPanel_arr);
}
  
else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not found
    echo json_encode(
        array("message" => "No test panel found.","errorFound"=>true,"error"=>"404 Not found")
    );
}
?>