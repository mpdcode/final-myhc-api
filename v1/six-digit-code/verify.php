<?php
/**
 * Author: Majina
 * SixDigitCode.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/six-digit-code/get-one.php?m=<mobile>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

include_once '../../config/db.php';
include_once '../../objects/v1/six-digit-code.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare object
$sixDigitCode = new SixDigitCode($db);

// set ID property of record to read
$data = json_decode(file_get_contents("php://input"));
$sixDigitCode->mobile = $data->mobile;

// read the details of data to be edited
$sixDigitCode->readOne();

//$ts = time();
// $expired = (microtime($ts) * 1000) + (60*60);
$now = time();
 
if (isset($sixDigitCode->code)){ //mobile number has found
    if ($sixDigitCode->code!=$data->code){
        http_response_code(404);
        echo json_encode(array("message" => "Invalid 6 digit code","errorFound"=>true,"error" => "404 Not found"));
    }else{
        if ($now>$sixDigitCode->expired){
            http_response_code(503);
            echo json_encode(array("message" => "Your 6 digit code has already expired","errorFound"=>true,"error" => "Code has expired"));
        }else{
            http_response_code(200);
            echo json_encode(array("message" => "Valid 6 digit code","errorFound"=>true,"error" => null));
        }

    }
}else{
    http_response_code(404);
    echo json_encode(array("message" => "Invalid 6 digit code","errorFound"=>true,"error" => "404 Not found"));
}
?>