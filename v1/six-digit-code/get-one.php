<?php
/**
 * Author: Majina
 * SixDigitCode.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/six-digit-code/get-one.php?m=<mobile>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

include_once '../../config/db.php';
include_once '../../objects/v1/six-digit-code.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare object
$sixDigitCode = new SixDigitCode($db);

// set ID property of record to read
$sixDigitCode->mobile = isset($_GET['m']) ? $_GET['m'] : die();


// read the details of data to be edited
$sixDigitCode->readOne();
$randomid = mt_rand(100000,999999); 
$sixDigitCode->code = $randomid;

//$ts = time();
// $expired = (microtime($ts) * 1000) + (60*60);
$sixDigitCode->expired = time() + (60*60);

$mode = "update";
$mode_result = false;
if (isset($sixDigitCode->date_sent))  {
    if ($sixDigitCode->update()) $mode_result = true;
}else{
    if ($sixDigitCode->create()) $mode_result = true;
    $mode = "create";
}  
  
echo json_encode(array("code" => $randomid, "mobile" => $sixDigitCode->mobile, "mode"=> $mode, "mode-result"=>  $mode_result));
?>