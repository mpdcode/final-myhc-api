<?php
/**
 * Author: Majina
 * Person.search()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/person/search.php?s=<keyword>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// include database and object files
include_once '../../config/core.php';
include_once '../../config/db.php';
include_once '../../objects/v1/person.php';
  
// instantiate database and patient object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$person = new Person($db);
  
// get keywords
$keywords=isset($_GET["s"]) ? $_GET["s"] : "";
  
// query data
$stmt = $person->search($keywords);
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $person_arr=array();
    person_arr["data"]=array();
  
    $total_records = 0;
    
    // retrieve our table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $person_rec=array(
            "reg_no" => $reg_no,
            "ic_no" => $ic_no,
            "name" => $name, 
            "mobile_no" => $mobile_no, 
            "email" => $mobile_no, 
            "age" => $age, 
            "gender" => $gender, 
            "patient_type_code" => $patient_type_code, 
            "address" => $address, 
            "town" => $town, 
            "district" => $district, 
            "postcode" => $postcode, 
            "state" => $state,
            "relationship" => $relationship
        );
  
        array_push($person_arr["data"], $person_rec);
        $total_records++;
    }
  
    $person_arr["total_records"]=$total_records;

    // set response code - 200 OK
    http_response_code(200);
  
    // show data
    echo json_encode($person_arr);
}
  
else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not found
    echo json_encode(
        array("message" => "No Person found.","errorFound"=>true,"error"=>"404 Not found")
    );
}
?>