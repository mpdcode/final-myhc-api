<?php
/**
 * Author: Majina
 * Person.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/person/read-one.php?c=<code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/person.php';
include_once '../../objects/v1/person-document.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$person = new Person($db);
$personDocument = new PersonDocument($db);

// set ID property of record to read
$person->ic_no = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$person->readOne();
  
if (isset($person->name)){
 
    // create array
    $person_arr = array(
        "reg_no" => $person->reg_no,
        "ic_no" => $person->ic_no,
        "name" => $person->name, 
        "mobile_no" => $person->mobile_no, 
        "email" => $person->mobile_no, 
        "age" => $person->age, 
        "gender" => $person->gender, 
        "patient_type_code" => $person->patient_type_code, 
        "address" => $person->address, 
        "town" => $person->town, 
        "district" => $person->district, 
        "postcode" => $person->postcode, 
        "state" => $person->state,
        "photo_path" => $person->photo_path,
        "relationship" => $person->relationship,
        "documents" => $personDocument->readByIcNo($person->ic_no)
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($person_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Person info does not exist for " . $person->ic_no,"error" => "404 Not found"));
}
?>