<?php
ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ALL);
/**
 * Author: Majina
 * Person.createForAdmin()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/person/create-for-admin.php
 * JSON input: { "person": 
 *                  { "reg_no":"<reg_no>", "ic_no":"<ic_no>", "name": "<name>",
 *                  "gender":"<gender>", "age":"<age>", "email": "<email>",
 *                  "mobile_no":"<mobile_no>", "patient_type_code":"<patient_type_code>", "address": "<address>",
 *                  "town":"<town>", "district":"<district>", "postcode": "<postcode>", "state": "<state>"},
 *               "registration" : 
 *                  { "ic_no": "<ic_no>", "reg_no": "<reg_no>", 
 *                  "person_type_code":"<person_type_code>", "admin_type":"<admin_type>"}
 *             }
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/person.php';
include_once '../../objects/v1/user-account.php';
include_once '../../objects/v1/sms-notification.php';
include_once '../../objects/v1/email-mgmt.php';
  
$database = new Database();
$db = $database->getConnection();
  
$person = new Person($db);
$smsNotification = new SmsNotification($db);
$emailMgmt = new EmailMgmt($db);
  
// get posted data
$dataPerson = json_decode(file_get_contents("php://input"));

  
// make sure data is not empty
if (
    !empty($dataPerson->ic_no) &&
    !empty($dataPerson->name) &&
    !empty($dataPerson->email) &&
    !empty($dataPerson->mobile_no)
){
  
   

    // person does not registered to any package plan and ready for registration
    // set data property values
    $person->ic_no = $dataPerson->ic_no;
    $person->name = $dataPerson->name;
    $person->age = $dataPerson->age;
    $person->email = $dataPerson->email;
    $person->mobile_no = $dataPerson->mobile_no;
    $person->gender = $dataPerson->gender;
    $person->patient_type_code=$dataPerson->patient_type_code;
    $person->address = $dataPerson->address;
    $person->town = $dataPerson->town;
    $person->district = $dataPerson->district;
    $person->postcode = $dataPerson->postcode;
    $person->state = $dataPerson->state;
    $person->relationship = $dataPerson->relationship;


    // create the record
    if($person->create()){
 
        //create user account record
        $userAccount = new UserAccount($db);
        $id = sprintf( '%005d', $userAccount->getNextId());
        $userAccount->username = 'ADM'. $id ;  //P = prefix utk portal patient, id = id seq reg no,
        $password = $userAccount->randomPassword(8);
        $userAccount->password = $userAccount->encryptPassword($password);
        $userAccount->ic_no = $dataPerson->ic_no;
        $userAccount->acc_type_code = "ADMIN";
        $userAccount->acc_status_code = "APPROVED";
        $userAccount->menu_owner = $dataPerson->user_group;
        $userAccount->reg_no = "ADMIN";
        $userAccount->date_created = date('Y-m-d H:i:s');

        if ($userAccount->create()){

            //send sms notification
            $smsNotification->code = "acc-admin-created";
            $smsNotification->readOne();
            if (isset($smsNotification->code)){
                $smsNotification->mobile_no = "60". $person->mobile_no;
                $smsNotification->paramValues =  array( 
                    array("param" => "?username", "value" =>$userAccount->username) ,
                    array("param" => "?password", "value" =>$password) 
                );
                $smsNotification->sendSms();
            }

            //send email
            $emailMgmt->code = "acc-admin-created";
            $emailMgmt->readOne();
            if (isset($emailMgmt->sender)){
                $emailMgmt->receivers = $person->email;
                $emailMgmt->paramValues = $smsNotification->paramValues;
                $emailMgmt->sendMail();
            }

            // set response code - 201 created
            http_response_code(201);

            // tell the user
            echo json_encode(array("message" => "Admin account has been successfully created.".$dataPerson->user_group,"errorFound"=>false,"error" => "",));

        }else{
            // set response code - 503 service unavailable
            http_response_code(503);

            // tell the user
            echo json_encode(array("message" => "Unable to create admin account with new person info.".$id,"errorFound"=>true,"error" => "Service unavailable"));

        }    }

    // if unable to create record, tell the user
    else{
        $person->ic_no = $dataPerson->ic_no;
        // read the details of patient to be edited
        $person->readOne();
        if (isset($person->name)){

            //create user account record
            $userAccount = new UserAccount($db);
            $id = sprintf( '%005d', $userAccount->getNextId());
            $userAccount->username = 'ADM'. $id ;  //P = prefix utk portal patient, id = id seq reg no,
            $password = $userAccount->randomPassword(8);
            $userAccount->password = $userAccount->encryptPassword($password);
            $userAccount->ic_no = $dataPerson->ic_no;
            $userAccount->acc_type_code = "ADMIN";
            $userAccount->acc_status_code = "APPROVED";
            $userAccount->menu_owner = $dataPerson->user_group;
            $userAccount->reg_no = "ADMIN";
            $userAccount->date_created = date('Y-m-d H:i:s');

            if ($userAccount->create()){

                //send sms notification
                $smsNotification->code = "acc-admin-created";
                $smsNotification->readOne();
                if (isset($smsNotification->code)){
                    $smsNotification->mobile_no = "60". $person->mobile_no;
                    $smsNotification->paramValues =  array( 
                        array("param" => "?username", "value" =>$userAccount->username) ,
                        array("param" => "?password", "value" =>$password) 
                    );
                    $smsNotification->sendSms();
                }

                //send email
                $emailMgmt->code = "acc-admin-created";
                $emailMgmt->readOne();
                if (isset($emailMgmt->sender)){
                    $emailMgmt->receivers = $person->email;
                    $emailMgmt->paramValues = $smsNotification->paramValues;
                    $emailMgmt->sendMail();
                }

                // set response code - 201 created
                http_response_code(201);

                // tell the user
                echo json_encode(array("message" => "Admin account has been successfully created.","errorFound"=>false,"error" => "",));

            }else{
                // set response code - 503 service unavailable
                http_response_code(503);

                // tell the user
                echo json_encode(array("message" => "Unable to create admin account with existing person info.".$id,"errorFound"=>true,"error" => "Service unavailable"));

            }

        }else{
            // set response code - 503 service unavailable
            http_response_code(503);

            // tell the user
            echo json_encode(array("message" => "Unable to create admin account - Person info not found.","errorFound"=>true,"error" => "Service unavailable"));
        }

    }
    
 

  
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Person info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>