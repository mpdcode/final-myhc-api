<?php
/**
 * Author: Majina
 * TestResultIInput.delete()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-marker/delete.php
 * JSON input: [{ "patient_ic_no":<patient_ic_no>, "booking_no":"<booking_no>", "test_panel_code": "<test_panel_code>", "test_marker_code": "<test_marker_code>", 
 *          "test_date":"<test_date>","test_value":"<test_value>", "source":"<source>","date_updated":"<date_updated>"}]
 * Method: POST 
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/test-result-input.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$testResultInput = new TestResultInput($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted
$testResultInput->test_panel_code = $data->test_panel_code;
$testResultInput->patient_ic_no = $data->patient_ic_no;
$testResultInput->test_date = $data->test_date;
  
// delete the record
if($testResultInput->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Test Result info has been deleted.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete Test Result info.","error"=>"503 service unavailable","errorFound"=>true));
}
?>