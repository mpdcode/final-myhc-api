<?php
/**
 * Author: Majina
 * TestResultInput.readAllResult()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-result-input/read-all-result.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/test-result-input.php';
include_once '../../objects/v1/person.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$testResultInput = new TestResultInput($db);

// query data
$arr_data = $testResultInput->readAllResult();
    
// set response code - 200 OK
http_response_code(200);

// show data in json format
echo json_encode($arr_data);
 
?>