<?php
/**
 * Author: Majina
 * TestResultInput.search()
 * URL for testing : https://myhc.my/myhc-api/v1/test-result-input/result-dashboard-by-ic-no.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/test-result-input.php';
include_once '../../objects/v1/person.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$testResultInput = new TestResultInput($db);

// get keywords
$keywords=isset($_GET["s"]) ? $_GET["s"] : "";

// query data
$arr_data = $testResultInput->resultDashboardRefByIcNo($keywords);
    
// set response code - 200 OK
http_response_code(200);

// show data in json format
echo json_encode($arr_data);
 
?>