<?php

ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ALL);

/**
 * Author: Majina
 * TestResultInput.push()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-result-input/push-result.php
 * JSON input: [{ "patient_ic_no":<patient_ic_no>, "booking_no":"<booking_no>", "test_panel_code": "<test_panel_code>", "test_marker_code": "<test_marker_code>", 
 *          "test_date":"<test_date>","test_value":"<test_value>", "source":"<source>","date_updated":"<date_updated>"}]
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';

// get auth class object
require_once '../../auth/jwt_utils.php';
  
// instantiate class object
include_once '../../objects/v1/test-result-input.php';


$bearer_token = get_bearer_token();
$is_jwt_valid = is_jwt_valid($bearer_token);

if ($is_jwt_valid) {

    
$database = new Database();
$db = $database->getConnection();
  
$testResultInput = new TestResultInput($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"),true);

$response_arr=[];

foreach ($data as $item) {
    if (
        !empty($item['patient_ic_no']) &&
        !empty($item['test_marker_code']) &&
        // !empty($item['booking_no']) &&
        !empty($item['test_value'])
    ){

        // $response_arr[] = array("message" => "upload in progress.","errorFound"=>false,"error" => "");
        // set data property values
        $testResultInput->test_marker_code = $item['test_marker_code'];
        $testResultInput->test_panel_code = $item['test_panel_code'];
        $testResultInput->patient_ic_no = $item['patient_ic_no'];
        $testResultInput->booking_no = $item['booking_no'];
        $testResultInput->test_date = $item['test_date'];
        $testResultInput->test_value = $item['test_value'];
        $testResultInput->source = $item['source'];
        $testResultInput->date_updated = date("Y-m-d h:i a");

       
        // create the record
        if ($testResultInput->create()){
            $response_arr[] = array("message" => $testResultInput->date_updated . " : " . $item["booking_no"] ." : ". $item["patient_ic_no"] ." : ".$item['test_marker_code'] ." = " . $item['test_value'] . " -> Test result info was created.","errorFound"=>false,"error" => "");
        }
        // if unable to create record, tell the user
        else{
            if($testResultInput->patient_ic_no!=null){
                // record already exist and let the record to be updated
                if ($testResultInput->update()){
                    $response_arr[] = 
                    array("message" =>  $testResultInput->date_updated . " : " . $item["booking_no"] ." : ".$item["patient_ic_no"] ." : ".$item['test_marker_code'] ." = " . $item['test_value'] . " ->  Test result info was updated.","errorFound"=>false,"error" => "");
                }else{
                    $response_arr[] = array("message" =>  $testResultInput->date_updated . " : " . $item["booking_no"] ." : ".$item["patient_ic_no"] ." : ".$item['test_marker_code'] ." = " . $item['test_value'] . " ->  Unable to update Test result info.","errorFound"=>false,"error" => "");
                }
            }else{
                $response_arr[] = array("message" =>  $testResultInput->date_updated . " : " . $item["booking_no"] ." : ".$item["patient_ic_no"] ." : ".$item['test_marker_code'] ." = " . $item['test_value'] . " ->  Unable to create Test result info.","errorFound"=>true,"error" => "503 service unavailable");
            }

        }
         
    }else{
        $response_arr[] = array("message" =>   " ->  Unable to create Test result info - Data Incomplete.","errorFound"=>true,"error" => "503 service unavailable");
    }
} 

echo json_encode($response_arr);
  
} else {

	echo json_encode(array('message'=>'Authorization failed', 'error' => 'Access denied'));

}

   
?>