<?php
/**
 * Author: Majina
 * companyDocument.delete()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/company-document/delete.php
 * JSON input: { "co_reg_no":"<co_reg_no>", "document_code":"<document_code>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/company-document.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$companyDocument = new CompanyDocument($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted
$companyDocument->co_reg_no = $data->co_reg_no;
$companyDocument->filename = $data->filename;

// delete the record
if($companyDocument->delete()){

    unlink($data->file_path);
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Document has been removed.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to remove document.","error"=>"503 service unavailable","errorFound"=>true));
}
?>