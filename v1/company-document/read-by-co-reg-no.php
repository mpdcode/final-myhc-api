<?php
/**
 * Author: Majina
 * companyDocument.readByCoRegNo()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/company-document/read-by-co-reg-no.php?c=<code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/company-document.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$companyDocument = new CompanyDocument($db);
  
// set ID property of record to read
$companyDocument->co_reg_no = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$result = $companyDocument->readByCoRegNo($companyDocument->co_reg_no);
  
// if(sizeof($result)>0){
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    // echo json_encode($result);
    echo json_encode(array("data" => $result,"errorFound"=>false,"error" => null));
// }else{
    // set response code - 404 Not found
    // http_response_code(200);
  
    // tell the user that record does not exist
    // echo json_encode(array("message" => "Person document does not exist for " . $companyDocument->ic_no,"errorFound"=>true,"error" => "Not found"));
// }
?>