<?php
/**
 * Author: Majina
 * BookingProduct.update()
 * URL for testing : https://myhc.com/myhc-api/v1/booking-product/update.php 
 * JSON input: { "booking_id":"<booking_id>", "code":"<code>", "name":"<name>", 
 * "price":"<price>", "quantity":"<quantity>", "total_price":"<total_price>" ,
 * "remark":"<remark>" }
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/booking-product.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$bookingProduct = new BookingProduct($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited, 1 PK
$bookingProduct->booking_id = $data->booking_id;
  
// set data property values
$bookingProduct->code = $data->code;
$bookingProduct->name = $data->name;
$bookingProduct->patient_type = $data->patient_type;
$bookingProduct->price = $data->price;
$bookingProduct->quantity =  $data->quantity;
$bookingProduct->total_price =  $data->total_price;
$bookingProduct->remark =  $data->remark; 
$bookingProduct->ref_no =  $data->ref_no; 

  
// update the record
if($bookingProduct->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Booking's product info was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update Booking's product info.","errorFound"=>true,"error"=>"Service unavailable"));
}
?>