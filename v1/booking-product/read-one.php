<?php
/**
 * Author: Majina
 * BookingProduct.readOne()
 * URL for testing : https://myhc.my/myhc-api/v1/booking-product/read-one.php?c=<code>&b=<booking_id>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/booking-product.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$bookingProduct = new BookingProduct($db);
  
// set ID property of record to read
$bookingProduct->code = isset($_GET['c']) ? $_GET['c'] : die();
$bookingProduct->booking_id = isset($_GET['b']) ? $_GET['b'] : die();
  
// read the details of data to be edited
$bookingProduct->readOne();
  
if ($bookingProduct->booking_id!=null){
 
    // create array
    $booking_arr = array(
        "booking_id" => $booking_id,
        "code" => $code,
        "patient_type" => $patient_type,
        "name" => $name,
        "price" => $price,
        "quantity" => $quantity,
        "total_price" => $total_price,
        "remark" => $remark ,
        "ref_no" => $ref_no 
    );
  
    
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($booking_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Booking's product info does not exist for " . $bookingProduct->booking_id,"error" => "404 Not found"));
}
?>