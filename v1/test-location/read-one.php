<?php
/**
 * Author: Elizha
 * TestLocation.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-location/read-one.php?c=<code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/test-location.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$testLocation = new TestLocation($db);

// set ID property of record to read
$testLocation->code = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$testLocation->readOne();
  
if($testLocation->code!=null){
    // create array
    $testLocation_arr = array(
        "code"  => $testLocation->code,
        "name"  => $testLocation->name 
        
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($testLocation_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Test Location info does not exist for " . $testLocation->code,"error" => "404 Not found"));
}
?>