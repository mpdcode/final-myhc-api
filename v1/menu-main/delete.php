<?php
/**
 * Author: Majina
 * MenuMain.delete()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/menu-main/delete.php
 * JSON input: { "main_id":"<main_id>", "title":"<title>", "owner": "<owner>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/menu-main.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$menuMain = new MenuMain($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted
$menuMain->main_id = $data->main_id;
  
// delete the record
if($menuMain->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Main menu info was deleted.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete Main Menu info.","error"=>"503 service unavailable","errorFound"=>true));
}
?>