<?php
/**
 * Author: Majina
 * MenuMain.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/menu-main/read-one.php?c=<code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/menu-main.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$menuMain = new MenuMain($db);
  
// set ID property of record to read
$menuMain->main_id = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$menuMain->readOne();
  
if (isset($menuMain->title)){
 
    // create array
    $menuMain_arr = array(
        "main_id" => $menuMain->main_id,
		"title" => $menuMain->title,
        "owner" => $menuMain->owner
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($menuMain_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Main menu info does not exist for " . $_GET['c'] ,"error" => "404 Not found"));
}
?>