<?php
/**
 * Author: Majina
 * MenuMain.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/menu-main/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/menu-main.php';
include_once '../../objects/v1/menu-item.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$menuMain = new MenuMain($db);
$menuItem = new MenuItem($db);

// query data
$stmt = $menuMain->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $menuMain_arr=array();
    $menuMain_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $menuMain_item=array(
            "main_id" => $main_id,
			"title" => $title,
            "owner" => $owner,
            "lang_bm"=> $lang_bm,
            "items" => $menuItem->readByMainId($main_id)
        );
  
        array_push($menuMain_arr["data"], $menuMain_item);
        $total_records++;
    }

    $menuMain_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($menuMain_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No main menu has found.","error" => "404 Not found")
    );
}
?>