<?php

ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ALL);

/**
 * Author: Majina
 * URL for testing : https://myhc.live/myhc-api/v1/screening-center/photo-upload.php
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
// header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/screening-center.php';
 

if($_SERVER['REQUEST_METHOD']=="POST")
{

  $database = new Database();
  $db = $database->getConnection();
    
  $screeningCenter = new ScreeningCenter($db);
  
  $target_dir = $_SERVER['DOCUMENT_ROOT'] . "/" . "upload_files/screening_center/";

  $uploadOk = 1;

  $sc_id = $_POST["sc_id"];
  // $sc_id = $_REQUEST["sc_id"]

  $errorFound = false;

  // Check if file already exists
  if (!isset($sc_id)) {
    $msg = "Data incomplete - screening center id";
    $errorFound=true;
  }

  // Check if file already exists
  // if (file_exists($target_file) && !$errorFound) {
  //     $msg = "File already exists. Please remove the existing uploaded file before upload";
  //     $errorFound=true;
  // }

  // Check file size
  //print_r($_FILES);
  //echo $sc_id;
  if ($_FILES["fileToUpload"]["size"][0] > 3072000  && !$errorFound) {
      $msg = "Your file is too large exceeding maximum size of 3MB.";
      $errorFound=true;
  }

  // // Allow certain file formats
  // if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
  // && $imageFileType != "gif"  && !$errorFound) {
  //     $msg = "Only JPG, JPEG, PNG and GIF files are allowed.";
  //     $errorFound=true;
  // }

  if (!$errorFound){
    $file_names = $_FILES["fileToUpload"]['name'];

    for ($i = 0; $i < sizeof($file_names); $i++) {
      $fileName = $file_names[$i];
      
      // $fileNameCmps = explode(".", $fileName);
      // $fileExtension = strtolower(end($fileNameCmps));
      // $newFilename = $ic_no .'_'.$document_code.'.'.$fileExtension;

      $newFilename = $sc_id .'_'.str_replace(' ','_',$fileName);
      $target_file = $target_dir . $newFilename;

      if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"][$i], $target_file) ) {

        $screeningCenter->sc_id = $sc_id;
        $screeningCenter->readOne();
        $screeningCenter->sc_picture_path = $newFilename;

        if ($screeningCenter->update()){
          $msg = "The file ". htmlspecialchars( basename($fileName)). " has been uploaded.";
          $errorFound=false;
        }else{
          $msg = "Sorry, there was an error uploading your file. ";
          $errorFound=true;
        }

      } else {
        $msg = "Sorry, your file is not uploaded. ";
        $errorFound=true;
      }
    }
  } 
}else{
  $msg = "Sorry, invalid request method. ";
  $errorFound=true;
}

if ($errorFound) http_response_code(503); else http_response_code(200);
echo json_encode(array("message" => $msg,"errorFound" => $errorFound));
?>