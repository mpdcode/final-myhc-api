<?php
/**
 * Author: Elizha
 * ContentMgmt.search()
 * URL for testing : https://myhealthcard.my/myhc-api/v1/content-mgmt/search.php?s=<keyword>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// include database and object files
include_once '../../config/core.php';
include_once '../../config/db.php';
include_once '../../objects/v1/content-mgmt.php';
  
// instantiate database and patient object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$contentMgmt = new ContentMgmt($db);
  
// get keywords
$keywords=isset($_GET["s"]) ? $_GET["s"] : "";
  
// query data
$stmt = $addOn->search($keywords);
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $contentMgmt_arr=array();
    $contentMgmt_arr["data"]=array();
  
    $total_records = 0;
    
    // retrieve our table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
        
        $contentMgmt_rec=array(   
            
            "name"  => $name ,
            "content" => $content,
            "date_updated"  => $date_updated
        
        );
  
        array_push($contentMgmt_arr["data"], $contentMgmt_rec);
        $total_records++;
    }
  
    $contentMgmt_arr["total_records"]=$total_records;

    // set response code - 200 OK
    http_response_code(200);
  
    // show data
    echo json_encode($contentMgmt_arr);
}
  
else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not found
    echo json_encode(
        array("message" => "No Content Mgmt found.","errorFound"=>true,"error"=>"404 Not found")
    );
}
?>