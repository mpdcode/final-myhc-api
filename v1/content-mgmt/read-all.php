<?php
/**
 * Author: Elizha
 * ContentMgmt.readAll()
 * URL for testing : https://myhealthcard.my/myhc-api/v1/content-mgmt/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/content-mgmt.php';
//include_once '../../objects/v1/test-marker.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$contentMgmt = new ContentMgmt($db);
//$testMarker = new TestMarker($db);

// query data
$stmt = $contentMgmt->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $contentMgmt_arr=array();
    $contentMgmt_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
         
        $contentMgmt_item=array(
          
            "name"  => $name ,
            "content" => $content,
            "date_updated"  => $date_updated
              //"test_markers" => $testMarker->readByPanelCode($code)
        );
  
        array_push($contentMgmt_arr["data"], $contentMgmt_item);
        $total_records++;
    }

    $contentMgmt_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($contentMgmt_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No Content Mgmt found.","error" => "404 Not found")
    );
}
?>