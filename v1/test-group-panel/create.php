<?php
/**
 * Author: Majina
 * TestGroupPanel.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/registration-person/create.php
 * JSON input: { "test_group_code":"<test_group_code>", "test_panel_code":"<test_panel_code>"}
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/test-group-panel.php';
  
$database = new Database();
$db = $database->getConnection();
  
$testGroupPanel = new TestGroupPanel($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->test_group_code) &&
    !empty($data->test_panel_code)  
){
  
    // set data property values
    $testGroupPanel->test_group_code=$data->test_group_code;
    $testGroupPanel->test_panel_code=$data->test_panel_code;
 
  
    // create the record
    if($testGroupPanel->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Test Group Panel has been created.","errorFound"=>false,"error" => "",));
    }
  
    // if unable to create record, tell the user
    else{
		$testGroupPanel->test_group_code = $data->test_group_code;
        $testGroupPanel->test_panel_code = $data->test_panel_code;
  		// read the details of patient to be edited
		$testGroupPanel->readOne();
		if (isset($testGroupPanel->test_panel_code)){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Test Group Panel already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Test Group Panel","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Person registration. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>