<?php
/**
 * Author: Majina
 * Booking.create()
 * URL for testing : https://myhc.my/myhc-api/v1/booking/create.php
 * JSON input: { "booking_no":"<booking_no>", "reg_no":"<reg_no>", "ic_no":"<ic_no>", 
 * "screening_location":"<screening_location>", "screening_center_id":"<screening_center_id>", "screening_date":"<screening_date>" ,
 * "screening_time":"<screening_time>" }
 *  
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {    
    return 0;    
} 

// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/booking.php';
include_once '../../objects/v1/booking-patient.php';
include_once '../../objects/v1/booking-product.php';
  
$database = new Database();
$db = $database->getConnection();
  
$booking = new Booking($db);
$bookingPatient = new BookingPatient($db);
$BookingProduct = new BookingProduct($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->reg_no) &&
    !empty($data->ic_no) &&
    !empty($data->screening_date) &&
    !empty($data->screening_time) 
    ){
  
    $booking->booking_id =  $booking->getNextId();
    $booking_no = "B" . date("Ym") ."-" . $booking->booking_id; 
    $booking->reg_no = $data->reg_no;
	$booking->booking_no = $booking_no;
    $booking->ic_no = $data->ic_no;
    $booking->screening_location = $data->screening_location;
    $booking->screening_center_id = $data->screening_center_id;
    $booking->screening_date = $data->screening_date;
    $booking->screening_time =  $data->screening_time;
    date_default_timezone_set('Asia/Kuala_Lumpur');
    $booking->date_modified = date("Y-m-d h:i:s");

    $products  = $data->products;
    $patients  = $data->patients;
  
    // create the record
    if($booking->create()){
  
        foreach ($products as $product) {
            $bookingProduct = new BookingProduct($db);
            $bookingProduct->booking_id =  $booking->booking_id;
            $bookingProduct->code = $product->code;
            $bookingProduct->name = $product->name;
            $bookingProduct->patient_type = $product->patient_type;
            $bookingProduct->price = $product->price;
            $bookingProduct->quantity = $product->quantity;
            $bookingProduct->total_price = $product->total_price;
            $bookingProduct->remark = $product->remark;
            $bookingProduct->ref_no = $booking->booking_no .'#1';
            $bookingProduct->create();
        }

        foreach ($patients as $patient) {
            $bookingPatient = new BookingPatient($db);
            $bookingPatient->booking_id =  $booking->booking_id;
            $bookingPatient->ic_no = $patient->ic_no;
            $bookingPatient->create();
        }
        
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Booking info has been created.","errorFound"=>false,"error" => "","content"=>$booking));
    }
  
    // if unable to create record, tell the user
    else{
  		// read the details of patient to be edited
		$booking->readOne();
		if($booking->booking_no!=null){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Booking info already exist","errorFound"=>true,"error" => "Conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create booking info.","errorFound"=>true,"error" => "Service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create booking info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>