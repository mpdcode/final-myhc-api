<?php
/**
 * Author: Majina
 * Booking.readOne()
 * URL for testing : https://myhc.my/myhc-api/v1/booking/read-one.php?c=<booking_no>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/booking.php';
include_once '../../objects/v1/booking-patient.php';
include_once '../../objects/v1/booking-product.php';
include_once '../../objects/v1/screening-center.php';
include_once '../../objects/v1/person.php';
include_once '../../objects/v1/payment.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$booking = new Booking($db);
$bookingProduct = new BookingProduct($db);
$bookingPatient = new BookingPatient($db);
$screeningCenter = new ScreeningCenter($db);
$person = new Person($db);
$payment = new Payment($db);
  
// set ID property of record to read
$booking->booking_no = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$booking->readOne();
  
if($booking->reg_no!=null){
 
    // create array
    $arr = array(
        "booking_id" => $booking->booking_id,
        "booking_no" => $booking->booking_no,
        "reg_no" => $booking->reg_no,
        "ic_no" => $booking->ic_no,
        "person" => $person->readByIcNo($booking->ic_no),
        "screening_location" => $booking->screening_location,
        "screening_center_id" => $booking->screening_center_id,
        "screening_center" => $screeningCenter->readById($booking->screening_center_id),
        "screening_date" => $booking->screening_date,
        "screening_time" => $booking->screening_time,
        "date_modified" => $booking->date_modified,
        "status" => $booking->status,
        "remark" => $booking->remark,
        "products" => $bookingProduct->readByBookingId($booking->booking_id),
        "patients" => $bookingPatient->readByBookingId($booking->booking_id),
        "payments" => $payment->readByBookingNo($booking->booking_no)
    );

    
    
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Booking info does not exist for " . $booking->booking_no,"error" => "404 Not found"));
}
?>