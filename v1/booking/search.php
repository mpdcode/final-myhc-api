<?php
/**
 * Author: Majina
 * booking.search()
 * URL for testing : https://myhc.my/myhc-api/v1/booking/search.php?s=<keyword>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// include database and object files
include_once '../../config/core.php';
include_once '../../config/db.php';
include_once '../../objects/v1/booking.php';
include_once '../../objects/v1/booking-product.php';
include_once '../../objects/v1/booking-patient.php';
include_once '../../objects/v1/person.php';
include_once '../../objects/v1/screening-center.php';
include_once '../../objects/v1/user-account.php';
  
// instantiate database and patient object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$booking = new Booking($db);
$bookingProduct = new BookingProduct($db);
$bookingPatient = new BookingPatient($db);
$person = new Person($db);
$screeningCenter = new ScreeningCenter($db);
$userAccount = new UserAccount($db);
  
// get keywords
$keywords=isset($_GET["s"]) ? $_GET["s"] : "";
  
// query data
$stmt = $booking->search($keywords);
$num = $stmt->rowCount();
  
// check if more than 0 record found
// if($num>0){
  
    // record array
    $booking_arr=array();
    $booking_arr["data"]=array();
  
    $total_records = 0;
    
    // retrieve our table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
        $userAccount->ic_no = $ic_no;
        $booking_rec=array(
            "booking_id" => $booking_id,
            "booking_no" => $booking_no,
			"reg_no" => $reg_no,
            "ic_no" => $ic_no,
            "person" => $person->readByIcNo($ic_no),
            "account" => $userAccount->readOneByIcNo_array($ic_no),
            "screening_location" => $screening_location,
			"screening_center_id" => $screening_center_id,
            "screening_center" => $screeningCenter->readById($screening_center_id),
            "screening_date" => $screening_date,
            "screening_time" => $screening_time,
            "date_modified" => $date_modified,
            "status" => $status,
            "remark" => $remark,
            "products" => $bookingProduct->readByBookingId($booking_id),
            "patients" => $bookingPatient->readByBookingId($booking_id)
        );
  
        array_push($booking_arr["data"], $booking_rec);
        $total_records++;
    }
  
    $booking_arr["total_records"]=$total_records;

    // set response code - 200 OK
    http_response_code(200);
  
    // show data
    echo json_encode($booking_arr);
// }
  
// else{
//     // set response code - 404 Not found
//     http_response_code(404);
  
//     // tell the user that record does not found
//     echo json_encode(
//         array("message" => "No booking found.","errorFound"=>true,"error"=>"404 Not found")
//     );
// }
?>