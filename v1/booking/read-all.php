<?php
/**
 * Author: Majina
 * booking.readAll()
 * URL for testing : https://myhc.my/myhc-api/v1/booking/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/booking.php';
include_once '../../objects/v1/booking-product.php';
include_once '../../objects/v1/booking-patient.php';
include_once '../../objects/v1/person.php';
include_once '../../objects/v1/screening-center.php';
include_once '../../objects/v1/user-account.php';

// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$booking = new Booking($db);
$bookingProduct = new BookingProduct($db);
$bookingPatient = new BookingPatient($db);
$person = new Person($db);
$screeningCenter = new ScreeningCenter($db);
$userAccount = new UserAccount($db);

// query data
$stmt = $booking->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $booking_arr=array();
    $booking_arr["data"]=array();

   
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
        $userAccount->ic_no = $ic_no;
        $booking_item=array(
            "booking_id" => $booking_id,
            "booking_no" => $booking_no,
			"reg_no" => $reg_no,
            "ic_no" => $ic_no,
            "person" => $person->readByIcNo($ic_no),
            "account" => $userAccount->readOneByIcNo_array($ic_no),
            "screening_location" => $screening_location,
			"screening_center_id" => $screening_center_id,
            "screening_center" => $screeningCenter->readById($screening_center_id),
            "screening_date" => $screening_date,
            "screening_time" => $screening_time,
            "date_modified" => $date_modified,
            "status" => $status,
            "remark" => $remark,
            "products" => $bookingProduct->readByBookingId($booking_id),
            "patients" => $bookingPatient->readByBookingId($booking_id)
        );
  
        array_push($booking_arr["data"], $booking_item);
        $total_records++;
    }


    $booking_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($booking_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No booking has found.","error" => "404 Not found")
    );
}
?>