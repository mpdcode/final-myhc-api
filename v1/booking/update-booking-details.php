<?php
/**
 * Author: Majina
 * Booking.updateBookingDetails()
 * URL for testing : https://myhc.com/myhc-api/v1/booking/update-booking-details.php 
 * JSON input: { "booking_no":"<booking_no>", "reg_no":"<reg_no>", "ic_no":"<ic_no>", 
 * "screening_location":"<screening_location>", "screening_center_id":"<screening_center_id>", "screening_date":"<screening_date>" ,
 * "screening_time":"<screening_time>" }
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");



ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ALL);


if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {    
    return 0;    
} 
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/booking.php';
include_once '../../objects/v1/constant.php';
include_once '../../objects/v1/email-mgmt.php';
include_once '../../objects/v1/sms-notification.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
$updateBooking = new Booking($db);  
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
$updateType = isset($_GET['t']) ? $_GET['t'] : die();

if (!isset($updateType)){
        // set response code - 503 service unavailable
        http_response_code(503);

        // tell the user
        echo json_encode(array("message" => "Unable to update booking info.","errorFound"=>true,"error"=>"Update type does not specified"));

}else{

    if ($updateType=='DATE' || $updateType=='TIME' || $updateType=='VENUE' || $updateType=='STATUS' || $updateType=='REMARK'){
   
        $updateBooking = new Booking($db);

        // set ID property of record to be edited, 1 PK
        $updateBooking->booking_id = $data->booking_id;
        $updateBooking->booking_no = $data->booking_no;
 
        $updateBooking->readOne();

        if (isset($updateBooking->reg_no)){
            $displayBooking = new Booking($db);
            $displayBooking->booking_no = $data->booking_no;
            $result = $displayBooking->readByBookingNo();

            $old_visit = $result['screening_location']!='HOME'?"Screening Centre" :"Home Visit";
            $old_venue = $result['screening_center']['sc_name'];
            $old_booking_datetime = $result['screening_date'].', '.$result['screening_time'];
            
            if ($updateType=='STATUS') {
                $updateBooking->status = $data->status;
                if ($data->status=="CANCELLED")  $updateBooking->remark =  $data->remark;
            }
            if ($updateType=='DATE') {
                $updateBooking->screening_date = $data->screening_date;
                $new_changes = 
"New Changes : DATE
{$data->screening_date}";
            }
            if ($updateType=='VENUE') {
                $updateBooking->screening_center_id = $data->screening_center_id;
            }
            if ($updateType=='TIME') {
                $updateBooking->screening_time = $data->screening_time;
                $new_changes = 
"New Changes : TIME
{$data->screening_time}";
            }
            if ($updateType=='REMARK') {
                $updateBooking->remark = $data->remark;
            }

            // update the record
            if($updateBooking->update()){
                if ($updateType=='VENUE' || $updateType=='TIME' || $updateType=='DATE'){
                    $displayBooking = new Booking($db);
                    $displayBooking->booking_no = $data->booking_no;
                    $result = $displayBooking->readByBookingNo();

                    
                    if ($updateType=='VENUE') {
                        $updateBooking->screening_center_id = $data->screening_center_id;
                        $new_changes = 
"New Changes : VENUE
Location (".($result['screening_location']!='HOME'?"Screening Centre" :"Home Visit").") : {$result['screening_center']['sc_name']}";
                    }

                    $emailMgmt = new EmailMgmt($db);
                    $emailMgmt->code = "booking-update";
                    $emailMgmt->readOne();
                    //$result['person']['email'] = "arslan@skuire.com";
                    $adminEm = CONST_ADMIN_EMAIL;
                    //$adminEm = 'arslanulhaq319@gmail.com';
                    $emailMgmt->receivers = $adminEm.",".$result['person']['email'];

                    /*$dt = new DateTime($result['date_modified'], new DateTimeZone('UTC'));
                    // change the timezone of the object without changing its time
                    $dt->setTimezone(new DateTimeZone("Asia/Kuala_Lumpur"));
                    // format the datetime
                    $result['date_modified'] = $dt->format('Y-m-d H:i:s');*/

                    $emailMgmt->paramValues =  array( 
                        array("param" => "?booking_no", "value" =>  $result['booking_no']),
                        array("param" => "?username", "value" =>  $result['userAccount']['username']),
                        array("param" => "?name", "value" => $result['person']['name']),
                        array("param" => "?ic_no", "value" => $result['person']['ic_no']),
                        array("param" => "?new_changes", "value" => $new_changes),
                        array("param" => "?venue", "value" => $old_venue),
                        array("param" => "?visit", "value" => $old_visit),
                        
                        array("param" => "?price", "value" => $result['products'][0]['price']),
                        array("param" => "?package", "value" => $result['products'][0]['name']),
                        array("param" => "?change_time", "value" => $result['date_modified']),
                        array("param" => "?booking_datetime", "value" => $old_booking_datetime)
                   );

                   $emailMgmt->sendMail();
                   //print_r($result);
                }
                if ($data->status=="PENDING-CANCELLATION"){ //user request for cancellation
                    $displayBooking = new Booking($db);
                    $displayBooking->booking_no = $data->booking_no;
                    $result = $displayBooking->readByBookingNo();

                    $emailMgmt = new EmailMgmt($db);
                    $emailMgmt->code = "user-cancel-request";
                    $emailMgmt->readOne();
                    $emailMgmt->receivers = CONST_ADMIN_EMAIL;
                    $emailMgmt->paramValues =  array( 
                        array("param" => "?booking_no", "value" =>  $result['booking_no']),
                        array("param" => "?username", "value" =>  $result['userAccount']['username']),
                        array("param" => "?name", "value" => $result['person']['name']),
                        array("param" => "?ic_no", "value" => $result['person']['ic_no'])
                   );

                   foreach ($result['products'] as $product) {
                    array_push($emailMgmt->paramValues,array("param" => "?addon_item", "value" =>  $product['code'].'-'.$product['name']));
                    break;
                   }

                   $emailMgmt->sendMail();
                }

                if ($data->status=="CANCELLED" && $data->remark=='REFUND IS ON-GOING'){ //admin cancel the booking
                    $displayBooking = new Booking($db);
                    $displayBooking->booking_no = $data->booking_no;
                    $result = $displayBooking->readByBookingNo();

                    $emailMgmt = new EmailMgmt($db);
                    $emailMgmt->code = "admin-booking-cancelled";
                    $emailMgmt->readOne();
                    $emailMgmt->receivers = $result['person']['email'];
                    $emailMgmt->paramValues =  array( 
                        array("param" => "?booking_no", "value" =>  $result['booking_no']),
                        array("param" => "?username", "value" =>  $result['userAccount']['username']),
                        array("param" => "?name", "value" => $result['person']['name']),
                        array("param" => "?ic_no", "value" => $result['person']['ic_no']),
                        array("param" => "?date_cancellation", "value" => $result['date_modified']),
                        array("param" => "?booking_datetime", "value" => $result['screening_date'].', '.$result['screening_time'])
                   );

                   if ($result['screening_location']=='HOME'){
                    array_push($emailMgmt->paramValues,array("param" => "?venue", "value" => 'HOME VISIT'));
                   }else{
                    array_push($emailMgmt->paramValues,array("param" => "?venue", "value" =>  $result['screening_center']['sc_name'])); 
                   }

                   if (sizeof($result['products'])>0){
                    foreach ($result['products'] as $product) {
                        array_push($emailMgmt->paramValues,array("param" => "?addon_item", "value" =>  $product['code'].'-'.$product['name']));
                        break;
                    }
                   }else{
                    array_push($emailMgmt->paramValues,array("param" => "?addon_item", "value" =>  'None'));
                   }


                   $total_payment=0;
                   foreach ($result['payments'] as $payment) {
                    $total_payment+=$payment['amount'];
                   }
                   array_push($emailMgmt->paramValues,array("param" => "?total_payment", "value" =>  number_format((float)$total_payment, 2, '.', '')));

                   $emailMgmt->sendMail();

                    //send sms notification
                    $smsNotification = new SmsNotification($db);
                    $smsNotification->code = "admin-booking-cancelled";
                    $smsNotification->readOne();
                    $smsNotification->mobile_no = "60". $result['person']['mobile_no'];
                    $smsNotification->paramValues =  array( 
                        array("param" => "?total_payment", "value" => number_format((float)$total_payment, 2, '.', '')) 
                    );
                    $smsNotification->sendSms();
                }
            
                // set response code - 200 ok
                http_response_code(200);
            
                // tell the user
                echo json_encode(array("message" => "Booking has been successfully updated.","errorFound"=>false,"error"=>"success"));
            }
            
            // if unable to update the record, tell the user
            else{
            
                // set response code - 503 service unavailable
                http_response_code(503);
            
                // tell the user
                echo json_encode(array("message" => "Unable to update the booking.","errorFound"=>true,"error"=>"Service unavailable"));
            }
        }else{
            // set response code - 503 service unavailable
            http_response_code(503);

            // tell the user
            echo json_encode(array("message" => "Unable to update the booking.","errorFound"=>true,"error"=>"Booking no does not exist"));
        }
    }else{
        // set response code - 503 service unavailable
        http_response_code(503);

        // tell the user
        echo json_encode(array("message" => "Unable to update booking.","errorFound"=>true,"error"=>"Update not allowed"));

    }
 
}
  

?>