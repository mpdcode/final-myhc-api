<?php
/**
 * Author: Majina
 * booking.searchByStatusAndRegNo()
 * URL for testing : https://myhc.my/myhc-api/v1/booking/search-by-status-and-reg-no.php?s=<keyword>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// include database and object files
include_once '../../config/core.php';
include_once '../../config/db.php';
include_once '../../objects/v1/booking.php';
 
  
// instantiate database and patient object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$booking = new Booking($db);
 
  
// get keywords
$reg_no=isset($_GET["rn"]) ? $_GET["rn"] : "";
$keyword=isset($_GET["kw"]) ? $_GET["kw"] : "";
$keysearch=isset($_GET["ks"]) ? $_GET["ks"] : "";
  
// query data
$result = $booking->readByStatusAndRegNo($reg_no,$keyword,$keysearch);
 
// set response code - 200 OK
http_response_code(200);

// show data
echo json_encode($result);
 
?>