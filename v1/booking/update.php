<?php
/**
 * Author: Majina
 * Booking.update()
 * URL for testing : https://myhc.com/myhc-api/v1/booking/update.php 
 * JSON input: { "booking_no":"<booking_no>", "reg_no":"<reg_no>", "ic_no":"<ic_no>", 
 * "screening_location":"<screening_location>", "screening_center_id":"<screening_center_id>", "screening_date":"<screening_date>" ,
 * "screening_time":"<screening_time>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/booking.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$booking = new Booking($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited, 1 PK
$booking->booking_id = $data->booking_id;
$booking->booking_no = $data->booking_no;
  
// set data property values
$booking->reg_no = $data->reg_no;
$booking->ic_no = $data->ic_no;
$booking->screening_location = $data->screening_location;
$booking->screening_center_id = $data->screening_center_id;
$booking->screening_date = $data->screening_date;
$booking->screening_time =  $data->screening_time;
$booking->date_modified = date("Y-m-d h:i a");
$booking->status = $data->status;
$booking->remark = $data->remark;

  
// update the record
if($booking->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Booking info was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update booking info.","errorFound"=>true,"error"=>"Service unavailable"));
}
?>