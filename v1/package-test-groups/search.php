<?php
/**
 * Author: Elizha
 * PackageTestGroups.search()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-test-groups/search.php?s=<keyword>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// include database and object files
include_once '../../config/core.php';
include_once '../../config/db.php';
include_once '../../objects/v1/package-test-groups.php';
  
// instantiate database and patient object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$packageTestGroups = new PackageTestGroups($db);
  
// get keywords
$keywords=isset($_GET["s"]) ? $_GET["s"] : "";
  
// query data
$stmt = $packageTestGroups->search($keywords);
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $packageTestGroups_arr=array();
    $packageTestGroups_arr["data"]=array();
  
    $total_records = 0;
    
    // retrieve our table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $packageTestGroups_rec=array(   
            "package_code"  => $package_code,
            "test_group_code"  => $test_group_code ,
            "test_location" => $test_location,
            "total_test_conducted"  => $total_test_conducted,
            "remark" =>$remark
            
        );
  
        array_push($packageTestGroups_arr["data"], $packageTestGroups_rec);
        $total_records++;
    }
  
    $packageTestGroups_arr["total_records"]=$total_records;

    // set response code - 200 OK
    http_response_code(200);
  
    // show data
    echo json_encode($packageTestGroups_arr);
}
  
else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not found
    echo json_encode(
        array("message" => "No Package Test Groups found.","errorFound"=>true,"error"=>"404 Not found")
    );
}
?>