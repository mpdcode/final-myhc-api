<?php
/**
 * Author: Elizha
 * PackageTestGroups.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-test-groups/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/package-test-groups.php';
//include_once '../../objects/v1/test-marker.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$packageTestGroups = new PackageTestGroups($db);
//$testMarker = new TestMarker($db);

// query data
$stmt = $packageTestGroups->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $packageTestGroups_arr=array();
    $packageTestGroups_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $packageTestGroups_item=array(
            "package_code"  => $package_code,
            "test_group_code"  => $test_group_code ,
            "test_location" => $test_location,
            "total_test_conducted"  => $total_test_conducted,
            "remark" =>$remark
              

        );
  
        array_push($packageTestGroups_arr["data"], $packageTestGroups_item);
        $total_records++;
    }

    $packageTestGroups_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($packageTestGroups_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No Package Test Groups found.","error" => "404 Not found")
    );
}
?>