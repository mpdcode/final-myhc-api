<?php
/**
 * Author: Majina
 * RegistrationPerson.verifyIcNoDependent()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/registration-person/verify-ic-no-dependent.php?c=<code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/registration-person.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$registrationPerson = new RegistrationPerson($db);
  
// set ID property of record to read
$registrationPerson->ic_no = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$registrationPerson->hasRegisteredToPackagePlan();
  
if (isset($registrationPerson->person_type_code)){
 
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode(array("message" => "Verification failed. Sorry, dependent has already registered with package plan","errorFound"=>true,"error" => "Found"));
}else{
    // set response code - Not found
    http_response_code(200);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Verification success","errorFound"=>false,"error" => "IC No does not registered with any package plan"));
}
?>