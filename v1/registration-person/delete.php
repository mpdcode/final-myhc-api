<?php
/**
 * Author: Majina
 * RegistrationPerson.delete()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/registration-person/delete.php
 * JSON input: { "reg_no":"<reg_no>", "ic_no":"<ic_no>", "person_type_code": "<person_type_code>" }
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/registration-person.php';
include_once '../../objects/v1/person.php';
include_once '../../objects/v1/person-document.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$registrationPerson = new RegistrationPerson($db);
$personDocument = new PersonDocument($db);

// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted
$registrationPerson->ic_no = $data->ic_no;
$registrationPerson->reg_no = $data->reg_no;
  
$registrationPerson->readOne();
if ($registrationPerson->admin_type!='MAU'){
    // delete the record except MAU //todo
    if ($registrationPerson->delete()){

        //check whether the person has registered to any other package plan
        $registrationPerson->ic_no = $data->ic_no;
        $registrationPerson->reg_no = $data->reg_no;
        $registrationPerson->readOne();

        //if not registered to any plan then delete the person record
        if ($registrationPerson->reg_no==null){
            $person = new Person($db);
            $person->ic_no = $data->ic_no;
            $person->delete();
        }

        //delete all uploaded files for this person
        $personDocument->ic_no = $data->ic_no;
        $personDocument->deleteDocumentsByIcNo();


        // set response code - 200 ok
        http_response_code(200);
    
        // tell the user
        echo json_encode(array("message" => "Person registration has been removed.","error"=>"","errorFound"=>false));
    }
    
    // if unable to delete the record
    else{
    
        // set response code - 503 service unavailable
        http_response_code(503);
    
        // tell the user
        echo json_encode(array("message" => "Unable to remove Person registration.","error"=>"503 service unavailable","errorFound"=>true));
    }
}else{
           // set response code - 503 service unavailable
           http_response_code(503);
    
           // tell the user
           echo json_encode(array("message" => "Main User Account is not allowed to be removed.","error"=>"Not allowed","errorFound"=>true));
    
}

?>