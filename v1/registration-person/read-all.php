<?php
/**
 * Author: Majina
 * RegistrationPerson.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/registration-person/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../config/db.php';
// include_once '../../objects/v1/pre-registration.php';
include_once '../../objects/v1/registration-person.php';
// include_once '../../objects/v1/screening-plan.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$registrationPerson = new registrationPerson($db);

// query data
$stmt = $registrationPerson->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){

    $registrationPerson = new RegistrationPerson($db);
  
    // record array
    $record_arr=array();
    $record_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);

        $record_item = array(
            "reg_no" => $reg_no,
            "ic_no" => $ic_no,
            "person_type_code" => $person_type_code,
            "admin_type" => $admin_type,
            "patient_type_code" => $patient_type_code
            // "registration_persons"=> $registrationPerson->readByRegNo($reg_no),
            // "screening_plan"=> $screeningPlan->readByPackageCode($package_code)
        );
  
        array_push($record_arr["data"], $record_item);
        $total_records++;
    }

    $record_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($record_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No registration person found.","error" => "Not found")
    );
}
?>