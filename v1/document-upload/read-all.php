<?php
/**
 * Author: Majina
 * DocumentUpload.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/document-upload/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/document-upload.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$documentUpload = new DocumentUpload($db);

// query data
$stmt = $documentUpload->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $rec_arr=array();
    $rec_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $rec_item=array(
            "code" => $code,
			"name" => $name,
            "note" => $note,
            "doc_path" => $doc_path,
            "sort_id" => $sort_id, 
            "enabled" => $enabled 
        );
  
        array_push($rec_arr["data"], $rec_item);
        $total_records++;
    }

    $rec_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($rec_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No record has been found.","error" => "Not found")
    );
}
?>