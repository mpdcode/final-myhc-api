<?php
/**
 * Author: Majina
 * ScreeningCenterTimeslot.readAvailableTimeslotsByScreeningCenterId()
 * URL for testing : https://myhc.my/myhc-api/v1/screening-center-timeslot/read-available-timeslots-by-screening-center-id.php?c=<sc_id>
 * JSON input: none
 * Method: GET   
 */

error_reporting(E_ALL);

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/screening-center-timeslot.php';
include_once '../../objects/v1/booking.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$screeningCenterTimeslot = new ScreeningCenterTimeslot($db);
$booking = new Booking($db);
  
// set ID property of record to read
$screeningCenterTimeslot->sc_id = isset($_GET['scid']) ? $_GET['scid'] : die();
$sdate = isset($_GET['sdate']) ? $_GET['sdate'] : die();


$max_count_slot = 10;
  
if (isset($screeningCenterTimeslot->sc_id)){

   
    $sct_arr = $screeningCenterTimeslot->readByScreeningCenterId($screeningCenterTimeslot->sc_id);
    $booking_arr = $booking->readCountTimeslotByScreeningDate($screeningCenterTimeslot->sc_id, $sdate);

    $available_status = true;

    $data=array();
 

    foreach ($sct_arr as $timeslot) {
        
        $available_status = getAvailableStatus($booking_arr,$sdate,$timeslot['ts_code'],$max_count_slot);
        if ( !isset( $data[$sdate] ) ) {
            $data[$sdate]= array(
                "screening_center_id" => $screeningCenterTimeslot->sc_id,
                "screening_date" => $sdate
            );
        }

        $data[$sdate][]= array(
            "screening_time" => $timeslot['ts_code'],
            "time_available_status" => $available_status
        );
        
        }


    $arr_record=array();
    foreach($data as $keyDate => $valueDate) 
    {
        $date_available_status = true;
        $timeslots = getFormatTimeslot($keyDate,$valueDate);
        foreach($timeslots as $keySlot => $valueSlot){
            if ($valueSlot['time_available_status']==false) {
                $date_available_status = false;
                break;
            }
        }
        $record_item = array(
            "screening_center_id"=> $valueDate['screening_center_id'],
            "screening_date"=> $valueDate['screening_date'],
            "timeslots" => $timeslots,
            "date_available_status" => $date_available_status
        );

        array_push($arr_record, $record_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($arr_record);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Screening Center's timeslot info does not exist for " . $screeningCenterTimeslot->sc_id,"error" => "404 Not found"));
}

function getAvailableStatus($arr, $date, $timeslot,$max_count_slot){
    $status = true;
    foreach ($arr as $booking) {
        if ($booking['screening_date']==$date && $booking['screening_time']==$timeslot){
            if ((int)$booking['total_count']>=$max_count_slot){
                $status =  false;
            }
            break;
        }
    }
    return $status;
}

function getFormatTimeslot($keyDate,$valueDate){
    $arr=array();
    foreach($valueDate as $keyTime => $valueTime) 
    {
        if (isset($valueTime['screening_time'])) array_push($arr, $valueTime);
    }
    return $arr;
}
?>