<?php
/**
 * Author: Elizha
 * ScreeningPlan.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/screening-plan/update.php 
 * JSON input: { "package_code":"<package_code>", "single_package": "<single_package>", "category_code":"<category_code>", 
 * "picture_path":"<picture_path>" , "price":"<price>",
 * "license_validity_year":"<license_validity_year>", "test_included":"<test_included>", "note":"<note>" }
 * Method: POST   
 */


// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/screening-plan.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$screeningPlan = new ScreeningPlan($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$screeningPlan->package_code = $data->package_code;


// set data property values
$screeningPlan->package_code = $data->package_code;
$screeningPlan->single_package = $data->single_package;
$screeningPlan->category_code= $data->category_code;
$screeningPlan->description= $data->description;
$screeningPlan->picture_path= $data->picture_path;
$screeningPlan->price = $data->price;
$screeningPlan->license_validity_year=$data->license_validity_year;
$screeningPlan->test_included=$data->test_included;
$screeningPlan->note=$data->note;
$screeningPlan->commercial=$data->commercial;
  
// update the record
if($screeningPlan->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Screening Packages info was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update Screening Packages info.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>