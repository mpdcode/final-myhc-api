<?php
/**
 * Author: Majina
 * MenuItem.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/menu-item/update.php
 * JSON input: { "item_id":"<item_id>", "name":"<name>", "path": "<path>", "page": "<page>", "enabled": "<enabled>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/menu-item.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$menuItem = new MenuItem($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$menuItem->item_id = $data->item_id;
  
// set data property values
$menuItem->name = $data->name;
$menuItem->page = $data->page;
$menuItem->path = $data->path;
$menuItem->enabled = $data->enabled;
  
// update the record
if($menuItem->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Menu item info was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update Menu item info.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>