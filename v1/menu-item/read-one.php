<?php
/**
 * Author: Majina
 * MenuItem.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/menu-item/read-one.php?c=<code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/menu-item.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$menuItem = new MenuItem($db);
  
// set ID property of record to read
$menuItem->item_id = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$menuItem->readOne();
  
if(isset($menuItem->name)){
 
    // create array
    $menuItem_arr = array(
        "item_id" => $menuItem->item_id,
		"name" => $menuItem->name,
        "page" => $menuItem->page,
        "path" => $menuItem->path,
        "enabled" => $menuItem->enabled
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($menuItem_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Menu item info does not exist for " . $_GET['c'] ,"error" => "404 Not found"));
}
?>