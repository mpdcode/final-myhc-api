<?php
/**
 * Author: Majina
 * PackageCategory.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-category/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/package-category.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$packageCategory = new PackageCategory($db);

// query data
$stmt = $packageCategory->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $packageCategory_arr=array();
    $packageCategory_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $packageCategory_item=array(
            "category_code" => $category_code,
			"description" => $description,
            "picture_path" => $picture_path  
        );
  
        array_push($packageCategory_arr["data"], $packageCategory_item);
        $total_records++;
    }

    $packageCategory_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($packageCategory_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No package category has found.","error" => "404 Not found")
    );
}
?>