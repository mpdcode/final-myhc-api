<?php
/**
 * Author: Majina
 * PackageCategory.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-category/create.php
 * JSON input: { "code":"<code>", "description":"<description>", "picture_path": "<picture_path>"}
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/package-category.php';
  
$database = new Database();
$db = $database->getConnection();
  
$packageCategory = new PackageCategory($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->code) 
){
  
    // set data property values
    $packageCategory->code = $data->code;
	$packageCategory->description = $data->description;
    $packageCategory->picture_path = $data->picture_path;
    
  
    // create the record
    if($packageCategory->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Package Category info was created.","errorFound"=>false,"error" => "",));
    }
  
    // if unable to create record, tell the user
    else{
		$packageCategory->code = $data->code;
  		// read the details of patient to be edited
		$packageCategory->readOne();
		if (isset($packageCategory->description)){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Package Category info already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Package Category info.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Package Category info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>