<?php


/**
 * Author: Majina
 * TestGroup.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-group/create.php
 * JSON input: { "test_group_code":null, "package_category":"<package_category>", "group_name": "<group_name>", 
 *              "patient_type":"<patient_type>","price":"<price>","enabled":"<enabled>"}
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {    
    return 0;    
}
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/test-group.php';
  
$database = new Database();
$db = $database->getConnection();
  
$testGroup = new TestGroup($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->test_group_code) &&
    !empty($data->group_name) 
){

    // set data property values
    $testGroup->test_group_code = $data->test_group_code;
    $testGroup->package_category = $data->package_category;
    $testGroup->group_name = $data->group_name;
    $testGroup->patient_type = $data->patient_type;
    $testGroup->price = $data->price;
    $testGroup->enabled = $data->enabled;

    // create the record
    if($testGroup->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Test group info was created.","errorFound"=>false,"error" => "","content"=>$testGroup));
    }
  
    // if unable to create record, tell the user
    else{
		$testGroup->test_group_code = $data->test_group_code;
  		// read the details of record to be edited
		$testGroup->readOne();
		if (isset($testGroup->group_name)){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Test Group info already exist","errorFound"=>true,"error" => "409 record already exist"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Test Group info.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Test Group info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>