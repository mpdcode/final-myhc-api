<?php
/**
 * Author: Majina
 * TestGroup.delete()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-panel/delete.php
 * JSON input: { "test_group_code":null, "test_panel_code":"<test_panel_code>", "group_name": "<group_name>", 
 *              "patient_type":"<patient_type>","price":"<price>","enabled":"<enabled>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/test-group.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$testGroup = new TestGroup($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted
$testGroup->test_group_code = $data->test_group_code;
  
// delete the record
if($testGroup->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Test Group info was deleted.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete Test Group info.","error"=>"503 service unavailable","errorFound"=>true));
}
?>