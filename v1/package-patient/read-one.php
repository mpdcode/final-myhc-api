<?php
/**
 * Author: Elizha
 * PackagePatient.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-patient/read-one.php?c=<package_code>&p=<patient_type_code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/package-patient.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$packagePatient = new PackagePatient($db);
  
// set ID property of record to read
$packagePatient->package_code = isset($_GET['c']) ? $_GET['c'] : die();
$packagePatient->patient_type_code = isset($_GET['p']) ? $_GET['p'] : die();
  
// read the details of data to be edited
$packagePatient->readOne();
  
if (isset($packagePatient->patient_type_code)){
    // create array
    $packagePatient_arr = array(
        "package_code"  => $packagePatient->package_code,
        "patient_type_code"  => $packagePatient->patient_type_code ,
        "total_patient" => $packagePatient->total_patient,
        "doc_required" => $packagePatient->doc_required
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($packagePatient_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Package Patient info does not exist for " . $packagePatient->patient_type_code,"error" => "404 Not found"));
}
?>