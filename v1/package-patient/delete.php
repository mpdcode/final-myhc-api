<?php
/**
 * Author: Elizha
 * PackagePatient.delete()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-patient/delete.php
 * JSON input: { "package_code":"<package_code>", "patient_type_code": "<patient_type_code>", "total_patient":"<total_patient>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/package-patient.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$packagePatient = new PackagePatient($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted, got 2 composite keys
$packagePatient->package_code = $data->package_code;
$packagePatient->patient_type_code = $data->patient_type_code;
  
// delete the record
if($packagePatient->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Package Patient info was deleted.","error"=>"","errorFound"=>false));
}


// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete Package Patient info.","error"=>"503 service unavailable","errorFound"=>true));
}
?>