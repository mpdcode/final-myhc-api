<?php
/**
 * Author: Elizha
 * PackagePatient.search()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-patient/search.php?s=<keyword>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// include database and object files
include_once '../../config/core.php';
include_once '../../config/db.php';
include_once '../../objects/v1/package-patient.php';
  
// instantiate database and patient object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$packagePatient = new PackagePatient($db);
  
// get keywords
$keywords=isset($_GET["s"]) ? $_GET["s"] : "";
  
// query data
$stmt = $packagePatient->search($keywords);
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $packagePatient_arr=array();
    $packagePatient_arr["data"]=array();
  
    $total_records = 0;
    
    // retrieve our table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $packagePatient_rec=array(   
            "package_code"  => $package_code,
            "patient_type_code"  => $patient_type_code ,
            "total_patient" => $total_patient,
            "doc_required" => $doc_required
        );
  
        array_push($packagePatient_arr["data"], $packagePatient_rec);
        $total_records++;
    }
  
    $packagePatient_arr["total_records"]=$total_records;

    // set response code - 200 OK
    http_response_code(200);
  
    // show data
    echo json_encode($packagePatient_arr);
}
  
else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not found
    echo json_encode(
        array("message" => "No Package Patient found.","errorFound"=>true,"error"=>"404 Not found")
    );
}
?>