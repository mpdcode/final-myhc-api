<?php
/**
 * Author: Majina
 * TestReferenceRange.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-reference-range/create.php
 * JSON input: { "test_marker_code":<test_marker_code>, "code":"<code>", "min": "<min>", "max":"<max>","summary":"<summary>"}
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/test-reference-range.php';
  
$database = new Database();
$db = $database->getConnection();
  
$testReferenceRange = new TestReferenceRange($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->code) &&
    !empty($data->test_marker_code) 
){
  
    // set data property values
	$testReferenceRange->test_marker_code = $data->test_marker_code;
    $testReferenceRange->code = $data->code;
	$testReferenceRange->summary = $data->summary;
    $testReferenceRange->min = $data->min;
    $testReferenceRange->max = $data->max;
  
    // create the record
    if ($testReferenceRange->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Test reference range info was created.","errorFound"=>false,"error" => ""));
    }
  
    // if unable to create record, tell the user
    else{
		// $testReferenceRange->code = $data->code;
  		// read the details of record to be edited
		// $testReferenceRange->readOne();
		if (isset($testReferenceRange->code)){
			// record already exist and let the record to be updated
            $testReferenceRange->update();
            http_response_code(200);
  
			// tell the user
			echo json_encode(array("message" => "Test reference range info was updated.","errorFound"=>false,"error" => ""));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Test reference range.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Test reference range. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>