<?php
/**
 * Author: Majina
 * SmsNotification.create()
 * URL for testing : https://myhc.my/myhc-api/v1/payment/create.php
 * JSON input: { "payment_id":"<payment_id>", "payment_date":"<payment_date>", "booking_no":"<booking_no>",
 * "ref_no":"<ref_no>", "amount":"<amount>",  "status":"<status>"
 * "remark":"<remark>" }
 * Method: POST   
 */


     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/payment.php';
  
$database = new Database();
$db = $database->getConnection();
  
$payment = new Payment($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->amount && !empty($data->ref_no ) 
){
  

    // set data property values
    $payment->trans_no = $data->trans_no;
    $payment->payment_id = $data->payment_id;
	$payment->payment_date = $data->payment_date;
    $payment->booking_no = $data->booking_no;
    $payment->ref_no = $data->ref_no;
    $payment->amount = $data->amount;
 
    $payment->status = $data->status;
    $payment->remark = $data->remark;
 
  
    // create the record
    if($payment->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Payment info has been created successfully.","errorFound"=>false,"error" => ""));
    }
  
    // if unable to create record, tell the user
    else{
		$payment->payment_id = $data->payment_id;
  		// read the details of patient to be edited
		$payment->readOne();
		if (isset($payment->payment_desc)){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Payment info already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Payment info.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Payment info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>