<?php
/**
 * Author: Elizha
 * Payment.search()
 * URL for testing : https://myhc.my/myhc-api/v1/payment/search.php?s=<keyword>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// include database and object files
include_once '../../config/core.php';
include_once '../../config/db.php';
include_once '../../objects/v1/payment.php';
  
// instantiate database and patient object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$payment = new Payment($db);
  
// get keywords
$keywords=isset($_GET["s"]) ? $_GET["s"] : "";
  
// query data
$stmt = $payment->search($keywords);
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $payment_arr=array();
    $payment_arr["data"]=array();
  
    $total_records = 0;
    
    // retrieve our table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
        
        $payment_rec=array(   
            "trans_no"  => $trans_no,
            "payment_id"  => $payment_id,
            "payment_date"  => $payment_date,
            "booking_no"  => $booking_no,
            "ref_no"  => $ref_no,
            "amount"  => $amount,
            "status"  => $status,
            "remark"  => $remark
 
        );
  
        array_push($payment_arr["data"], $payment_rec);
        $total_records++;
    }
  
    $payment_arr["total_records"]=$total_records;

    // set response code - 200 OK
    http_response_code(200);
  
    // show data
    echo json_encode($payment_arr);
}
  
else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not found
    echo json_encode(
        array("message" => "No Payment record found.","errorFound"=>true,"error"=>"404 Not found")
    );
}
?>