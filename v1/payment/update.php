<?php
/**
 * Author: Elizha
 * Payment.update()
 * URL for testing : https://myhc.my/myhc-api/v1/payment/update.php 
 * JSON input: { "payment_id":"<payment_id>", "payment_date":"<payment_date>", "booking_no":"<booking_no>",
 * "ref_no":"<ref_no>", "amount":"<amount>", "status":"<status>"
 * "remark":"<remark>" }
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/payment.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$payment = new Payment($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$payment->trans_no = $data->trans_no;


// set data property values
$payment->payment_date = $data->payment_date;
$payment->payment_id = $data->payment_id;
$payment->booking_no = $data->booking_no;
$payment->ref_no = $data->ref_no;
$payment->amount = $data->amount;
$payment->status = $data->status;
$payment->remark = $data->remark;
  
// update the record
if($payment->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Payment info has been updated successfully.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update payment info.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>