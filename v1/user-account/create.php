<?php
/**
 * Author: Majina
 * UserAccount.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/user-account/create.php
 * JSON input: { "username":"<username>", "password":"<password>", "ic_no": "<ic_no>" 
 * 			"acc_type_code":"<acc_type_code>","reg_no":"<reg_no>", "acc_status_code":"<acc_status_code>",
 * 			"date_created":"<date_created>", "date_updated":"<date_updated>"			
 * }
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/user-account.php';
include_once '../../objects/v1/email-mgmt.php';
include_once '../../objects/v1/person.php';
  
$database = new Database();
$db = $database->getConnection();
  
$userAccount = new UserAccount($db);
$emailMgmt = new EmailMgmt($db);
$person = new Person($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->ic_no) &&
	!empty($data->acc_type_code) &&
	!empty($data->acc_status_code) &&
	!empty($data->username) 
){
  
    // set data property values
    $userAccount->username = $data->username;
	$userAccount->ic_no = $data->ic_no;
	$userAccount->acc_type_code = $data->acc_type_code;
	$userAccount->reg_no = $data->reg_no;
	$userAccount->acc_status_code = $data->acc_status_code;
	$userAccount->menu_owner = $data->menu_owner;
	$userAccount->date_created = $data->date_created;
	$userAccount->date_updated = $data->date_updated;
  
    // create the record
    if ($userAccount->create()){

		$emailMgmt->code = "pre-account-created";
		if ($emailMgmt->readOne()){
			$person->ic_no =  $data->ic_no;
			$person->readOne();
			mail($person->email,$emailMgmt->title,$emailMgmt->content,"From: ".$emailMgmt->sender);
			// set response code - 201 created
			http_response_code(201);

			// tell the user
			echo json_encode(array("message" => "New user account has created.","errorFound"=>false,"error" => "",));
		
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Account has been created but failed to send email to user.","errorFound"=>true,"error" => "503 service unavailable"));

		}
  
    }
  
    // if unable to create record, tell the user
    else{
		$userAccount->username = $data->username;
  		// read the details of patient to be edited
		$userAccount->readOne();
		if(isset($userAccount->ic_no)){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "User account already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create new user account.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create new user account. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>