<?php
/**
 * Author: Majina
 * UserAccount.reset-password()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/user-account/reset-password.php
 * JSON input: { "username":"<username>","ic_no":"<ic_no>" }"			
 * }
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/user-account.php';
include_once '../../objects/v1/person.php';
// include_once '../../objects/v1/sms-notification.php';
// include_once '../../objects/v1/email-mgmt.php';
  
$database = new Database();
$db = $database->getConnection();
  
$userAccount = new UserAccount($db);
$person = new Person($db);
// $smsNotification = new SmsNotification($db);
// $emailMgmt = new EmailMgmt($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->username) &&
	!empty($data->cpass) &&
	!empty($data->npass) 
){
  
    // set data property values
	$userAccount->username = $data->username;

	$userAccount->readOne();
	
	if (isset($userAccount->username)){

		if ($userAccount->isPasswordVerified($data->cpass,$data->username)){
			$userAccount->password = $userAccount->encryptPassword($data->npass);
			// create the record
			if ($userAccount->changePassword()){
				// set response code - 200 Ok
				http_response_code(200);
	
				// tell the user
				echo json_encode(array("message" => "Your password has been updated.","errorFound"=>false,"error" => "",));
		
			}else{
				// set response code - 400 bad request
				http_response_code(400);
			
				// tell the user
				echo json_encode(array("message" => "Unable to reset password due to technical error","errorFound"=>true,"error" => "400 bad request"));
			}
		}else{
			// set response code - 400 bad request
			http_response_code(400);

			// tell the user
			echo json_encode(array("message" => "Your have entered invalid current password","errorFound"=>true,"error" => "400 bad request"));

		}


	}else{
		// set response code - 503 service unavailable
		http_response_code(503);

		// tell the user
		echo json_encode(array("message" => "User account does not exist.","errorFound"=>true,"error" => "Service unavailable"));

	}
  
 
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create change your password. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>