<?php
/**
 * Author: Majina
 * MessageBox.readAllAdminPerson()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/user-account/read-all-admin.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/user-account.php';
include_once '../../objects/v1/person.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$userAccount = new UserAccount($db);

// query data
$stmt = $userAccount->readAllAdminPerson();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $rec_arr=array();
    $rec_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);

        $person = new Person($db);
  
         
        $rec_item=array(
            "username"  => $username,
            "ic_no"  => $ic_no ,
            "acc_type_code" => $acc_type_code,
            "acc_status_code"  => $acc_status_code ,
            "menu_owner" => $menu_owner,
            "reg_no"  => $reg_no,
            "date_created" => $date_created,
            "date_updated" => $date_updated,
            "name" =>$name,
            "email" =>$email,
            "person" => $person->readByIcNo($ic_no)
        );
  
        array_push($rec_arr["data"], $rec_item);
        $total_records++;
    }

    $rec_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($rec_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No MAU found.","error" => "404 Not found")
    );
}
?>