<?php
/**
 * OBSELETE - Not in used anymore
 * 
 * Author: Majina
 * UserAccount.verifyPassword()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/user-account/verify-password.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/user-account.php';
include_once '../../objects/v1/person.php';
include_once '../../objects/v1/pre-registration.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$userAccount = new UserAccount($db);
$person = new Person($db);
$preRegistration = new PreRegistration($db);
  
// set ID property of record to read
$data = json_decode(file_get_contents("php://input"));

$userAccount->username = $data->username;
$userAccount->password = $data->password;
  
// read the details of data to be edited
$userAccount->verifyPassword();
  
if (isset($userAccount->username)){
 
    // create array
    $userAccount_arr = array(
        "username" => $userAccount->username,
		"password" => $userAccount->password,
        "ic_no" => $userAccount->ic_no,
        "acc_type_code" => $userAccount->acc_type_code,
        "reg_no" => $userAccount->reg_no,
        "acc_status_code" => $userAccount->acc_status_code,
        "menu_owner" => $userAccount->menu_owner,
        "date_created" => $userAccount->date_created,
        "date_updated" => $userAccount->date_updated,
        "last_login" => $userAccount->last_login,
        "person"=> $person->readByIcNo($userAccount->ic_no),
        "registration"=> $preRegistration->readByRegNo($userAccount->reg_no)
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($userAccount_arr);
}else{
    // set response code - 404 Not found
    http_response_code(200);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Invalid username or password","error" => "Invalid"));
}
?>