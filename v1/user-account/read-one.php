<?php
/**
 * Author: Majina
 * UserAccount.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/user-account/read-one.php?c=<code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/user-account.php';
include_once '../../objects/v1/person.php';
include_once '../../objects/v1/pre-registration.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$userAccount = new UserAccount($db);
$person = new Person($db);
$preRegistration = new PreRegistration($db);
  
// set ID property of record to read
$userAccount->username = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$userAccount->readOne();
  
if(isset($userAccount->ic_no)){
 
    // create array
    $userAccount_arr = array(
        "username" => $userAccount->username,
        "ic_no" => $userAccount->ic_no,
        "acc_type_code" => $userAccount->acc_type_code,
        // "reg_no" => $userAccount->reg_no,
        "acc_status_code" => $userAccount->acc_status_code,
        "date_of_birth" => $userAccount->date_of_birth,
        "date_created" => $userAccount->date_created,
        "last_login" => $userAccount->last_login,
        "date_updated" => $userAccount->date_updated,
        "person"=> $person->readByIcNo($userAccount->ic_no),
        "registration"=> $preRegistration->readByIcNo($userAccount->ic_no)
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($userAccount_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "User account does not exist for " . $_GET['c'],"error" => "404 Not found"));
}
?>