<?php
/**
 * Author: Majina
 * UserAccount.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/user-account/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/package-category.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$userAccount = new UserAccount($db);

// query data
$stmt = $userAccount->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $userAccount_arr=array();
    $userAccount_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $userAccount_item=array(
            "username" => $username,
			"ic_no" => $ic_no,
            "acc_type_code" => $acc_type_code, 
            "reg_no" => $reg_no, 
            "acc_status_code" => $acc_status_code, 
            "menu_owner" => $menu_owner,  
            "date_created" => $date_created, 
            "date_updated" => $date_updated,   
            "last_login" => $last_login
        );
  
        array_push($userAccount_arr["data"], $userAccount_item);
        $total_records++;
    }

    $userAccount_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($userAccount_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No user account has found.","error" => "404 Not found")
    );
}
?>