<?php
/**
 * Author: Majina
 * NcdProfile.readOne()
 * URL for testing : https://myhc.my/myhc-api/v1/ncd-profile/read-one.php?ic=<ic_no>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ALL);

// include database and object files
include_once '../../config/db.php';

include_once '../../objects/v1/data-sharing.php';

// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$ncdProfile = new DataSharing($db);
  
// set ID property of record to read
$ncdProfile->ic_no = isset($_GET['ic']) ? $_GET['ic'] : die();
  
// read the details of data to be edited
$ncdProfile->readOne();
  
if (isset($ncdProfile->ic_no)){
 
    // create array
    $arr = array(
        "ic_no" => $ncdProfile->ic_no,
		"bmz_updates" => $ncdProfile->bmz_updates,
        "partner_updates" => $ncdProfile->partner_updates,
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "does not exist for " . $ncdProfile->ic_no,"error" => "404 Not found"));
}
?>