<?php
/**
 * Author: Majina
 * SmsNotification.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/sms-notification/update.php
 * JSON input: { "code":"<code>", "message":"<message>", "params":"<params>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/sms-notification.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$smsNotification = new smsNotification($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$smsNotification->code = $data->code;
  
// set data property values
$smsNotification->name = $data->name;
$smsNotification->message = $data->message;
$smsNotification->params = $data->params;
  
// update the record
if($smsNotification->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "SMS Notification info was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update SMS Notification info.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>