<?php
/**
 * Author: Elizha
 * PackageAddOns.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-add-ons/read-one.php?c=<[package_code]>&p=<add_on_code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/package-add-ons.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$packageAddOns = new PackageAddOns($db);
  
// set ID property of record to read
$packageAddOns->package_code = isset($_GET['c']) ? $_GET['c'] : die();
$packageAddOns->add_on_code = isset($_GET['p']) ? $_GET['p'] : die();
  
// read the details of data to be edited
$packageAddOns->readOne();
  
if($packageAddOns->add_on_code!=null){
    // create array
    $packageAddOns_arr = array(

        "package_code"  => $packageAddOns->package_code,
        "add_on_code"  => $packageAddOns->add_on_code ,
        "add_on_name" => $packageAddOns->add_on_name,
        "test_location_code"  => $packageAddOns->test_location_code,
        "test_location_name" => $packageAddOns->test_location_name,
        "total_test_conducted" =>$packageAddOns->total_test_conducted,
        "patient_type_code" =>$packageAddOns->patient_type_code


    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($packageAddOns_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Package Add Ons info does not exist for " . $packageAddOns->add_on_code,"error" => "404 Not found"));
}
?>