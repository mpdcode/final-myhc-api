<?php
/**
 * Author: Elizha
 * PackageAddOns.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-add-ons/update.php 
 * JSON input: { "package_code":"<package_code>", "add_on_code": "<add_on_code>", "add_on_name":"<add_on_name>", 
 * "test_location_code":"<test_location_code>", "test_location_name":"<test_location_name>", 
 * "total_test_conducted":"<total_test_conducted>", "patient_type_code":"<patient_type_code>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/package-add-ons.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$packageAddOns = new PackageAddOns($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited, got 2 PK, composite keys
$packageAddOns->package_code = $data->package_code;
$packageAddOns->add_on_code = $data->add_on_code;

  
// set data property values

$packageAddOns->package_code = $data->package_code;
$packageAddOns->add_on_code = $data->add_on_code;
$packageAddOns->add_on_name = $data->add_on_name;
$packageAddOns->test_location_code = $data->test_location_code;
$packageAddOns->test_location_name = $data->test_location_name;
$packageAddOns->total_test_conducted = $data->total_test_conducted;
$packageAddOns->patient_type_code = $data->patient_type_code;

  
// update the record
if($packageAddOns->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Package Add Ons info was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update Package Add Ons info.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>