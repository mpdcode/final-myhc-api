<?php
/**
 * Author: Majina
 * TestMarker.readByPanelCode()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-marker/read-by-panel-code.php?c=<code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/test-marker.php';
include_once '../../objects/v1/test-reference-range.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$testMarker = new TestMarker($db);
$testRefRange = new TestReferenceRange($db);
  
// set ID property of record to read
$testMarker->test_panel_code = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$testMarker_arr = $testMarker->readByPanelCode($testMarker->test_panel_code);
  
if (isset($testMarker->test_panel_code)){
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($testMarker_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Test Marker info does not exist for " . $testMarker->code,"error" => "404 Not found"));
}
?>