<?php
/**
 * Author: Elizha
 * MessageBox.search()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/message-box/search.php?s=<keyword>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// include database and object files
include_once '../../config/core.php';
include_once '../../config/db.php';
include_once '../../objects/v1/message-box.php';
  
// instantiate database and patient object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$messageBox = new MessageBox($db);
  
// get keywords
$keywords=isset($_GET["s"]) ? $_GET["s"] : "";
  
// query data
$stmt = $messageBox->search($keywords);
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $messageBox_arr=array();
    $messageBox_arr["data"]=array();
  
    $total_records = 0;
    
    // retrieve our table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
        
        $messageBox_rec=array(   
            "message_id"  => $message_id,
            "sender"  => $sender ,
            "receiver" => $receiver,
            "subject"  => $subject,
            "message" => $message;
            "headers" => $headers,
            "date_sent" =>$date_sent,
            "message_type_code" =>$message_type_code,
            "status"=>$status,
            "attachment"=>$attachment
        );
  
        array_push($messageBox_arr["data"], $messageBox_rec);
        $total_records++;
    }
  
    $messageBox_arr["total_records"]=$total_records;

    // set response code - 200 OK
    http_response_code(200);
  
    // show data
    echo json_encode($messageBox_arr);
}
  
else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not found
    echo json_encode(
        array("message" => "No Message found.","errorFound"=>true,"error"=>"404 Not found")
    );
}
?>