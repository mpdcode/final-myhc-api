<?php
/**
 * Author: Elizha
 * MessageBox.readByUsername()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/message-box/read-all-messages.php?u=<username>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/message-box.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$messageBox = new MessageBox($db);
$submessageBox = new MessageBox($db);

// set ID property of record to read
$username = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$arr = $messageBox->readAllMessages($username);
  
if (sizeOf($arr)>0){
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($arr);
}else{
    // set response code - 404 Not found
    http_response_code(200);
  
    // tell the user that record does not exist
    echo json_encode(array());
}
?>