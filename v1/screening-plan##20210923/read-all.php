<?php
/**
 * Author: Elizha
 * ScreeningPlan.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/screening-plan/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/screening-plan.php';
include_once '../../objects/v1/package-patient.php';
//include_once '../../objects/v1/package-test-panels.php';
include_once '../../objects/v1/package-test-groups.php';
include_once '../../objects/v1/package-add-ons.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$screeningPlan = new ScreeningPlan($db);
$packagePatient = new PackagePatient($db);
$packageTestGroups = new PackageTestGroups($db);
//$packageTestPanels = new PackageTestPanels($db);
$packageAddOns = new PackageAddOns($db);


// query data
$stmt = $screeningPlan->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $screeningPlan_arr=array();
    $screeningPlan_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
 

        $screeningPlan_item=array(
            "package_code"  => $package_code,
            "single_package"  => $single_package ,
            "category_code" => $category_code,
            "picture_path" => $picture_path,
            "price"  => $price,
            "license_validity_year" => $license_validity_year,
            "test_included" =>$test_included,
            "application_license" =>$application_license,
           //output json, this is for case 1 to M
            "package_patients" => $packagePatient->readByPackageCode($package_code),
            //"package_test_panels"=> $packageTestPanels->readByPackageCode($package_code),
            "package_test_groups"=> $packageTestGroups->readByPackageCode($package_code),
            "package_add_ons"=> $packageAddOns->readByPackageCode($package_code)
            
        );
  
        array_push($screeningPlan_arr["data"], $screeningPlan_item);
        $total_records++;
    }

    $screeningPlan_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($screeningPlan_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No Screening Package found.","error" => "404 Not found")
    );
}
?>