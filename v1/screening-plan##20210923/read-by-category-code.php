<?php
/**
 * Author: Majina
 * ScreeningPlan.readByCategoryCode()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/screening-plan/read-by-category-code.php?c=<code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/screening-plan.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$screeningPlan = new ScreeningPlan($db);
  
// set ID property of record to read
$screeningPlan->category_code = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$screeningPlan_arr = $screeningPlan->readByCategoryCode($screeningPlan->category_code);

http_response_code(200);

echo json_encode($screeningPlan_arr);
  
?>