<?php
/**
 * Author: Elizha
 * ScreeningPlan.search()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/screening-plan/search.php?s=<keyword>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// include database and object files
include_once '../../config/core.php';
include_once '../../config/db.php';
include_once '../../objects/v1/screening-plan.php';
  
// instantiate database and patient object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$screeningPlan = new ScreeningPlan($db);
  
// get keywords
$keywords=isset($_GET["s"]) ? $_GET["s"] : "";
  
// query data
$stmt = $screeningPlan->search($keywords);
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $screeningPlan_arr=array();
    $screeningPlan_arr["data"]=array();
  
    $total_records = 0;
    
    // retrieve our table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);


        $screeningPlan_rec=array(   
            "package_code"  => $package_code,
            "single_package"  => $single_package ,
            "category_code" => $category_code,
            "picture_path" => $picture_path,
            "price"  => $price,
            "license_validity_year" => $license_validity_year,
            "test_included" =>$test_included,
            "application_license" =>$application_license
           
        );
  
        array_push($screeningPlan_arr["data"], $screeningPlan_rec);
        $total_records++;
    }
  
    $screeningPlan_arr["total_records"]=$total_records;

    // set response code - 200 OK
    http_response_code(200);
  
    // show data
    echo json_encode($screeningPlan_arr);
}
  
else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not found
    echo json_encode(
        array("message" => "No Screening Package found.","errorFound"=>true,"error"=>"404 Not found")
    );
}
?>