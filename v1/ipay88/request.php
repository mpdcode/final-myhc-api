<?php
    ini_set('display_errors', 1); 
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    date_default_timezone_set('Asia/Kuala_Lumpur');
    //echo date('Y-m-d H:i:s');

    // get database connection
    include_once '../../config/db.php';
    include_once '../../objects/v1/ipay88-log.php';
    include_once '../../objects/v1/constant.php';
    include_once 'service.php';

    $database = new Database();
    $db = $database->getConnection();

    $log = new Ipay88Log($db);
  
    // get posted data
    // $data = file_get_contents("php://input");


    if (!empty($_POST['Amount'] && !empty($_POST['RefNo']))){
 
        
        $paymentGatewayURL = CONST_IPAY88_PAYMENT_GATEWAY_URL;
        $merchant_key = CONST_IPAY88_MERCHANT_KEY;
        $merchant_code = CONST_IPAY88_MERCHANT_CODE;
        $ref_no = $_POST['RefNo'];
        $amount = $_POST['Amount'];
        $prodDesc = $_POST['ProdDesc'];
        $userName = $_POST['UserName'];
        $userEmail = $_POST['UserEmail'];
        $userContact = $_POST['UserContact'];
        $remark = $_POST['Remark'];
        $currency= "MYR";
        $responseUrl = CONST_IPAY88_RESPONSE_URL;
        $backendUrl = CONST_IPAY88_BACKEND_URL;

        $log->log_id = 0;
        $log->trans_id = 0;
        $log->trans_date =  date('Y-m-d H:i:s');
        $log->trans_status = "New submission";
        $log->payment_id = 0;
        $log->ref_no = $ref_no;
        $log->amount = $amount;
        $log->params = 'MerchantCode = ' . $merchant_code .
            ', PaymentId = ' .  '' .
            ', RefNo = ' .  $ref_no .
            ', Amount = ' .  $amount .
            ', Currency = ' .  $currency.
            ', ProdDesc = ' . $prodDesc .
            ', UserName = ' .  $userName .
            ', UserEmail = ' . $userEmail .
            ', UserContact = ' .  $userContact  .
            ' Remark = ' .  $remark;

        if ($log->create()){

            $sha256 = iPay88_signature($merchant_key.$merchant_code.$ref_no.str_replace(".", "", $amount).$currency);

        
            $fieldValues = array(
                'MerchantCode' => $merchant_code,
                'PaymentId' => '',
                'RefNo' => $ref_no,
                'Amount' => $amount,
                'Currency' => $currency,
                'ProdDesc' => $prodDesc,
                'UserName' => $userName,
                'UserEmail' => $userEmail,
                'UserContact'  => $userContact  ,
                'Remark'=>  $remark,
                'Lang'=> 'UTF-8',
                'SignatureType' => 'SHA256',
                'Signature' => $sha256,
                'ResponseURL' => $responseUrl,
                'BackendURL' => $backendUrl 
            );    
    
            echo "Please wait while redirecting to Payment Gateway...";
            echo "<form id='autosubmit' name='ePayment' action='".$paymentGatewayURL."' method='post'>";
                if (is_array($fieldValues) || is_object($fieldValues))
                {
                    foreach ($fieldValues as $key => $val) {
                        echo  "<input type='hidden' name='".ucfirst($key)."' value='".htmlspecialchars($val)."'><br>";
                    }
                }
                echo "</form>";
            echo "
            <script type='text/javascript'>
                function submitForm() {
                    document.getElementById('autosubmit').submit();
                }
                window.onload = submitForm;
            </script>
            ";
      
        }else{
            echo "Sorry, error in creating trans logs.";
        }
        

    }else{
        echo "Sorry, could not connect to Payment Gateway.";
        //redirect("https://myhc.live/myhc/error/fail-payment");
    }
 

?>

