<?PHP
    ini_set('display_errors', 1); 
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    // get database connection
    include_once '../../config/db.php';
    include_once '../../objects/v1/ipay88-log.php';
    include_once '../../objects/v1/payment.php';
    include_once '../../objects/v1/sms-notification.php';
    include_once '../../objects/v1/email-mgmt.php';
    include_once '../../objects/v1/booking.php';
    include_once '../../objects/v1/booking.php';
    include_once '../../objects/v1/trans-log.php';
    include_once 'service.php';

    $database = new Database();
    $db = $database->getConnection();

    $log = new Ipay88Log($db);
    $payment = new Payment($db);
    $transLog = new TransLog($db);
    

    $s_country = "-";
    $s_bankname = "-";
    $ccno = "-";

    $merchantcode = $_REQUEST["MerchantCode"];
    $paymentid = $_REQUEST["PaymentId"];
    $refno = $_REQUEST["RefNo"];
    $amount = $_REQUEST["Amount"];
    $ecurrency = $_REQUEST["Currency"];
    $remark = $_REQUEST["Remark"];
    $transid = $_REQUEST["TransId"];
    $authcode = $_REQUEST["AuthCode"];
    $estatus = $_REQUEST["Status"];
    $errdesc = $_REQUEST["ErrDesc"];
    $signature = $_REQUEST["Signature"];
    $ccname = $_REQUEST["CCName"];
    if (isset($_REQUEST["CCNo"])) $ccno = $_REQUEST["CCNo"];
    if (isset($_REQUEST["S_bankname"])) $s_bankname = $_REQUEST["S_bankname"];
    if (isset($_REQUEST["S_country"])) $s_country = $_REQUEST["S_country"];

    $arr_refno = explode("#", $refno);
    $booking_no =  $arr_refno[0];

    if ($estatus==1) {
    // COMPARE Return Signature with Generated Response Signature
    // update order to PAID
 
            echo "Thank you for payment. <br>".
            // "merchantcode-".$merchantcode ."<br>".
            "Payment ID -".$paymentid ."<br>".
            "Reference No -". $refno ."<br>".
            "Amount (MYR) -". $amount ."<br>".
            "Currency -". $ecurrency ."<br>".
            "Remark -". $remark ."<br>".
            "Transaction ID-". $transid ."<br>".
            // "authcode-". $authcode ."<br>".
            // "estatus-". $estatus ."<br>".
            "Error - ". $errdesc ."<br>".
            // "signature-". $signature ."<br>".
            "ccname-". $ccname ."<br>".
            // "ccno-". $ccno ."<br>".
            "s_bankname-". $s_bankname ."<br>".
            "s_country-". $s_country ."<br>";
            
            if (isset($paymentid)){
                
                $payment->trans_no = $transid;
                $payment->payment_id = $paymentid;
                $payment->payment_date = date('Y-m-d H:i:s');
                $payment->booking_no =  $booking_no;
                $payment->ref_no = $refno;
                $payment->amount = $amount;
                $payment->status = $transid . " - PAID";
                $payment->remark =  "ccname = ". $ccname .
                ", ccno = ". $ccno . 
                ", s_bankname = ". $s_bankname  .
                ", s_country = ". $s_country;
                $payment->create();

                /**------ START:SENDING EMAIL ------*/
                // prepare object
                $emailMgmt = new EmailMgmt($db);
                $emailMgmt->code = "ipay88-thank-you-payment";
                $emailMgmt->readOne();
 
                $booking = new Booking($db);
                $booking_arr = $booking->readByRefNo($refno);

                $receipt_path = "../../pdf/";

                require_once $receipt_path.'receipt.php'; 
                include($receipt_path.'pdf.php');
                $file_name = date('Ymd')."_". $booking_no . '.pdf';
                $html_code = '<link rel="stylesheet" href="'.$receipt_path.'bootstrap/bootstrap.min.css">';
                $html_code .= fetch_data($booking_arr,$receipt_path);
                $pdf = new Pdf();
                $pdf->load_html($html_code);
                $pdf->render();
                $file = $pdf->output();
                file_put_contents($file_name, $file);
                
            
                require $receipt_path.'class/class.phpmailer.php'; // filepath to the PHPMailer class
                require $receipt_path.'class/class.smtp.php';
            
                $mail = new PHPMailer();
            
            //  $mail->SMTPDebug = 2;
                $mail->Mailer = "smtp";
                $mail->Host = "localhost";
                $mail->Port = 25;
                $mail->SMTPAuth = true; // turn on SMTP authentication
                $mail->Username = CONST_EMAIL_ID; // SMTP username
                $mail->Password = CONST_EMAIL_PASSWORD; // SMTP password 
                $mail->Priority = 1;
                $mail->AddAddress($booking_arr['person']['email'],"Name");

                if (isset($emailMgmt->sender)){
                    $emailMgmt->paramValues =  array( 
                        array("param" => "?name", "value" => $booking_arr['person']['name']),
                        array("param" => "?total_payment", "value" => $amount),
                        array("param" => "?date_of_payment", "value" => $payment->payment_date)
                    );
                    $mail->SetFrom($emailMgmt->sender, "Info");
                    $mail->AddReplyTo($emailMgmt->sender, "Info");
                    $mail->Subject  = $emailMgmt->title;
                    $mail->Body     = $emailMgmt->readMailContent();
                }

                $mail->WordWrap = 50;  
                $mail->IsHTML(true);       //Sets message type to HTML    
                $mail->AddAttachment($file_name);         //Adds an attachment from a path on the filesystem
            
                $transLog->activity = 'ref_no='.$refno.', email='.$booking_arr['person']['email']
                .', name='.$booking_arr['person']['name'].', content=response payment status ipay88 ';
                $transLog->trans_date=date('Y-m-d H:i:s');
                $transLog->username=$booking_arr['person']['ic_no'];


                if(!$mail->Send()) {
                    $transLog->status='Email not sent - '.$mail->ErrorInfo;
                    $transLog->create();
                    echo 'Message was not sent.';
                    echo 'Mailer error: ' . $mail->ErrorInfo;
                } else {
                    $transLog->status='Email sent';
                    $transLog->create();
                    echo 'Message has been sent.';
                }
            
                unlink($file_name);
                /**------ END:SENDING EMAIL ------*/

                /**------ START:SENDING SMS ------*/
                $smsNotification = new SmsNotification($db);
                $smsNotification->code = "ipay88-thank-you-payment";
                $smsNotification->readOne();
                $smsNotification->mobile_no = "60". $booking_arr['person']['mobile_no'];
                $smsNotification->paramValues =  array( 
                     array("param" => "?total_payment", "value" =>  $amount)
                );
                $smsNotification->sendSms();
                /**------ END:SENDING SMS ------*/

    
            }


        
    } else {
        // update order to FAIL
        $errdesc = $errdesc . " - Payment failed.";
    }
 
    $tstatus = "";
    if (isset($errdesc)) $tstatus = 'ERROR ' . $errdesc;
    if (isset($paymentid)) $tstatus = $paymentid. ' PAID';

    $log->log_id = 0;
    $log->trans_id = $transid;
    $log->trans_date =  date('Y-m-d H:i:s');
    $log->trans_status = $tstatus . ' ' . $errdesc . ' - response from iPay88';
    $log->payment_id = $paymentid;
    $log->ref_no = $refno;
    $log->amount = $amount;
    $log->params = "merchantcode = ".$merchantcode . 
        ", paymentid = ".$paymentid . 
        ", refno = ".$refno . 
        ", amount = ".$amount . 
        ", ecurrency = ".$ecurrency . 
        ", remark = ".$remark . 
        ", transid = ".$transid . 
        ", authcode = ".$authcode . 
        ", estatus = ". $estatus . 
        ", errdesc = ".$errdesc . 
        ", signature = ".$signature . 
        ", ccname = ".$ccname . 
        ", ccno = ".$ccno . 
        ", s_bankname = ".$s_bankname . 
        ", s_country = ".$s_country;
        $log->create();

    if ($estatus==1) {
        redirect("https://myhealthcard.my/myhc/marketplace/mp-booking-completed/" . $booking_no);
    }else{
        redirect("https://myhealthcard.my/myhc/error/payment-failed/" . $booking_no);
    }


    
?>