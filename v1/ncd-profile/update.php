<?php
/**
 * Author: Majina
 * NcdProfile.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/ncd-profile/update.php
 * JSON input: { "ic_no":"<ic_no>", "tabacco_use":"<tabacco_use>", "alcohol_consumption":"<alcohol_consumption>, "diet_eat_fruit":"<diet_eat_fruit>", "diet_eat_vege":"<diet_eat_vege>", "physical_activities":"<physical_activities>", "history_hypertension":"<history_hypertension>", "history_diabetes":"<history_diabetes>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/ncd-profile.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$ncdProfile = new NcdProfile($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$ncdProfile->ic_no = $data->ic_no;
  
// set data property values
$ncdProfile->ic_no = $data->ic_no;
$ncdProfile->tabacco_use = $data->tabacco_use;
$ncdProfile->alcohol_consumption = $data->alcohol_consumption;
$ncdProfile->diet_eat_fruit = $data->diet_eat_fruit;
$ncdProfile->diet_eat_vege = $data->diet_eat_vege;
$ncdProfile->physical_activities = $data->physical_activities;
$ncdProfile->history_hypertension = $data->history_hypertension;
$ncdProfile->history_diabetes = $data->history_diabetes;
  
// update the record
if($ncdProfile->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "NCD profile was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update NCD profile.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>