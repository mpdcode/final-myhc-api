<?php
/**
 * Author: Majina
 * NcdProfile.readOne()
 * URL for testing : https://myhc.my/myhc-api/v1/ncd-profile/read-one.php?ic=<ic_no>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/ncd-profile.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$ncdProfile = new NcdProfile($db);
  
// set ID property of record to read
$ncdProfile->ic_no = isset($_GET['ic']) ? $_GET['ic'] : die();
  
// read the details of data to be edited
$ncdProfile->readOne();
  
if (isset($ncdProfile->ic_no)){
 
    // create array
    $arr = array(
        "ic_no" => $ncdProfile->ic_no,
		"tabacco_use" => $ncdProfile->tabacco_use,
        "alcohol_consumption" => $ncdProfile->alcohol_consumption,
        "diet_eat_fruit" => $ncdProfile->diet_eat_fruit,
        "diet_eat_vege" => $ncdProfile->diet_eat_vege,
        "physical_activities" => $ncdProfile->physical_activities,
        "history_hypertension" => $ncdProfile->history_hypertension,
        "history_diabetes" => $ncdProfile->history_diabetes
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "NCD profile does not exist for " . $ncdProfile->ic_no,"error" => "404 Not found"));
}
?>