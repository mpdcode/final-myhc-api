<?php
/**
 * Author: Majina
 * PreRegistration.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/pre-registration/read-one.php?c=<code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/pre-registration.php';
include_once '../../objects/v1/registration-person.php';
include_once '../../objects/v1/screening-plan.php';
include_once '../../objects/v1/company.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$preRegistration = new PreRegistration($db);
$registrationPerson = new RegistrationPerson($db);
$screeningPlan = new ScreeningPlan($db);
$company = new Company($db);
  
// set ID property of record to read
$preRegistration->reg_no = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$preRegistration->readOne();
  
if (isset($preRegistration->seq_reg_no)){
 
    // create array
    $preRegistration_arr = array(
        "seq_reg_no" => $preRegistration->seq_reg_no,
		"reg_no" => $preRegistration->reg_no,
        "ic_no" => $preRegistration->ic_no,
        "name" => $preRegistration->name,
        "mobile_no" => $preRegistration->mobile_no,
        "email" => $preRegistration->email,
        "package_code" => $preRegistration->package_code,
        "amount_paid" => $preRegistration->amount_paid,
        "payment_no" => $preRegistration->payment_no,
        "payment_method" => $preRegistration->payment_method,
        "payment_date" => $preRegistration->payment_date,
        "date_registered" => $preRegistration->date_registered,
        "date_expired" => $preRegistration->date_expired,
        "center_code" => $preRegistration->center_code,
        "status" => $preRegistration->status,
        "company_reg_no"=> $preRegistration->company_reg_no,
        "company"=> $company->readByCoRegNo($preRegistration->company_reg_no),
        "registration_persons"=> $registrationPerson->readByRegNo($preRegistration->reg_no),
        "screening_plan"=> $screeningPlan->readByPackageCode($preRegistration->package_code)
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($preRegistration_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Registrationn info does not exist for " . $preRegistration->seq_reg_no,"error" => "404 Not found"));
}
?>