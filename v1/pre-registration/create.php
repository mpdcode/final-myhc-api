<?php
/**
 * Author: Majina
 * Registration.preRegistration()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/pre-registration/create.php
 * JSON input: { "ic_no":<ic_no>, "mobile_no":"<mobile_no>", "name": "<name>", "email":"<email>", "package_code":"<package_code>"}
 * Method: POST   
 */
     
ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ALL);

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {    
    return 0;    
} 
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/pre-registration.php';
include_once '../../objects/v1/user-account.php';
include_once '../../objects/v1/sms-notification.php';
include_once '../../objects/v1/email-mgmt.php';
include_once '../../objects/v1/screening-plan.php';
include_once '../../objects/v1/package-category.php';
include_once '../../objects/v1/registration-person.php';
include_once '../../objects/v1/ncd-profile.php';
include_once '../../objects/v1/trans-log.php';

  
$database = new Database();
$db = $database->getConnection();
  
$preRegistration = new PreRegistration($db);
$screeningPlan = new ScreeningPlan($db);
$userAccount = new UserAccount($db);
$smsNotification = new SmsNotification($db);
$emailMgmt = new EmailMgmt($db);
$packageCategory = new PackageCategory($db);
$registrationPerson = new RegistrationPerson($db);
$transLog = new TransLog($db);
$ncdProfile = new NcdProfile($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));

// make sure data is not empty
if (
    !empty($data->name) &&
    !empty($data->ic_no) &&
    !empty($data->mobile_no) &&
    !empty($data->email) &&
    !empty($data->package_code)  
){
  
    // set data property values
    //$preRegistration->ic_no = $data->ic_no;
    //$preRegistration->package_code = $data->package_code;
    //$preRegistration->readOneByIcNoAndPackageCode();

    //get selected screening package plan
    $screeningPlan->package_code = $data->package_code;
    $screeningPlan->readOne();

    $proceed = true;



   
    // $password = $userAccount->randomPassword(8);
    // $smsNotification->code = "reg-pre-acc-created";
    // $smsNotification->readOne();
    // $smsNotification->mobile_no = "60173019855";
    // $smsNotification->paramValues =  array( 
    //     array("param" => "?username", "value" => 'M00014'),
    //     array("param" => "?password", "value" => $password )
    // );

    // echo "<pre>";
    // print_R($smsNotification);
    // $smsNotification->sendSms();

    // exit;







    //  //send sms notification
    //  $smsNotification->code = "reg-pre-acc-created";
    //  $smsNotification->readOne();
    //  $smsNotification->mobile_no = "60173019855";
    //  $smsNotification->paramValues =  array( 
    //      array("param" => "?username", "value" => 'reeveperk1'),
    //      array("param" => "?password", "value" => '1234!@#' ),

    //  );

    // // echo "<pre>";
    // // print_R($smsNotification);
    // $resp = $smsNotification->sendSms();
    //  print_R($resp);
    //$password="sjX7qwfH";
    

   

    if ($screeningPlan->category_code=='FAMILY' || $screeningPlan->category_code=='INDIVIDUAL'){
        //check if the user has registered with patient status and not allowed to register as MAU for family and individual 
        $registrationPerson->readByIcNoWithPatientStatus( $data->ic_no);   
        if (isset($registrationPerson->ic_no)){ //record does exist
            // record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Registration info under your name with other package plan has already exist","errorFound"=>true,"error" => "409 record already exist"));
            $proceed = false;
        }    
    }
   // print_R($proceed);
   // exit;
    if ($proceed){
        $preRegistration->ic_no = $data->ic_no;
        $preRegistration->package_code = $data->package_code;
        $preRegistration->readOneByIcNoAndPackageCode();

      
        if (isset($preRegistration->reg_no)){
            // record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Registration info under your name with selected package plan has already exist","errorFound"=>true,"error" => "409 record already exist"));
            $proceed = false;
        }else{
           
            
            $preRegistration->ic_no = $data->ic_no;
            // $preRegistration->reg_no = $data->reg_no;
            $preRegistration->name = $data->name;
            $preRegistration->mobile_no = $data->mobile_no;
            $preRegistration->email = $data->email;
            $preRegistration->package_code = $data->package_code;
            $preRegistration->amount_paid = $data->amount_paid;
            $preRegistration->payment_no = $data->payment_no;
            $preRegistration->payment_method = $data->payment_method;
            $preRegistration->payment_date = $data->payment_date;
            $preRegistration->date_expired = $data->date_expired;
            $preRegistration->center_code = $data->center_code;
            $preRegistration->status = "PRE-ACCOUNT";


            // Add New Fields
            $preRegistration->citizen =$data->citizen;
            $preRegistration->race =$data->race;
            $preRegistration->religion =$data->religion;
            $preRegistration->marital =$data->marital;
            $dob =substr($data->dob,0,10);
            $today = date("Y-m-d");
            $agevalue = date_diff(date_create($dob), date_create($today));
            $preRegistration->age = $agevalue->format('%y');
          
           // Add-on existing fields
           

            $preRegistration->gender = $data->gender;
            $preRegistration->address =$data->address;
            $preRegistration->town =$data->town;
            $preRegistration->district =$data->district;
            $preRegistration->postcode =$data->postcode;
            $preRegistration->state =$data->state;

            $preRegistration->date_of_birth =substr($data->dob,0,10);
            

           // create the record
            if ($preRegistration->create()){

              //exit;

                $preRegistration->ic_no = $data->ic_no;
                $preRegistration->package_code = $data->package_code;
                $preRegistration->readOne();

                //get selected screening package plan
                $screeningPlan->package_code = $preRegistration->package_code;
                $screeningPlan->readOne();

                //get selected package category
                $packageCategory->category_code = $screeningPlan->category_code;
                $packageCategory->readOne();

                
                $userAccount->ic_no = $preRegistration->ic_no;
                $userAccount->readOneByIcNo();
              
                //save ncd details
                
                if (isset($data->ncd)){
                    $dataNcd = $data->ncd;
                  
                    $ncdProfile->ic_no = $dataNcd->ic_no;
                    $ncdProfile->tabacco_use = $dataNcd->tabacco_use;
                    $ncdProfile->alcohol_consumption = $dataNcd->alcohol_consumption;
                    $ncdProfile->diet_eat_fruit = $dataNcd->diet_eat_fruit;
                    $ncdProfile->diet_eat_vege = $dataNcd->diet_eat_vege;
                    $ncdProfile->physical_activities = $dataNcd->physical_activities;
                    $ncdProfile->history_hypertension = $dataNcd->history_hypertension;
                    $ncdProfile->history_diabetes = $dataNcd->history_diabetes;
                    $ncdProfile->create();
                }

             
                if (!isset($userAccount->username)){ //create main user account if not exist
                    $id = sprintf( '%005d', $preRegistration->seq_reg_no );
                    // $userAccount->username = $packageCategory->prefix . $id;
                    $userAccount->email = $preRegistration->email;
                    $userAccount->username = 'M' . $id;
                    $password = $userAccount->randomPassword(8);
                    $userAccount->password = $userAccount->encryptPassword($password);
                    $userAccount->ic_no = $preRegistration->ic_no;
                    if ($screeningPlan->category_code=='FAMILY') $userAccount->patient_type_code = "ADULT";
                    $userAccount->acc_type_code = "MAU";
                    $userAccount->acc_status_code = "PRE-ACCOUNT";
                    $userAccount->menu_owner = $screeningPlan->category_code;
                    // $userAccount->reg_no = $preRegistration->reg_no; //no need any more
                    $userAccount->date_created = date('Y-m-d H:i:s');
                   
                   // New Fields Adding
                   $userAccount->name =$data->name;
                    $userAccount->last_login = date('Y-m-d H:i:s');
                    $userAccount->dob =substr($data->dob,0,10);
                   
                    if ($userAccount->create()){    
                        
                        //send sms notification
                        $smsNotification->code = "reg-pre-acc-created";
                        $smsNotification->readOne();
                        $smsNotification->mobile_no = "60". $preRegistration->mobile_no;
                        $smsNotification->paramValues =  array( 
                            array("param" => "?username", "value" => 'Registered Email'),
                            array("param" => "?password", "value" => $password )
                        );
                        $smsNotification->sendSms();
    
                        //send email
                        $emailMgmt->code = "reg-pre-acc-created";
                        $emailMgmt->readOne();
                        $emailMgmt->receivers = $preRegistration->email;
                        $emailMgmt->paramValues = $smsNotification->paramValues;
                        if($_SERVER['HTTP_HOST']!="127.0.0.1")
                          {$emailMgmt->sendMail();}

                        $transLog->activity="Create user account: " . $userAccount->email . ", reg no: ". $preRegistration->reg_no;
                        $transLog->username="";
                        $transLog->status="success";
                        $transLog->create();
                    }else{
                        $transLog->activity="Create user account: " . $userAccount->email . ", reg no: ". $preRegistration->reg_no;
                        $transLog->username="";
                        $transLog->status="failed";
                        $transLog->create();
                    }
                }else{
                    //send sms notification
                    $smsNotification->code = "reg-package-plan-created";
                    $smsNotification->readOne();
                    $smsNotification->mobile_no = "60". $preRegistration->mobile_no;
                    $smsNotification->paramValues =  array( 
                        array("param" => "?username", "value" => $userAccount->email),
                        array("param" => "?package_plan", "value" => $screeningPlan->single_package )
                    );
                    $smsNotification->sendSms();

                    //send email
                    $emailMgmt->code = "reg-package-plan-created";
                    $emailMgmt->readOne();
                    $emailMgmt->receivers = $preRegistration->email;
                    $emailMgmt->paramValues = $smsNotification->paramValues;
                    if($_SERVER['HTTP_HOST']!="127.0.0.1")
                          {$emailMgmt->sendMail();}

                    $transLog->activity="Create user account: " . $userAccount->email . ", reg no: ". $preRegistration->reg_no;
                    $transLog->username="";
                    $transLog->status="conflict";
                    $transLog->create();
                }
        
                // set response code - 201 created
                http_response_code(201);
        
                // tell the user
                echo json_encode(array("message" => "Pre-registration successfully completed - ".$password,"errorFound"=>false,"error" => "",));
            }else{
                // set response code - 503 service unavailable
                http_response_code(503);
    
                // tell the user
                echo json_encode(array("message" => "Unable to process pre-registration.","errorFound"=>true,"error" => "503 service unavailable"));
            }
        }
    } 
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to process pre-registration. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}

 
 
?>